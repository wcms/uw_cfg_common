<?php

/**
 * @file
 * Module file.
 */

use Drupal\Component\Utility\Html;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\Element;
use Drupal\Core\Session\AccountInterface;
use Drupal\fillpdf\Controller\HandlePdfController;
use Drupal\media_library\MediaLibraryState;
use Drupal\webform\WebformInterface;

include_once 'includes/alter_functions.php';
include_once 'includes/fillpdf_functions.php';
include_once 'includes/form_alter_functions.php';
include_once 'includes/form_id_alter_functions.php';
include_once 'includes/helper_functions.php';
include_once 'includes/preprocess_functions.php';
include_once 'includes/submit_functions.php';
include_once 'includes/validate_functions.php';
include_once 'includes/views_functions.php';
include_once 'includes/webform_functions.php';
include_once 'includes/widget_type_alter.php';

/**
 * Implements hook_entity_presave().
 */
function uw_cfg_common_entity_presave(EntityInterface $entity) {

  // Check if we are on a menu link.
  if ($entity->getEntityTypeId() == 'menu_link_content') {

    // Check that we are on a Information For (audience) link.
    if ($entity->menu_name->value == 'uw-menu-audience-menu') {

      // Invalid all the menu caching.
      \Drupal::cache('menu')->invalidateAll();

      // Rebuild all the menus.
      \Drupal::service('plugin.manager.menu.link')->rebuild();
    }
  }

  // On a node entity save, check if the responsive
  // image has created the derivatives so that things
  // like hero images will load when no image has yet
  // been rendered.  If we do not do this, most hero
  // images will not work.
  if ($entity->getEntityTypeId() == 'node') {

    // ISTWCMS-5846: if the leave summary blank is checked
    // on the node add/edit page, set the summary to NULL.
    // Check that the node has the field leave summary blank.
    if ($entity->hasField('field_uw_blank_summary')) {

      // If the leave summary blank is checked, set summary to NULL.
      if ($entity->field_uw_blank_summary->value) {

        // Get the node type from the entity.
        $node_type = $entity->getType();

        // Since all the summary fields are field_uw_ct_<node_type>_summary,
        // we need to get the node_type from the getType.  We need this
        // because the node_type has uw_ct_ in the name, so simply replacing
        // the uw_ct_ with nothing will give us the node_type.
        // the uw_ct_ with nothing will give us the node_type.
        $node_type = str_replace('uw_ct_', '', $node_type);

        // Since news has the content type with news_item, we need
        // to remove the _item to get the correct field name.
        $node_type = str_replace('_item', '', $node_type);

        // Set the field name using the notation from above.
        $field_name = 'field_uw_' . $node_type . '_summary';

        // Now set the summary to NULL using the field_name.
        $entity->$field_name = NULL;
      }
    }

    // If there is a hero image (media), continue to process.
    if (
      $entity->hasField('field_uw_hero_image') &&
      $media = $entity->field_uw_hero_image->entity
    ) {

      // Hero media exists, get file entity from media.
      if ($file = $media->field_media_image->entity) {

        // Load the image styles that are needed for the hero.
        $uw_styles = \Drupal::service('uw_cfg_common.uw_service')->uwGetResponsiveImageStyles();

        // Step through each of the image styles and ensure that
        // the derivative is created.
        foreach ($uw_styles as $uw_style) {

          // Load the image style.
          $style = \Drupal::entityTypeManager()
            ->getStorage('image_style')
            ->load($uw_style);

          // Get the styled image derivative.
          $destination = $style->buildUri($file->getFileUri());

          // If the derivative doesn't exist yet (as the image style may
          // have been added post launch), create it.
          if (!file_exists($destination)) {
            $style->createDerivative($file->getFileUri(), $destination);
          }
        }
      }
    }

    // If there is a type of media, check if it is set
    // to none and if it is then zero out any banners
    // that were added.
    if ($entity->hasField('field_uw_type_of_media')) {
      if (!$entity->field_uw_type_of_media->value) {
        $entity->set('field_uw_banner', NULL);
      }
    }
  }
}

/**
 * Implements hook_page_attachments().
 */
function uw_cfg_common_page_attachments(array &$page) {
  $page['#attached']['library'][] = 'uw_cfg_common/uw_mathjax';

  // Load uw_cfg_common module analytics configuration.
  $config = \Drupal::config('uw_cfg_common.google_settings');

  if ($config && $gso = $config->get('uw_cfg_common_google_site_ownership')) {
    $data = [
      '#tag' => 'meta',
      '#attributes' => [
        'name' => 'google-site-verification',
        'content' => $gso,
      ],
    ];

    // Attach tag to HEAD section.
    $page['#attached']['html_head'][] = [$data, 'uw-google-site-verification'];
  }

  $admin_page = \Drupal::service('uw_cfg_common.uw_analytics')->administrationPage();

  // Get the code from config and inject to the page.
  if (!$admin_page && !empty($config->get('uw_cfg_common_ga_account'))) {
    $code = Html::escape($config->get('uw_cfg_common_ga_account'));

    // Prevent any output for old Google Analytics code snippets.
    if (str_starts_with($code, 'UA-')) {
      return;
    }

    $snippet = _uw_cfg_common_google_analytics_snippet($code);

    $external_script_data = [
      '#tag' => 'script',
      '#attributes' => [
        'async' => TRUE,
        'src' => 'https://www.googletagmanager.com/gtag/js?id=' . $code,
      ],
    ];
    $page['#attached']['html_head'][] = [
      $external_script_data,
      'uw-google-tag-manager',
    ];

    $analytics = [
      '#type' => 'html_tag',
      '#tag' => 'script',
      '#value' => $snippet,
    ];

    $page['#attached']['html_head'][] = [
      $analytics,
      'uw-google-analytics',
    ];
  }
}

/**
 * Implements hook_ENTITY_TYPE_create_access().
 */
function uw_cfg_common_block_content_create_access(AccountInterface $account, array $context, string $entity_bundle): AccessResult {
  $route_name = \Drupal::routeMatch()->getRouteName();

  if ($route_name === 'media_library.ui') {
    /** @var \Drupal\media_library\MediaLibraryState $state */
    $state = MediaLibraryState::fromRequest(\Drupal::request());
    $openerParameters = $state->getOpenerParameters();

    // If the plugin ID exists within the opener parameters, we know
    // the media library is being used on the layout builder form.
    if (isset($openerParameters['plugin_id']) && substr($openerParameters['plugin_id'], 0, 12) === 'inline_block') {

      if ($account->hasPermission('create and edit custom blocks')) {
        return AccessResult::allowed();
      }
    }
  }

  // No opinion.
  return AccessResult::neutral();
}

/**
 * Implements hook_ENTITY_TYPE_view().
 *
 * Provide download links to associated PDFs on Layout Builder pages containing
 * a Webform block. Only the first such block is used.
 */
function uw_cfg_common_node_view(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, string $view_mode): void {
  // Do nothing unless view_mode full.
  if ($view_mode !== 'full') {
    return;
  }

  // Do nothing unless user is authenticated.
  $current_user = \Drupal::currentUser();
  if (!$current_user->id()) {
    return;
  }

  // Find the first Layout Builder block that contains a Webform.
  $webform = NULL;
  foreach (Element::children($build['_layout_builder']) as $child) {
    foreach (Element::children($build['_layout_builder'][$child]) as $grand_child) {
      foreach (Element::children($build['_layout_builder'][$child][$grand_child]) as $great_grand_child) {
        $element = $build['_layout_builder'][$child][$grand_child][$great_grand_child];
        if (isset($element['content']['#webform']) && $element['content']['#webform'] instanceof WebformInterface) {
          $webform = $element['content']['#webform'];
          break 3;
        }
      }
    }
  }

  // Do nothing unless:
  // Node is Webform.
  // Webform "Show the notification about previous submissions" is enabled.
  if (!$webform || !$webform->getSetting('form_previous_submissions')) {
    return;
  }

  // Do nothing if fillpdf_form entity type does not exist.
  $fillpdf_form_exists = \Drupal::entityTypeManager()->hasDefinition('fillpdf_form');
  if (!$fillpdf_form_exists) {
    return;
  }

  // Load the FillPDF form for this Webform.
  $fillpdf_form_storage = \Drupal::entityTypeManager()->getStorage('fillpdf_form');
  $query = $fillpdf_form_storage->getQuery();
  $query->condition('default_entity_type', 'webform');
  $query->condition('default_entity_id', $webform->id());
  $fillpdf_forms = $query->execute();
  $fillpdf_forms = $fillpdf_form_storage->loadMultiple($fillpdf_forms);

  // Do nothing unless there are PDFs associated with this Webform.
  if (!$fillpdf_forms) {
    return;
  }

  // Do nothing unless there is a Webform submission by this user.
  // Find the most recent submission to this Webform by the current user.
  $webform_submission_entity_id = uw_cfg_common_get_most_recent_webform_submission($webform->id(), $current_user->id());
  if (!$webform_submission_entity_id) {
    return;
  }

  // For each FillPDF form, check access and generate a link with filename.
  $pdf_links = [];

  $fillpdf_link_manipulator = \Drupal::service('fillpdf.link_manipulator');
  $fillpdf_access_helper = \Drupal::service('fillpdf.access_helper');
  $handle_pdf_controller = HandlePdfController::create(\Drupal::getContainer());

  foreach ($fillpdf_forms as $fid => $fillpdf_form) {
    $parameters = ['fid' => $fid];
    $pdf_link = $fillpdf_link_manipulator->generateLink($parameters);

    $context = $fillpdf_link_manipulator->parseLink($pdf_link);

    $has_generate_pdf_access = $fillpdf_access_helper->canGeneratePdfFromContext($context, $current_user);

    if ($has_generate_pdf_access) {
      $handle_pdf_controller->alterContext($context);
      $filename = $handle_pdf_controller->getFilename($context);
      $pdf_links[] = Link::fromTextAndUrl($filename, $pdf_link);
    }
  }

  // Do nothing if no links to display.
  if (!$pdf_links) {
    return;
  }

  // Create status message.
  $message = [
    '#theme' => 'item_list',
    '#items' => $pdf_links,
    '#prefix' => t('Download PDF version of your form:'),
  ];
  $message = \Drupal::service('renderer')->render($message);
  \Drupal::messenger()->addStatus($message);
}

/**
 * Function to get options for faculty colors.
 *
 * @return array
 *   Array of faculty colors as options.
 */
function _uw_cfg_common_get_faculty_color_options(): array {

  return [
    'org-default' => t('Black (University of Waterloo)'),
    'org-art' => t('Orange (Arts)'),
    'org-eng' => t('Purple (Engineering)'),
    'org-env' => t('Green (Environment)'),
    'org-ahs' => t('Teal (Health)'),
    'org-mat' => t('Pink (Mathematics)'),
    'org-sci' => t('Blue (Science)'),
    'org-school' => t('Red (school)'),
    'org-cgc' => t('Red (Conrad Grebel University College)'),
    'org-ren' => t('Green (Renison University College)'),
    'org-stj' => t('Green (St. Jerome’s University)'),
    'org-stp' => t('Blue (United College)'),
  ];
}
