<?php

/**
 * @file
 * Form alter functions file.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_alter().
 *
 * Hide/disable metatag information on our nodes.
 */
function uw_cfg_common_form_alter(
  array &$form,
  FormStateInterface $form_state,
  string $form_id
): void {

  // Only blog, event and news item has 'Image' option.
  $hero_image_form_ids = [
    'node_uw_ct_blog_form',
    'node_uw_ct_blog_edit_form',
    'node_uw_ct_event_form',
    'node_uw_ct_event_edit_form',
    'node_uw_ct_news_item_form',
    'node_uw_ct_news_item_edit_form',
  ];
  if (in_array($form_id, $hero_image_form_ids)) {
    // Set required when hero image field shows.
    if (isset($form['field_uw_hero_image']['widget'])) {
      $form['field_uw_hero_image']['widget']['#required'] = TRUE;
    }
  }

  // Prevent redirects to pilots or staging URLs.
  if ($form_id == 'redirect_redirect_form' || $form_id == 'redirect_redirect_edit_form') {
    $form['#validate'][] = '_uw_cfg_common_redirect_destination_validation';
  }

  // ISTWCMS-5846: if we are on a UW content type which is
  // the notation of node_uw_ct_<node_type>_form of the
  // form_id, then we need to add the status to the
  // summary field.
  if (preg_match("/node_uw_ct_.*form/", $form_id)) {

    // Only add states if we have the blank summary field.
    if (isset($form['field_uw_blank_summary'])) {

      // Since we are unable to simply get the node type in format that
      // we need to use it (i.e. event, news, blog, etc.), we need to
      // take the form_id and manipulate it to get us the node type.
      // First is the remove the node_uw_ct_, then the _edit and
      // finally the _form.
      $node_type = str_replace('node_uw_ct_', '', $form_id);
      $node_type = str_replace('_edit', '', $node_type);
      $node_type = str_replace('_form', '', $node_type);
      $node_type = str_replace('_quick_node_clone', '', $node_type);

      // We need to add this because news has the name news_item in its
      // content type, so we need to remove that to just get news
      // to be used in the field name.
      $node_type = str_replace('_item', '', $node_type);

      // If the node type is an opportunity, we have to give
      // the specific field name.
      if ($node_type == 'opportunity') {
        $field_name = 'field_uw_opportunity_position';
      }
      // Now since all the rest of the field names
      // are the same, field_uw_<node_type>_summary,
      // we can get the proper field name.
      else {
        $field_name = 'field_uw_' . $node_type . '_summary';
      }

      // Set the states of the summary, required and visible.
      $form[$field_name]['widget'][0]['#states'] = [
        'visible' => [
          ':input[name="field_uw_blank_summary[value]"]' => ['checked' => FALSE],
        ],
      ];

      // Add custom validation for blank summaries.
      $form['#validate'][] = '_uw_cfg_common_blank_summaries_validation';
    }

    // Get the vimeo banner settings.
    $config = \Drupal::config('uw_cfg_common_video_banner.settings');

    // If the vimeo video banner is not enabled, remove the
    // option from the dropdown for banners.
    if (!$config->get('video_banner_enabled')) {
      unset($form['field_uw_banner']['widget']['add_more']['add_more_button_uw_para_vimeo_video_banner']);
    }
  }

  // ISTWCMS-4648: removing revisions from layout builder page.
  if (\Drupal::routeMatch()->getRouteName() == 'layout_builder.overrides.node.view') {
    $form['revision']['#access'] = FALSE;
    $form['advanced']['#access'] = FALSE;
  }

  // Ensure that we are on a UW content type node.
  if (preg_match('/^node_uw_ct_.*_form$/', $form_id)) {

    // Ensure that the node has metatag information.
    if (isset($form['field_uw_meta_tags'])) {

      // Hide elements under advanced settings.
      $form['field_uw_meta_tags']['widget'][0]['advanced']['canonical_url']['#access'] = FALSE;
      $form['field_uw_meta_tags']['widget'][0]['advanced']['shortlink_url']['#access'] = FALSE;

      // Hide elements under open graph.
      $form['field_uw_meta_tags']['widget'][0]['open_graph']['og_type']['#access'] = FALSE;
      $form['field_uw_meta_tags']['widget'][0]['open_graph']['og_url']['#access'] = FALSE;
      $form['field_uw_meta_tags']['widget'][0]['open_graph']['og_updated_time']['#access'] = FALSE;
      $form['field_uw_meta_tags']['widget'][0]['open_graph']['og_locale']['#access'] = FALSE;

      // Hide elements under X (formerly Twitter) settings.
      $form['field_uw_meta_tags']['widget'][0]['twitter_cards']['twitter_cards_page_url']['#access'] = FALSE;
    }
  }

  // If there is a type of media field, it means that we are
  // on a node add/edit page, so add the states for the
  // actual media types in the hero section.
  if (isset($form['field_uw_type_of_media'])) {

    // Get the node type from the form id.
    $node_type = str_replace('node_uw_ct_', '', $form_id);
    $node_type = str_replace('_edit', '', $node_type);
    $node_type = str_replace('_quick_node_clone', '', $node_type);
    $node_type = str_replace('_form', '', $node_type);
    $node_type = str_replace('_item', '', $node_type);

    // Content types to remove the hero image from media.
    $remove_hero_image_nodes = [
      'web_page',
      'catalog',
      'contact',
      'opportunity',
      'profile',
      'project',
      'service',
    ];

    // If the node type can not have hero image, remove the option.
    if (in_array($node_type, $remove_hero_image_nodes)) {
      unset($form['field_uw_type_of_media']['widget']['#options']['image']);
    }

    // Set the states for the hero image.
    $form['field_uw_hero_image']['#states'] = [
      'visible' => [
        [
          'select[name="field_uw_type_of_media"]' => [
            ['value' => 'image'],
          ],
        ],
      ],
    ];

    // Set the states for the banner.
    $form['group_banner']['#states'] = [
      'visible' => [
        [
          'select[name="field_uw_type_of_media"]' => [
            ['value' => 'banner'],
          ],
        ],
      ],
    ];

    // Set the states for the banner settings.
    $form['field_uw_text_overlay_style']['#states'] = [
      'visible' => [
        [
          'select[name="field_uw_type_of_media"]' => [
            ['value' => 'banner'],
          ],
        ],
      ],
    ];

    // Set the states for the banner settings.
    $form['field_uw_media_width']['#states'] = [
      'visible' => [
        [
          'select[name="field_uw_type_of_media"]' => [
            ['value' => 'banner'],
          ],
        ],
      ],
    ];

    // Set the states for the banner settings, if it exists.
    if (isset($form['field_uw_page_title_big_text'])) {

      // ISTWCMS-6313.
      // The next two are for existing content types that do not have
      // the default values and options set for page title big text.
      if (empty($form['field_uw_page_title_big_text']['widget']['#default_value'])) {
        $form['field_uw_page_title_big_text']['widget']['#default_value'][0] = 0;
      }
      if (isset($form['field_uw_page_title_big_text']['widget']['#options']['_none'])) {
        unset($form['field_uw_page_title_big_text']['widget']['#options']['_none']);
      }

      // The form state for page title as big text.
      $form['field_uw_page_title_big_text']['#states'] = [
        'visible' => [
          [
            'select[name="field_uw_type_of_media"]' => [
              ['value' => 'banner'],
            ],
          ],
        ],
      ];
    }

    // ISTWCMS-6313: adding the JS on the node add/edit page,
    // that will check for enable/disable big text in banner image.
    $form['#attached']['library'][] = 'uw_cfg_common/uw_cfg_common';

    // Add our custom validation for banners.
    $form['#validate'][] = '_uw_cfg_common_banner_validate';
  }

  // If we are on the media upload form, we want to restrict
  // what crops are available.
  if ($form_id == 'media_library_add_form_upload') {

    // If the crop widget is on the form, unset certain crops.
    if (isset($form['media'][0]['fields']['field_media_image']['widget'][0]['#crop_list'])) {

      // Get the parameters from the request, this was the only
      // way to get out what the bundle was.  Since this is a new
      // form call from media_library, we could not use get current
      // path or uri, since it would only return /media_library.
      // The media library parameters has everything listed for the
      // node and the node types.
      $media_lib_parameters = \Drupal::request()->query->get('media_library_opener_parameters');

      // If there are media lib parameters, process them.
      if ($media_lib_parameters) {

        // If there is a bundle on the parameters, continue
        // to process.
        if (isset($media_lib_parameters['bundle'])) {

          // If this is a contact, remove all the responsive crops.
          // If anything else, remove the portrait crop.
          if ($media_lib_parameters['bundle'] == 'uw_ct_contact') {
            foreach ($form['media'][0]['fields']['field_media_image']['widget'][0]['#crop_list'] as $index => $crop) {
              if ($crop !== 'uw_crop_portrait') {
                unset($form['media'][0]['fields']['field_media_image']['widget'][0]['#crop_list'][$index]);
              }
            }
          }
          else {
            foreach ($form['media'][0]['fields']['field_media_image']['widget'][0]['#crop_list'] as $index => $crop) {
              if ($crop == 'uw_crop_portrait') {
                unset($form['media'][0]['fields']['field_media_image']['widget'][0]['#crop_list'][$index]);
              }
            }
          }
        }
      }
    }
  }
  // Remove 'Unpublished' from state select list in content_list exposed filter.
  if ($form_id == 'views_exposed_form' && isset($form['current_state_views_filter'])) {
    // If the state select list includes 'Unpublished', remove it.
    unset($form['current_state_views_filter']['#options']['UW workflow']['uw_workflow-uw_wf_unpublished']);
  }
}
