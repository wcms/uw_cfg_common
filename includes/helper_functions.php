<?php

/**
 * @file
 * Helper functions file.
 */

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\taxonomy\Entity\Term;

/**
 * Function to get the class for the term form the object.
 *
 * @param \Drupal\taxonomy\Entity\Term $term
 *   The term object.
 *
 * @return string
 *   The class for the term.
 */
function uw_get_org_class_from_term(Term $term): string {
  return uw_get_org_class_from_name($term->label());
}

/**
 * Function to get the class for the term using the name.
 *
 * @param string $name
 *   The term object.
 *
 * @return string
 *   The class for the term.
 */
function uw_get_org_class_from_name(string $name): string {

  // Get the class name from the term label.
  switch ($name) {

    case 'Conrad Grebel University College':
      $class = 'org-cgc';
      break;

    case 'Faculty of Arts':
    case 'Arts':
      $class = 'org-art';
      break;

    case 'Faculty of Engineering':
    case 'Engineering':
      $class = 'org-eng';
      break;

    case 'Faculty of Environment':
    case 'Environment':
      $class = 'org-env';
      break;

    case 'Faculty of Health':
    case 'Health':
      $class = 'org-ahs';
      break;

    case 'Faculty of Mathematics':
    case 'Math':
      $class = 'org-mat';
      break;

    case 'Faculty of Science':
    case 'Science':
      $class = 'org-sci';
      break;

    case 'Renison University College':
      $class = 'org-ren';
      break;

    case 'St. Jerome’s University':
      $class = 'org-stj';
      break;

    case 'United College':
      $class = 'org-stp';
      break;

    default:
      $class = 'org-default';
      break;
  }

  return $class;
}

/**
 * Function to get the short faculty term name.
 *
 * @param string $label
 *   The current term label.
 *
 * @return string
 *   The shorten term label.
 */
function uw_get_short_faculty_tag(string $label): string {

  return str_replace('Faculty of ', '', $label);
}

/**
 * Function to get the content type list that use tags and their links.
 *
 * @return string[]
 *   Array of content types with tags and their links.
 */
function uw_get_content_type_list_for_tags(): array {

  // The list of content types and their listing page url.
  return [
    'uw_ct_blog' => 'blog',
    'uw_ct_event' => 'events',
    'uw_ct_news_item' => 'news',
    'uw_ct_opportunity' => 'opportunities',
    'uw_ct_profile' => 'profiles',
    'uw_ct_project' => 'projects/search',
    'uw_ct_service' => 'services',
  ];
}

/**
 * Function to get the tag field names and their links.
 *
 * @return string[]
 *   Array to get the field names and their links.
 */
function uw_get_tag_field_names_and_links(): array {

  // The tag field names to use in the parameters.
  return [
    'field_uw_audience' => 'audience',
    'field_uw_event_type' => 'type',
    'field_uw_opportunity_type' => 'opportunity_type',
    'field_uw_opportunity_employment' => 'employment_type',
    'field_uw_opportunity_pay_type' => 'rate_of_pay',
    'field_uw_ct_profile_type' => 'type',
    'field_uw_project_status' => 'status',
    'field_uw_project_topics' => 'topics',
    'field_uw_service_audience' => 'audience',
    'field_uw_service_category' => 'categories',
  ];
}

/**
 * Function to fix the array for tags.
 *
 * @param array $tags
 *   The array of tags.
 * @param string $content_type
 *   The content type.
 *
 * @return array
 *   Array of fixed tags.
 */
function uw_fix_tags_array(array $tags, string $content_type): array {

  // The tag field names to use in the parameters.
  $tag_field_names = uw_get_tag_field_names_and_links();

  // Get the content type list for tags.
  $content_type_list = uw_get_content_type_list_for_tags();

  // Step through each of the tags and get the
  // correct url.
  foreach ($tags as $index => $tag) {

    // Start the url to the content type listing page, service
    // category does not require the content type after the /.
    if ($tag['field_name'] !== 'field_uw_service_category') {
      $new_url = '/' . $content_type_list[$content_type];
    }
    else {
      $new_url = '';
    }

    // If the tag is in the list of field names to convert,
    // then convert them using the url provided, if not use
    // the field name with tags.
    if (array_key_exists($tag['field_name'], $tag_field_names)) {

      // If this is not a service tag, add the query parameters.
      // Services require a special url, so we will take care
      // of that later.
      if (
        $tag['field_name'] !== 'field_uw_service_audience' &&
        $tag['field_name'] !== 'field_uw_service_category'
      ) {
        $new_url .= '?' . $tag_field_names[$tag['field_name']];
      }
    }
    else {
      $new_url .= '?tags';
    }

    // Add the tid to the new url, projects status requires
    // the non array for tid.
    if ($tag['field_name'] == 'field_uw_project_status') {
      $new_url .= '=' . $tag['tid'];
    }
    // If this is a service, just use the url to the term.
    elseif (
      $tag['field_name'] == 'field_uw_service_audience' ||
      $tag['field_name'] == 'field_uw_service_category'
    ) {
      $new_url .= $tag['url'];
    }
    // Add the array tid query parameter.
    else {
      $new_url .= '[' . $tag['tid'] . ']=' . $tag['tid'];
    }

    // Set the new url.
    $tags[$index]['url'] = $new_url;
  }

  return $tags;
}

/**
 * Function to sort array by weight.
 *
 * @param array $arr
 *   The array to be sorted.
 *
 * @return array
 *   The sorted array.
 */
function uw_sort_array_by_weight(array $arr): array {

  // Sort the array by weight.
  usort($arr, function ($a, $b) {
    return $a['weight'] <=> $b['weight'];
  });

  return $arr;
}

/**
 * Function to get the type of banners.
 *
 * @return array
 *   Array of game types.
 */
function uw_get_banner_types(): array {

  return [
    'full-width' => t('Full banner width, bottom, theme colour background ("FDSU" style)'),
    'left-dark' => t('Left side of banner, vertically centered, faded background, white text (for dark images)'),
    'left-light' => t('Left side of banner, vertically centered, faded background, black text (for light images)'),
    'split' => t('Split top and bottom, black and white backgrounds ("single page" style)'),
    'full-overlay' => t('Full black overlay, centered text ("conference" style)'),
  ];
}

/**
 * Function to get the additional options.
 *
 * @return array
 *   Array of additional options.
 */
function uw_get_additional_options(): array {

  return [
    'all' => t('Show site title and menus'),
    'title' => t('Show site title only'),
    'none' => t('No site title or menus'),
  ];
}

/**
 * Get the layout builder styles as options.
 *
 * @param \Drupal\field\Entity\FieldStorageConfig $definition
 *   The field definition.
 * @param \Drupal\Core\Entity\ContentEntityInterface|null $entity
 *   The entity being created if applicable.
 * @param bool $cacheable
 *   Boolean indicating if the results are cacheable.
 *
 * @return array
 *   An array of possible key and value options.
 *
 * @see options_allowed_values()
 */
function _uw_cfg_common_get_layout_builder_styles(
  FieldStorageConfig $definition,
  ?ContentEntityInterface $entity = NULL,
  bool &$cacheable = NULL,
) {

  // Get all the styles for sections.
  $all_styles = _layout_builder_styles_retrieve_by_type('section');
  // Array of options.
  $options = [];

  // Step through each style and get the info.
  foreach ($all_styles as $style) {
    // Only load styles from the "default" group,
    // which contains the section widths.
    // Needed for when other section styles ship.
    if ($style->getGroup() == 'default') {
      $options[$style->id()] = $style->label();
    }
  }

  return $options;
}

/**
 * Returns Google Analytics snippet code.
 *
 * @param string $code
 *   Analytics code could be G-9999999999.
 *
 * @return string
 *   Code snippet to be injected to html page in script tag.
 */
function _uw_cfg_common_google_analytics_snippet(string $code): string {
  $host = \Drupal::request()->getBaseUrl();
  $domain = \Drupal::request()->getHttpHost();

  $snippet = "window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', '@gtag', {
  'cookie_domain': '" . $domain . "',
  'cookie_path': '" . $host . "/',
  'cookie_expires': 2592000
});";

  return str_replace('@gtag', $code, $snippet);
}

/**
 * Split text on EOL, trim, remove blanks and duplicates, and return as array.
 *
 * @param string|null $data
 *   The data to act on.
 *
 * @return string[]
 *   The data as an array.
 */
function uw_cfg_common_array_split_clean(?string $data): array {

  $data = preg_split('/[\r\n]+/', $data);
  $data = array_map('trim', $data);
  $data = array_filter($data);
  $data = array_unique($data);

  return $data;
}

/**
 * Function to remove extra classes.
 *
 * @param array $classes
 *   Array of classes.
 *
 * @return array
 *   Array of classes with removals
 */
function _uw_cfg_common_remove_extra_classes(array $classes): array {

  // The classes to be removed.
  $classes_to_remove = [
    'uw-section__background--contained',
    'uw-section__background--full-width',
  ];

  // Step through each of the classes and remove
  // the extra classes that are not required.
  foreach ($classes as $index => $value) {

    // If class is in the classes to remove array,
    // remove it from the classes array.
    if (in_array($value, $classes_to_remove)) {
      unset($classes[$index]);
    }
  }

  return $classes;
}

/**
 * Set dynamic allowed values for the type of media field.
 *
 * @param \Drupal\field\Entity\FieldStorageConfig $definition
 *   The field definition.
 * @param \Drupal\Core\Entity\ContentEntityInterface|null $entity
 *   The entity being created if applicable.
 * @param bool $cacheable
 *   Boolean indicating if the results are cacheable.
 *
 * @return array
 *   An array of possible key and value options.
 *
 * @see options_allowed_values()
 */
function _uw_cfg_common_allowed_media_types(
  FieldStorageConfig $definition,
  ?ContentEntityInterface $entity = NULL,
  bool &$cacheable = NULL,
): array {

  return [
    'banner' => 'Banner',
    'image' => 'Image',
  ];
}

/**
 * Return the Active Directory groups that the current user is part of.
 *
 * @return null|string[]
 *   An array of Active Directory group names or NULL if unable to determine.
 */
function uw_cfg_common_get_user_ad_groups(): ?array {

  $attributes = \Drupal::service('simplesamlphp_auth.manager')->getAttributes();

  return $attributes['http://schemas.xmlsoap.org/claims/Group'] ?? NULL;
}

/**
 * Function to get the uw content types.
 *
 * @return array
 *   The array of content types.
 *
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 */
function _uw_get_node_types() {

  // The content types that are not getting any changes.
  $cts_not_to_install = [
    'uw_ct_sidebar',
    'uw_ct_site_footer',
    'uw_ct_expand_collapse_group',
  ];

  // Get all the node types.
  $node_types = \Drupal::entityTypeManager()
    ->getStorage('node_type')
    ->loadMultiple();

  // Step through each node type and get the path to
  // the module and the machine name.
  foreach ($node_types as $node_type) {

    // Get the id from the node type.
    $id = $node_type->id();

    // Ensure we are only getting the node type
    // that need updating.
    if (!in_array($id, $cts_not_to_install)) {

      // Catalogs and opportunities have different paths
      // and ids, so setup the content types array with
      // the correct info.
      if ($id === 'uw_ct_catalog_item') {
        $content_types['uw_ct_catalog'] = $id;
      }
      elseif ($id === 'uw_ct_opportunity') {
        $content_types['uw_ct_opportunities'] = $id;
      }
      else {
        $content_types[$id] = $id;
      }
    }
  }

  return $content_types;
}

/**
 * Add University of Waterloo to head title, if not present.
 *
 * @param array $variables
 *   The variables.
 */
function _uw_add_page_title(array &$variables) {

  // Flag if we need to add UW to the page title.
  $uw_flag = FALSE;

  // The header title is an array that either contains
  // a single string with the full title, or pieces
  // to assemble the title. We only need to check
  // the last piece for the University name.
  $last_piece = $variables['head_title'][array_key_last($variables['head_title'])];

  // Check that the last piece is either exactly the
  // University name or ends with a pipe and the
  // University name, because the University name might
  // be in some site titles (e.g. "UofW's 60th").
  if ($last_piece == 'University of Waterloo' || str_ends_with($last_piece, '| University of Waterloo')) {
    $uw_flag = TRUE;
  }

  // If there is no UW flag, meaning there is no UW in the
  // head title, then add UW to the head title.
  if (!$uw_flag) {
    $variables['head_title']['uw'] = 'University of Waterloo';
  }
}

/**
 * Function to set a variable that can be used in other preprocesses.
 *
 * @param string $var_name
 *   The name of the variable.
 * @param mixed $new_val
 *   (Optional) The new value of the variable.
 *
 * @return mixed|null
 *   The value of the variable, or null if not set.
 */
function _uw_var(
  string $var_name,
  $new_val = NULL,
) {

  // Set the var to be used later.
  $vars = &drupal_static(__FUNCTION__, []);

  // If a new value has been passed, set it.
  if ($new_val) {
    $vars[$var_name] = $new_val;
  }

  // Return the variable or null.
  return $vars[$var_name] ?? NULL;
}

/**
 * Sets map variables that can be used in other preprocess functions.
 *
 * This function checks the content type and sets latitude, longitude,
 * and address details for 'uw_ct_event' and 'uw_ct_service' content types.
 *
 * @param array $variables
 *   The variables array passed by reference.
 * @param string $content_type
 *   The name of the content type.
 */
function _uw_set_map_variables(array &$variables, $content_type) {
  // Check if the content type is 'uw_ct_event' or 'uw_ct_service'.
  if ($content_type == 'uw_ct_event' || $content_type == 'uw_ct_service') {

    // Set latitude and longitude based on the content type.
    if ($content_type == 'uw_ct_event') {
      $latitude = $variables['node']->field_uw_event_location_coord->lat ?? NULL;
      $longitude = $variables['node']->field_uw_event_location_coord->lon ?? NULL;
      $address = $variables['node']->get('field_uw_event_location_address')->getValue();
    }
    else {
      $latitude = $variables['node']->field_uw_service_location_coord->lat ?? NULL;
      $longitude = $variables['node']->field_uw_service_location_coord->lon ?? NULL;
      $address = $variables['node']->get('field_uw_service_location')->getValue();
    }

    // If both latitude and longitude are available, proceed.
    if ($latitude && $longitude) {

      // Check if the address data is available and set address components.
      if (!empty($address[0])) {
        $address_line1 = $address[0]['address_line1'] ?? NULL;
        $organization = $address[0]['organization'] ?? NULL;
        $locality = $address[0]['locality'] ?? NULL;
        $administrative_area = $address[0]['administrative_area'] ?? NULL;
        $contry_code = $address[0]['country_code'] ?? NULL;
      }
      else {
        $address_line1 = NULL;
        $organization = NULL;
        $locality = NULL;
        $administrative_area = NULL;
        $contry_code = NULL;
      }

      // Attach the custom JavaScript file.
      $variables['content']['#attached']['library'][] = 'uw_cfg_common/leaflet_map';

      // Add the coordinates and address details to drupalSettings.
      $variables['content']['#attached']['drupalSettings']['uw_cfg_common'] = [
        'latitude' => $latitude,
        'longitude' => $longitude,
        'address_line1' => $address_line1,
        'organization' => $organization,
        'locality' => $locality,
        'administrative_area' => $administrative_area,
        'contry_code' => $contry_code,
      ];
    }
  }
}
