<?php

/**
 * @file
 * Simplesaml functions file.
 */

use Drupal\user\UserInterface;

/**
 * Implements hook_simplesamlphp_auth_user_attributes().
 *
 * Add role expiry records for all roles populated automatically from
 * simpleSAMLphp attributes by simplesamlphp_auth module.
 */
function uw_cfg_common_simplesamlphp_auth_user_attributes(
  UserInterface $account,
  array $attributes
): bool {

  // Get the SimplesamlphpDrupalAuth service.
  $drupalauth = \Drupal::service('simplesamlphp_auth.drupalauth');

  $roles_to_expire = $drupalauth->getMatchingRoles();

  // Get the role_expire API.
  $role_expire = \Drupal::service('role_expire.api');

  $expiry_timestamp = time() + 24 * 60 * 60;

  foreach ($roles_to_expire as $rid) {
    // Add role_expiry for the account.
    $role_expire->writeRecord($account->id(), $rid, $expiry_timestamp);
  }

  // This implementation does not alter $account, so return is always FALSE.
  return FALSE;
}
