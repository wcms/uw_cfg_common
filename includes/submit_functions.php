<?php

/**
 * @file
 * Submit functions file.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Submit handler for section background.
 *
 * @param array $form
 *   Form render array.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   Form state object.
 */
function uw_cfg_common_submit_section_configuration_submit(
  array &$form,
  FormStateInterface $form_state
) {

  $layout_builder_background = $form_state->getValues();
  $type = $layout_builder_background['layout_builder_background_type'] ?? 'none';

  /** @var \Drupal\Core\Layout\LayoutDefault $layout */
  $layout = $form_state->getFormObject()->getLayout();
  $config = $layout->getConfiguration();

  // Reset all previously stored values.
  $config['layout_builder_background_type'] = $type;
  $config['layout_builder_background_color'] = NULL;
  $config['layout_builder_background_image'] = NULL;
  $config['layout_builder_background_image_image_tint'] = $layout_builder_background['layout_builder_background_image_image_tint'] ?? 'none';
  $config['layout_builder_background_image_text_color'] = $layout_builder_background['layout_builder_background_image_text_color'] ?? 'white';

  if ($type !== 'none') {
    if ($type === 'solid_color') {
      $config['layout_builder_background_color'] = $layout_builder_background['layout_builder_background_color'] ?? 'neutral';
    }
    elseif ($type === 'image') {
      $config['layout_builder_background_image'] = $layout_builder_background['layout_builder_background_image'];
    }
  }
  else {
    $config['layout_builder_background_color'] = NULL;
    $config['layout_builder_background_image_image_tint'] = NULL;
    $config['layout_builder_background_image_text_color'] = NULL;
  }

  $layout->setConfiguration($config);
}
