<?php

/**
 * @file
 * Validate functions file.
 */

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\FormStateInterface;

/**
 * Validation for section background.
 *
 * @param array $form
 *   Form render array.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   Form state.
 */
function uw_cfg_common_validate_section_configuration_validate(
  array &$form,
  FormStateInterface $form_state
) {

  $type = $form_state->getValue('layout_builder_background_type');

  if ($type === 'image' && empty($form_state->getValue('layout_builder_background_image'))) {

    // Get the values from the form state.
    $values = $form_state->getValues();

    // If this is image, ensure one is selected.
    if (
      $values['layout_builder_background_type'] === 'image' &&
      empty($values['layout_builder_background_image'])
    ) {

      $form_state->setErrorByName(
        'layout_builder_background_type',
        t('Background image is required!')
      );
    }
  }
}

/**
 * Validates submission values for banners on nodes.
 */
function _uw_cfg_common_banner_validate(
  array &$form,
  FormStateInterface $form_state
) {

  // Get the values of the form state.
  $values = $form_state->getValues();

  // The flag for if this is a block.
  $is_block_flag = FALSE;

  // If this is a block, get the variables for the block.
  // If this is not a block, get the variables for a node.
  if (isset($values['settings']['block_form'])) {

    $is_block_flag = TRUE;
    $type_of_media = 'banner';
    $banners = $values['settings']['block_form']['field_uw_banner_item'];
    $form_element = 'set_by_name';
    $text_overlay = $values['settings']['block_form']['field_uw_text_overlay_style'][0]['value'];
  }
  else {

    // Ensure that this is a banner media and not an image.
    if (
      isset($values['field_uw_type_of_media']) &&
      !empty($values['field_uw_type_of_media']) &&
      $values['field_uw_type_of_media'][0]['value'] == 'banner'
    ) {

      $type_of_media = 'banner';
      $banners = $values['field_uw_banner'];
      $text_overlay = $values['field_uw_text_overlay_style'][0]['value'];
      $form_element = $form['group_banner'];
    }
    else {
      $type_of_media = NULL;
    }
  }

  // If there is a type of media set and it is banners,
  // then perform the validation.
  if (
    $type_of_media == 'banner'
  ) {

    // Flag used if there is a banner.
    $banner_flag = FALSE;

    // Step through the banners and get the actual count
    // of the total number of banners.
    foreach ($banners as $banner_key => $banner) {

      // If the key is an integer, it means it has an actual banner.
      // Also, if the top is set and there is no confirm removal button
      // then the banner is present, so set the flag.
      if (
        is_int($banner_key) &&
        isset($banner['top']) &&
        !isset($banner['top']['links']['confirm_remove_button'])
      ) {
        $banner_flag = TRUE;
        break;
      }
    }

    // If the banner flag is not set, set error about adding
    // at least one banner.
    if (!$banner_flag) {

      // Since there is a problem with block inline errors, we use the
      // set by name to use the setErrorByName function, which will
      // display the error and not really affect anything inline.
      // If not just use the regular form error.
      if ($form_element = 'set_by_name') {
        $form_state->setErrorByName('field_uw_banner_item', t('You must add at least one banner.'));
      }
      else {
        $form_state->setError($form_element, t('You must add at least one banner.'));
      }
    }
    // At least one banner, now check that there is an
    // image in each banner.
    else {

      // Step through all the values of banners and check for an image.
      foreach ($banners as $key => $value) {

        // Ensure that the key is an integer, since there is an add_more
        // in the values.  We also need to check if there is "top" in the
        // array, as this is what paragraphs uses for closed items.  These
        // closed items have already been validated so if there is a closed
        // item we can just ignore it, only open items, meaning it has values
        // in the fields needs to be validated.
        if (is_int($key) && !$value['top']) {

          // If there is no selection on the media, then there is no image,
          // so set the error.
          // @todo fix so that inline errors show.
          if (!isset($value['subform']['field_uw_ban_image']['selection'])) {

            // Get the banner image form element based on
            // the block flag.
            if ($is_block_flag) {
              $ban_image_form_element = $form['settings']['block_form']['field_uw_banner_item']['widget'][$key]['subform']['field_uw_ban_image'];
            }
            else {
              $ban_image_form_element = $form['field_uw_banner']['widget'][$key]['subform']['field_uw_ban_image'];
            }

            // Set the error that an image is missing.
            $form_state->setError(
              $ban_image_form_element,
              t('Banner image field is required.')
            );
          }

          // If this is a split text overlay, check for a link
          // inside the other text.
          if (
            $text_overlay == 'split' &&
            isset($value['subform']['field_uw_ban_link'][0]['uri']) &&
            $value['subform']['field_uw_ban_link'][0]['uri'] !== ''
          ) {

            // If there is a link in the other text, set an error.
            if (
              preg_match(
                '/<a.*?>/',
                $value['subform']['field_uw_other_text'][0]['value']
              )
            ) {

              // Get the other text form element based on
              // the block flag.
              if ($is_block_flag) {
                $other_text_form_element = $form['settings']['block_form']['field_uw_banner_item']['widget'][$key]['subform']['field_uw_other_text']['widget'];
              }
              else {
                $other_text_form_element = $form['field_uw_banner']['widget'][$key]['subform']['field_uw_other_text']['widget'];
              }

              // Set an error about a link in the other text.
              $form_state->setError(
                $other_text_form_element,
                t('You can not have a link in the other text when the banner image has a link.')
              );
            }
          }
        }
        else {

          // Flag to validate the image.
          $validate_image = TRUE;

          // Get the triggering element if any.
          if ($te = $form_state->getTriggeringElement()) {

            // If the triggering element is to remove the banner,
            // then we do not care about validating the image, so
            // set the flag to false.
            if (
              isset($te['#paragraphs_mode']) &&
              $te['#paragraphs_mode'] == 'remove'
            ) {
              $validate_image = FALSE;
            }
          }

          // Only validate the image, if the flag is set.
          if ($validate_image) {

            // If there is a banner, ensure that it has an image.
            if (isset($value['subform'])) {
              if (!isset($value['subform']['field_uw_ban_image']['selection'])) {

                if (isset($form['field_uw_banner']['widget'][$key]['subform']['field_uw_ban_image']['widget'])) {
                  $form_state->setError(
                    $form['field_uw_banner']['widget'][$key]['subform']['field_uw_ban_image']['widget'],
                    t("You must select an image")
                  );
                }

                if (isset($form['settings']['block_form']['field_uw_banner_item']['widget'][$key]['subform']['field_uw_ban_image']['widget'])) {
                  $form_state->setError(
                    $form['settings']['block_form']['field_uw_banner_item']['widget'][$key]['subform']['field_uw_ban_image']['widget'],
                    t("You must select an image")
                  );
                }
              }
            }
          }

          // If there is a banner, ensure that it has an image or video.
          if (isset($value['subform'])) {
            if (isset($value['subform']['field_uw_ban_video'])) {
              if (!isset($value['subform']['field_uw_ban_video']['selection'])) {
                if (isset($form['settings'])) {
                  $form_state->setError(
                    $form['settings']['block_form']['field_uw_banner_item']['widget'][$key]['subform']['field_uw_ban_video']['widget'],
                    t("You must select a video")
                  );
                }
                else {
                  $form_state->setError(
                    $form['field_uw_banner']['widget'][$key]['subform']['field_uw_ban_video']['widget'],
                    t("You must select a video")
                  );
                }
              }
            }
            if (isset($value['subform']['field_uw_ban_image'])) {
              if (!isset($value['subform']['field_uw_ban_image']['selection'])) {
                if (isset($form['settings'])) {
                  $form_state->setError(
                    $form['settings']['block_form']['field_uw_banner_item']['widget'][$key]['subform']['field_uw_ban_image']['widget'],
                    t("You must select an image")
                  );
                }
                else {
                  $form_state->setError(
                    $form['field_uw_banner']['widget'][$key]['subform']['field_uw_ban_image']['widget'],
                    t("You must select an image")
                  );
                }
              }
            }
          }
        }
      }
    }
  }
}

/**
 * Validates submission values for alias on nodes.
 */
function _uw_cfg_common_alias_validate(array &$element, FormStateInterface $form_state) {

  // Trim the submitted value of whitespace and slashes
  // of url alias field.
  $url = rtrim(trim($element['alias']['#value']), " \\/");

  // The url alias field is not empty.
  if ($url !== '') {

    // Set the url entered as the element value.
    $form_state->setValueForElement($element['alias'], $url);

    // Don't allow urls to start with /admin.
    if (preg_match('/^\/(?:admin|node)(?:$|\/)/i', $url)) {
      $form_state->setError($element['alias'], t('The URL must be changed - "@url" starts with /admin or /node.', ['@url' => $url]));
      return;
    }

    // Reserved urls.
    $reserved_urls = [
      '/profile',
      '/service',
      '/users',
    ];

    if (in_array($url, $reserved_urls)) {
      $form_state->setError($element['alias'], t('The alias "@url" is a reserved path that cannot be used.', ['@url' => $url]));
      return;
    }

    // Reserved urls for catalog tabs.
    $reserved_catalogs_urls = [
      'new',
      'updated',
      'popular',
      'category',
      'audience',
      'faculty',
    ];
    $catalogs_path = explode('/', $url);
    if (isset($catalogs_path[1]) &&
      $catalogs_path[1] == 'catalogs' &&
      isset($catalogs_path[3]) &&
      in_array($catalogs_path[3], $reserved_catalogs_urls)) {
      $form_state->setError($element['alias'], t('The alias "@url" is a reserved path that cannot be used.', ['@url' => $url]));
      return;
    }

    // Don't allow external urls.
    if (UrlHelper::isExternal($url)) {
      $form_state->setError($element['alias'], t('URL alias is required to be internal.'));
      return;
    }

    // Check if url has valid format, this is based on regex only.
    // Valid values per RFC 3986.
    if (!UrlHelper::isValid($url)) {
      $form_state->setError($element['alias'], t('URL alias must be a valid format.'));
      return;
    }

    [
      'path' => $path,
      'query' => $query,
      'fragment' => $fragment,
    ] = UrlHelper::parse($url);

    // Check if url has "?".
    if (!empty($query)) {
      $form_state->setError($element['alias'], t('The URL must not contain question marks.'));
      return;
    }

    // Check if url has "#".
    if ($fragment) {
      $form_state->setError($element['alias'], t('The URL must not contain anchors.'));
      return;
    }

    // Check if url has '//'.
    if (str_contains($url, '//')) {
      $form_state->setError($element['alias'], t('The URL must not contain multiple consecutive forward slashes.'));
      return;
    }

    // This is for editing an existing node.
    if ($element['source']['#value'] !== NULL) {
      // Get the url which is saved from '/node/{id}'.
      $saved_url = \Drupal::service('path_alias.manager')->getAliasByPath($element['source']['#value']);

      // Check maybe if url here matches saved url.
      if ($saved_url && $saved_url !== $path && \Drupal::service('path.validator')->getUrlIfValidWithoutAccessCheck($path)) {
        $form_state->setError($element['alias'], t('The URL must be changed - "@url" is already in use on this site.', ['@url' => $url]));
        return;
      }
    }
    // This is for creating a new node.
    elseif (\Drupal::service('path.validator')->getUrlIfValidWithoutAccessCheck($path)) {
      $form_state->setError($element['alias'], t('The URL must be changed - "@url" is already in use on this site.', ['@url' => $url]));
      return;
    }
  }
}

/**
 * Validates submission values for redirects.
 */
function _uw_cfg_common_redirect_destination_validation(
  array &$form,
  FormStateInterface $form_state
) {

  // Get the values from the form state.
  $values = $form_state->getValues();

  // Prevent redirects to pilots or staging URLs.
  if (isset($values['redirect_redirect'][0]['uri'])) {
    $uri = $values['redirect_redirect'][0]['uri'];
    if (preg_match("/^https?:\/\/(?:pilots|staging)\.uwaterloo\.ca\/?.*/", $uri)) {
      $form_state->setError($form['redirect_redirect']['widget'][0], t('Redirecting to a staging/pilots site is not allowed as these are temporary sites not intended for public use. For redirects within a site, please use an internal path.'));
    }
  }
}

/**
 * Validates submission values for banners on nodes.
 */
function _uw_cfg_common_blank_summaries_validation(
  array &$form,
  FormStateInterface $form_state
) {

  // Get the values from the form state.
  $values = $form_state->getValues();

  // Get the node type.
  $node_type = str_replace('node_uw_ct_', '', $form['#form_id']);
  $node_type = str_replace('_edit', '', $node_type);
  $node_type = str_replace('_form', '', $node_type);
  $node_type = str_replace('_item', '', $node_type);
  $node_type = str_replace('_quick_node_clone', '', $node_type);

  // If the node type is an opportunity, we have to give
  // the specific field name, if not just use the field name.
  if ($node_type == 'opportunity') {
    $field_name = 'field_uw_opportunity_position';
  }
  else {
    $field_name = 'field_uw_' . $node_type . '_summary';
  }

  // If the blank summary is not checked and the summary
  // is null, then set an error.
  if (!$values['field_uw_blank_summary']['value']) {
    if ($values[$field_name][0]['value'] == '') {
      $form_state->setError($form[$field_name]['widget'][0], t('Summary/position field is required.'));
    }
  }
}

/**
 * Validates date range on event block via auto list block.
 */
function _uw_cfg_common_date_range_validate(
  array &$form,
  FormStateInterface $form_state
) {

  // Get the values of the form state.
  $values = $form_state->getValues();

  // Only for selecting date range option.
  if ($values['settings']['event']['date'] === 'range') {

    // Get from date if From date is selected.
    if (isset($values['settings']['event']['range']['from'])) {
      $from = $values['settings']['event']['range']['from'];
      $from_date = $from->getTimestamp();
    }
    // Set error message if From date is not selected.
    else {
      $form_state->setErrorByName('settings[event][range][from][date]', t('Please enter From date.'));
    }

    // Get to date if To date is selected.
    if (isset($values['settings']['event']['range']['to'])) {
      $to = $values['settings']['event']['range']['to'];
      $to_date = $to->getTimestamp();
    }
    // Set error message if To date is not selected.
    else {
      $form_state->setErrorByName('settings[event][range][to][date]', t('Please enter To date.'));
    }

    // Set error message if From date is greater than To date.
    if (isset($from_date) && isset($to_date) && $from_date > $to_date) {
      $form_state->setErrorByName('settings[event][range][from][date]', t('From date must be before To date.'));
    }
  }
}

/**
 * Link uri field validation function.
 */
function _uw_cfg_common_uw_link_validator(
  array $element,
  FormStateInterface &$form_state,
  array $form
): void {

  $uri = $element['#value'];

  // Get the field name and set it up in the format used by setErrorByName.
  $fieldname = rtrim($element['#name'], ']');
  $pos = strpos($fieldname, '[');
  $fieldname = substr_replace($fieldname, '][', $pos, 1);

  // Buttons are just not allowed.
  if ($uri == '<button>') {
    $form_state->setErrorByName($fieldname, t('The &lt;button&gt; value is not supported for this field.'));
  }

  if ($uri == '<nolink>') {
    // Don't allow blank link text when <nolink> is used.
    if ($fieldname == 'field_uw_event_host][0][uri') {
      $link_text = $form_state->getValue('field_uw_event_host')[0]['title'] ?? '';
      if ($link_text == '') {
        $form_state->setErrorByName('field_uw_event_host][0][title', t('You must provide link text when using &lt;nolink&gt; as the URL.'));
      }
    }
    else {
      // Nolink is not allowed, with one exception (event host field).
      $form_state->setErrorByName($fieldname, t('The &lt;nolink&gt; value is not supported for this field.'));
    }
  }

  // Prevent adding pilots or staging URLs in link fields.
  if (preg_match("/^https?:\/\/(?:pilots|staging)\.uwaterloo\.ca\/?.*/", $uri)) {
    $form_state->setErrorByName($fieldname, t('Linking to a staging/pilots site is not allowed as these are temporary sites not intended for public use. For links within a site, please use an internal path.'));
  }
}
