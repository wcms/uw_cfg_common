<?php

/**
 * @file
 * Preprocess functions file.
 */

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\node\NodeInterface;

/**
 * Implements hook_preprocess_page().
 */
function uw_cfg_common_preprocess_page(&$variables): void {

  // If this is taxonomy term page, then check catalogs and unpublished.
  if (\Drupal::routeMatch()->getRouteName() === 'entity.taxonomy_term.canonical') {

    // Load the term.
    $term = \Drupal::routeMatch()->getParameter('taxonomy_term');

    // If the term is unpublished and is a catalog,
    // then check if user has permission to edit, and
    // if so, let it through, if not throw 404.
    if (
      !$term->isPublished() &&
      $term->bundle() === 'uw_vocab_catalogs'
    ) {

      // Get the current user.
      $current_user = \Drupal::service('current_user');

      // Check if the user is authenticated, then check if they
      // have the permission to edit.  If not then throw the 404.
      if (
        !$current_user->isAuthenticated() ||
        !$current_user->hasPermission('edit terms in uw_vocab_catalogs')
      ) {
        throw new NotFoundHttpException();
      }
    }
  }
}

/**
 * Implements hook_preprocess_form_element().
 *
 * Allows for use of label_class in form elements and will add
 * any class in label_classes to the label.
 */
function uw_cfg_common_preprocess_form_element(&$variables) {
  if (isset($variables['element']['#label_classes'])) {
    $variables['label']['#attributes']['class'] = $variables['element']['#label_classes'];
  }
}

/**
 * Implements template_preprocess_form_element_label().
 */
function uw_cfg_common_preprocess_form_element_label(&$variables) {

  // Check if we need to add the form required to the label.
  // Conditions are not the blank summary checkbox,
  // the id of the label contains edit-field-uw and has
  // either summary or position in the id.
  if (
    isset($variables['element']['#id']) &&
    $variables['element']['#id'] !== 'edit-field-uw-blank-summary-value' &&
    str_contains($variables['element']['#id'], 'edit-field-uw-') &&
    (
      str_contains($variables['element']['#id'], 'summary') ||
      str_contains($variables['element']['#id'], 'position')
    )
  ) {

    // Try and get the node type, by replacing the id of the label.
    $node_type = $variables['element']['#id'];
    $node_type = str_replace('edit-field-uw-', '', $node_type);
    $node_type = str_replace('-summary-0-value', '', $node_type);
    $node_type = str_replace('-position-0-value', '', $node_type);

    // The node types to place the form required on the label.
    $blank_summary_node_types = [
      'blog',
      'event',
      'news',
      'opportunity',
      'profile',
      'project',
    ];

    // If we are on a node that needs a form required
    // on the label add the class.
    if (in_array($node_type, $blank_summary_node_types)) {
      $variables['attributes']['class'][] = 'form-required';
    }
  }
}

/**
 * Implements hook_preprocess_HOOK().
 */
function uw_cfg_common_preprocess_node(&$variables) {

  // Get the current path.
  $path = explode('/', \Drupal::service('path.current')->getPath());

  // The paths to place the content moderation block on.  Made this
  // an array to future proof, if there are more pages later.
  $paths_for_content_moderation = ['latest'];

  // ISTWCMS-4493: adding class if section has full width.
  // If there is a sidebar on the node, check all sections for full width.
  if (isset($variables['sidebar'])) {

    // Get the layouts from the node.
    $layouts = $variables['node']->layout_builder__layout->getValue();

    // Step through each of the layouts and check for full width.
    foreach ($layouts as $layout) {

      // Get the layout settings from the section.
      $settings = $layout['section']->getLayoutSettings();

      // If the layout builder style is set to full width, then set
      // the classes variable for the node and exit the loop.
      if (isset($settings['layout_builder_styles_style']) &&
        $settings['layout_builder_styles_style'] == "uw_lbs_full_width"
      ) {

        // Add a class to the node for full width on a section.
        $variables['attributes']['class'][] = 'uw-section-has-full-width';

        // Break out of the loop to save computational time.
        break;
      }
    }
  }

  // Check if we are to add the content moderation place.
  if (in_array(end($path), $paths_for_content_moderation)) {

    // Add the content moderation block.
    $variables['uw_content_moderation_form'] = \Drupal::formBuilder()->getForm('Drupal\content_moderation\Form\EntityModerationForm', $variables['node']);
  }
  else {

    $block_manager = \Drupal::service('plugin.manager.block');

    $plugin_block = $block_manager->createInstance('uw_cbl_content_moderation', []);

    $access_result = $plugin_block->access(\Drupal::currentUser());

    // Return empty render array if user doesn't have access.
    // $access_result can be boolean or an AccessResult class.
    if (
      is_object($access_result) &&
      $access_result->isForbidden() ||
      is_bool($access_result)
      && $access_result
    ) {

      $variables['uw_content_moderation_form'] = $plugin_block->build();
    }
  }

  // Set the media flags for the node.
  $variables['media_flags'] = \Drupal::service('uw_cfg_common.uw_service')->uwGetMediaFlags($variables['node'], $variables['view_mode']);

  // Get the content type of the node.
  $content_type = $variables['node']->getType();

  // Call the function to set map variables for event and service.
  _uw_set_map_variables($variables, $content_type);

}

/**
 * Implements hook_preprocess_HOOK().
 */
function uw_cfg_common_preprocess_responsive_image(&$variables) {

  // Get the current path.
  $current_path = \Drupal::service('path.current')->getPath();

  // Explode the current path so we can check where we are.
  $current_path_parts = explode('/', $current_path);

  // Get the media library parameters, we will use this
  // if we are on a media library page/modal.
  $media_lib_parameters = \Drupal::request()->query->get('media_library_opener_parameters');

  // If the current path has a node or media library params,
  // we need to alter the image styles.
  if (
    $current_path_parts[1] == 'node' ||
    $media_lib_parameters
  ) {

    // If we are on a contact image, remove all styles
    // but those for portraits.
    if (
      isset($media_lib_parameters['bundle']) &&
      $media_lib_parameters['bundle'] == 'uw_ct_contact' ||
      end($current_path_parts) == 'uw_ct_contact'
    ) {

      // Get the styles used for portraits.
      $uw_styles = \Drupal::service('uw_cfg_common.uw_service')->getCropImageStyles('portrait');
    }
    else {

      // Get the styles used for responsive.
      $uw_styles = \Drupal::service('uw_cfg_common.uw_service')->getCropImageStyles('responsive');
    }

    // Step through each of the sources and see if we are.
    // to use it.
    foreach ($variables['sources'] as $index => $source) {

      // Get the srcset.
      $srcset = $source->storage()['srcset']->render();

      // Break into parts so that we can check for image styles.
      $srcset_parts = explode('/', $srcset);

      // Step through each of the srcset parts.
      foreach ($srcset_parts as $sp) {

        // Ensure that we are on an image style.
        if (strpos($sp, 'uw_is') !== FALSE) {

          // If not in the list of image styles, remove
          // it from the sources.
          if (!in_array($sp, $uw_styles)) {
            unset($variables['sources'][$index]);
          }
        }
      }
    }
  }
}

/**
 * Implements template_preprocess_html().
 */
function uw_cfg_common_preprocess_html(&$variables): void {
  // Setting the meta tag for web forms getting indexed
  // if not used any content.
  if ($variables['root_path'] === 'webform') {
    $noindex = [
      '#tag' => 'meta',
      '#attributes' => [
        'name' => 'robots',
        'content' => 'noindex',
      ],
    ];
    $variables['page']['#attached']['html_head'][] = [$noindex, 'noindex'];
  }
}

/**
 * Implements template_preprocess_layout.
 */
function uw_cfg_common_preprocess_layout(&$variables) {

  // Get the settings from the content.
  $settings = $variables['content']['#settings'] ?? NULL;

  // If there are settings, then process them to get out
  // the options for the section.
  if (
    $settings &&
    isset($settings['layout_builder_background_type']) &&
    $settings['layout_builder_background_type'] !== 'none'
  ) {

    // Get the background type from the settings.
    $type = $settings['layout_builder_background_type'];

    // If the type is a solid color and the color is set,
    // then add a class to the variables.
    if (
      $type === 'solid_color' &&
      $color = $settings['layout_builder_background_color']
    ) {

      // Add the class to the variables.
      $variables['attributes']['class'][] = 'uw-section__background--' . $color;

      // If there are classes on the section, remove the ones
      // that are put in by default for background image and
      // solid color.
      if (isset($variables['attributes']['class'])) {
        $variables['attributes']['class'] = _uw_cfg_common_remove_extra_classes($variables['attributes']['class']);
      }
    }
    // If the background type is an image, get the image and
    // add the sources to the variables.
    elseif ($type === 'image') {

      // Get the mid from the settings.
      $media_id = $settings['layout_builder_background_image'];

      // If there is a media id, then get the sources for the
      // picture element and to the variables.
      if ($media_id) {

        // The UW service.
        /** @var \Drupal\uw_cfg_common\Service\UWServiceInterface $uw_service */
        $uw_service = \Drupal::service('uw_cfg_common.uw_service');

        // Load the media entity.
        $media_entity = \Drupal::service('entity_type.manager')->getStorage('media')->load($media_id);

        // Set the sources for the picture element to the variables.
        $variables['uw_section']['options']['image'] = $uw_service->prepareResponsiveImage($media_entity, 'uw_ris_media');

        // Fix the issue with portrait crops in the section background image,
        // by removing the portrait cut.
        foreach ($variables['uw_section']['options']['image']['sources'] as $index => $source) {
          if ($source->toArray()['media'] == 'all and (min-width: 1em)') {
            unset($variables['uw_section']['options']['image']['sources'][$index]);
          }
        }

        // Add the class to the section.
        $variables['attributes']['class'][] = 'uw-section__background-image';
      }

      // Add the image tint to the variables.
      $variables['uw_section']['options']['image_tint'] = $settings['layout_builder_background_image_image_tint'] ?? 'none';

      // Add the image tint.
      $tint_color_class = 'uw-section__tint-color--' . $variables['uw_section']['options']['image_tint'];
      $variables['attributes']['class'][] = $tint_color_class;

      // Add the image text color to the variables.
      $variables['uw_section']['options']['image_color'] = $settings['layout_builder_background_image_text_color'] ?? 'white';

      // Add the text color.
      $text_color_class = 'uw-section__text-color--' . $variables['uw_section']['options']['image_color'];
      $variables['attributes']['class'][] = $text_color_class;
    }
  }
  else {

    // If there are classes on the section, remove the ones
    // that are put in by default for background image and
    // solid color.
    if (isset($variables['attributes']['class'])) {
      $variables['attributes']['class'] = _uw_cfg_common_remove_extra_classes($variables['attributes']['class']);
    }
  }

  // Classes to check for.
  $classes['uw-section-spacing'] = 'uw-section-spacing--default';
  $classes['uw-section-separator'] = 'uw-section-separator--none';
  $classes['uw-column-separator'] = 'uw-column-separator--none';

  // Only check if classes exists if there are already claseses.
  // If there are no classes just add them.
  if (isset($variables['attributes']['class'])) {

    // Step through each of the classes and see if we need to
    // add it to the layout.
    foreach ($classes as $needle => $class) {

      // If there is no class for section spacing, add it
      // to the layout classes.
      if (empty(preg_grep('/' . $needle . '/i', $variables['attributes']['class']))) {
        $variables['attributes']['class'][] = $class;
      }
    }
  }
  else {

    // Step through each of the classes and add them.
    foreach ($classes as $class) {
      $variables['attributes']['class'][] = $class;
    }
  }

  // Check if the layout is the one you want to target.
  if ($variables['layout']->id() == 'uw_1_column') {
    // Get the node from the current route.
    $node = \Drupal::routeMatch()->getParameter('node');

    // Ensure the node is an instance of NodeInterface.
    if ($node instanceof NodeInterface) {
      // Get the content type of the node.
      $content_type = $node->getType();

      // Ensure the node is available in the variables array.
      $variables['node'] = $node;

      // Call the function to set map variables for event and service.
      _uw_set_map_variables($variables, $content_type);
    }
  }
}
