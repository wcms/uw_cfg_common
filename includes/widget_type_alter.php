<?php

/**
 * @file
 * Widget type alter functions file.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\media_library\MediaLibraryState;

/**
 * Implements hook_field_widget_single_element_WIDGET_TYPE_form_alter().
 */
function uw_cfg_common_field_widget_single_element_path_form_alter(
  array &$element,
  FormStateInterface $form_state,
  array $context
):void {

  // Add _uw_cfg_common_alias_validate.
  $element['#element_validate'][] = '_uw_cfg_common_alias_validate';
}

/**
 * Implements hook_field_widget_single_element_WIDGET_TYPE_form_alter().
 */
function uw_cfg_common_field_widget_single_element_link_default_form_alter(
  array &$element,
  FormStateInterface $form_state,
  array $context
): void {

  // Get field information from context.
  $field_definition = $context['items']->getFieldDefinition();

  // Set custom description only for event map field.
  if ($field_definition->getName() == 'field_uw_event_map') {
    $element['uri']['#description'] = t('Optional: provide a link to a map with the event location (e.g. https://uwaterloo.ca/map/). This must be an external URL such as https://example.com.');
  }
  else {

    // Set custom description for all link fields except event map.
    $element['uri']['#description'] = t('Start typing the title of a piece of content to select it. You can also enter an internal path such as /blog or an external URL such as https://example.com. Enter &lt;front&gt; to link to the front page.');

    // Set custom description for google maps.
    if ($field_definition->getName() == 'field_gmaps_embedded_url') {
      $element['uri']['#description'] = t('Google Maps starts with https://www.google.com/maps/embed?pb=<br>Google My Maps starts with https://www.google.com/maps/d/u/1/embed?mid=');
    }

    // Set custom description only for event host field.
    if ($field_definition->getName() == 'field_uw_event_host') {
      $element['uri']['#description'] .= ' ' . t('Enter &lt;nolink&gt; to display link text only.');
    }

    // Set custom description only for timeline link field.
    if ($field_definition->getName() == 'field_uw_timeline_link') {
      $element['uri']['#description'] = t('Links the entire content to a URL. If entered, do not use links inside the content itself.') . ' ' . $element['uri']['#description'];
    }

    // Set custom description only for banner link field.
    if ($field_definition->getName() == 'field_uw_ban_link') {
      $element['uri']['#description'] = t('Provide an optional link for this banner.') . ' ' . $element['uri']['#description'];
    }
  }

  // Add link uri field element validation function.
  $element['uri']['#element_validate'][] = '_uw_cfg_common_uw_link_validator';
}

/**
 * Implements hook_field_widget_WIDGET_TYPE_form_alter().
 */
function uw_cfg_common_field_widget_entity_reference_paragraphs_form_alter(
  array &$element,
  FormStateInterface &$form_state,
  array $context
): void {

  // If this is a banner image, add the required manually.
  if ($element['#paragraph_type'] == 'uw_para_image_banner') {

    // If this is not a block, add the class for required
    // on the image, we need to do this so that when using
    // banners with media, the required does not work properly
    // when banners are not selected.
    // If it is a block, just add the required to the element.
    if (!isset($context['form']['#block'])) {

      // If there already is classes set, then add to the array.
      // If no classes yet, add as an array.
      if (isset($element['subform']['field_uw_ban_image']['widget']['#attributes']['class'])) {
        $element['subform']['field_uw_ban_image']['widget']['#attributes']['class'][] = 'form-required';
      }
      else {
        $element['subform']['field_uw_ban_image']['widget']['#attributes']['class'] = ['form-required'];
      }
    }
    else {
      $element['subform']['field_uw_ban_image']['widget']['#required'] = TRUE;
    }
  }
}

/**
 * Implements hook_field_widget_WIDGET_TYPE_form_alter().
 */
function uw_cfg_common_field_widget_media_library_widget_form_alter(
  array &$element,
  FormStateInterface $form_state,
  array $context
): void {

  /** @var \Drupal\Core\Routing\RouteMatchInterface $route_match */
  $route_match = \Drupal::routeMatch();

  if ($route_match->getRouteName() === 'layout_builder.add_block') {

    /** @var \Drupal\media_library\MediaLibraryState $state */
    $state = $element['open_button']['#media_library_state'];

    $openerParameters = $state->getOpenerParameters();
    $openerParameters['plugin_id'] = $route_match->getParameters()->get('plugin_id');

    $new_state = MediaLibraryState::create($state->getOpenerId(), $state->getAllowedTypeIds(), $state->getSelectedTypeId(), $state->getAvailableSlots(), $openerParameters);

    $element['open_button']['#media_library_state'] = $new_state;
  }
}
