<?php

/**
 * @file
 * Webform functions file.
 */

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\webform\WebformInterface;
use Drupal\webform\WebformSubmissionStorageInterface;

/**
 * Implements hook_preprocess_HOOK().
 *
 * Custom access denied messages with login/logout links.
 */
function uw_cfg_common_preprocess_webform_access_denied(array &$variables): void {
  $webform = $variables['webform'];

  $message = NULL;
  switch ($webform->getThirdPartySetting('uw_cfg_common', 'access_control_method')) {
    case 'auth':
    case 'group':
    case 'user':
      // If authenticated access and anonymous user, login.
      if (\Drupal::currentUser()->isAnonymous()) {
        $route = 'user.login';
        $message = 'You must <a href="@url">log in to view this form</a>.';
      }
      break;

    case 'anon':
      // If anonymous access and authenticated user, logout.
      if (\Drupal::currentUser()->isAuthenticated()) {
        $route = 'user.logout';
        $message = 'This form must be completed anonymously. You must <a href="@url">logout to view this form</a>.';
      }
      break;
  }

  // Set a custom message only if a message has been chosen above.
  if ($message) {
    $options = ['query' => \Drupal::destination()->getAsArray()];
    $url = Url::fromRoute($route, [], $options);
    // phpcs:ignore Drupal.Semantics.FunctionT.NotLiteralString
    $message = '<p>' . t($message, ['@url' => $url->toString()]) . '</p>';

    $variables['message']['#markup'] = $message;
  }
}

/**
 * Return the most recent completed Webform submission for a user on a form.
 *
 * @param string $webform_id
 *   The entity ID of the Webform.
 * @param int $uid
 *   The user ID of the user.
 *
 * @return int|null
 *   The submission ID or NULL if there are none.
 */
function uw_cfg_common_get_most_recent_webform_submission(string $webform_id, int $uid): ?int {
  // Load submission IDs from webform_submission storage.
  $webform_submission_storage = \Drupal::entityTypeManager()->getStorage('webform_submission');
  $query = $webform_submission_storage->getQuery();
  $entity_ids = $query
    ->condition('webform_id', $webform_id)
    ->condition('uid', $uid)
    ->condition('in_draft', 0)
    ->sort('created', 'ASC')
    ->accessCheck(FALSE)
    ->execute();

  // If there is at least one, return the last as integer, otherwise NULL.
  return $entity_ids ? (int) end($entity_ids) : NULL;
}

/**
 * Implements hook_fillpdf_populate_pdf_context_alter().
 */
function uw_cfg_common_fillpdf_populate_pdf_context_alter(array &$context): void {
  // If there are no webform_submission entities but there is at least one
  // webform entity, add the most recent submission for each webform.
  // Only do this for authenticated users.
  $current_uid = (int) \Drupal::currentUser()->id();
  if ($current_uid && empty($context['entity_ids']['webform_submission']) && !empty($context['entity_ids']['webform'])) {
    foreach ($context['entity_ids']['webform'] as $webform_id) {
      $entity_id = uw_cfg_common_get_most_recent_webform_submission($webform_id, $current_uid);
      if ($entity_id) {
        $context['entity_ids']['webform_submission'][$entity_id] = $entity_id;
      }
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Configure admin/structure/webform/config/submissions.
 */
function uw_cfg_common_form_webform_admin_config_submissions_form_alter(array &$form, FormStateInterface $form_state, string $form_id): void {
  // Remove undesired features.
  $form['views_settings']['#access'] = FALSE;
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Remove all ability to set CSS classes, CSS styles and custom attributes
 * in admin/structure/webform/manage/{webform_id}.
 */
function uw_cfg_common_form_webform_ui_element_form_alter(array &$form, FormStateInterface $form_state, string $form_id): void {
  // Advanced -> Wrapper Attributes.
  $form['properties']['wrapper_attributes']['#access'] = FALSE;
  // Advanced -> Element Attributes.
  $form['properties']['element_attributes']['#access'] = FALSE;
  // Advanced -> Label Attributes.
  $form['properties']['label_attributes']['#access'] = FALSE;
  // Advanced -> Submission Display -> Display Wrapper Attributes.
  $form['properties']['display']['format_attributes']['#access'] = FALSE;
  // Advanced -> Summary Attributes.
  $form['properties']['summary_attributes']['#access'] = FALSE;
  // Advanced -> Title Attributes.
  $form['properties']['title_attributes']['#access'] = FALSE;
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Configure admin/structure/webform/manage/WEBFORM_ID/access.
 */
function uw_cfg_common_form_webform_settings_access_form_alter(array &$form, FormStateInterface $form_state, string $form_id): void {
  /** @var \Drupal\webform\WebformInterface $webform */
  $webform = $form_state->getFormObject()->getEntity();

  // Choose access control method.
  $form['access']['create']['uw_access_control_method'] = [
    '#type' => 'radios',
    '#title' => t('Choose who can view and submit the form'),
    '#options' => [
      'all' => t('Everyone'),
      'auth' => t('Users who are logged in'),
      'group' => t('Users specified by Active Directory groups'),
      'user' => t('Users specified below'),
      'anon' => t('Users who are logged out (for anonymous submission)'),
    ],
    '#default_value' => $webform->getThirdPartySetting('uw_cfg_common', 'access_control_method') ?: 'all',
    '#required' => TRUE,
    '#weight' => -50,
  ];

  // Access by Active Directory group.
  $form['access']['create']['uw_ad_access'] = [
    '#type' => 'details',
    '#title' => t('Access control by Active Directory group'),
    '#required' => TRUE,
    '#open' => TRUE,
    '#states' => [
      'visible' => [
        'input[name="access[create][uw_access_control_method]"]' => ['value' => 'group'],
      ],
    ],
  ];
  $form['access']['create']['uw_ad_access']['ad_require_groups'] = [
    '#type' => 'textarea',
    '#title' => t('Limit form submission to these Active Directory groups'),
    '#description' => t('Put one Active Directory group per line. To complete the form, the user must be in at least one of these groups. Leave blank to allow everyone.'),
    '#default_value' => implode("\r\n", $webform->getThirdPartySetting('uw_cfg_common', 'ad_require_groups') ?: []),
  ];
  $form['access']['create']['uw_ad_access']['ad_deny_groups'] = [
    '#type' => 'textarea',
    '#title' => t('Prevent form submission for these Active Directory groups'),
    '#description' => t('Put one Active Directory group per line. To complete the form, the user must not be in any of these groups. Leave blank to allow everyone.'),
    '#default_value' => implode("\r\n", $webform->getThirdPartySetting('uw_cfg_common', 'ad_deny_groups') ?: []),
  ];

  // Validate and submit handler to save UW settings.
  $form['actions']['submit']['#validate'][] = '_uw_cfg_common_form_webform_settings_access_form_validate';
  $form['actions']['submit']['#submit'][] = '_uw_cfg_common_form_webform_settings_access_form_submit';

  // Users control is hidden or required, the latter when authz by user.
  $access_rule = [
    'input[name="access[create][uw_access_control_method]"]' => ['value' => 'user'],
  ];
  $form['access']['create']['users']['#states']['required'][] = $access_rule;
  $form['access']['create']['users']['#states']['visible'][] = $access_rule;

  // Remove sections for access control that should not be available.
  $sections_to_remove = [
    'update_any',
    'update_own',
    'delete_own',
    'administer',
    'configuration',
  ];
  foreach ($sections_to_remove as $section) {
    $form['access'][$section]['#access'] = FALSE;
  }

  // Remove all but user-based access for submissions and test.
  $permissions_to_edit = [
    'create',
    'view_any',
    'delete_any',
    'purge_any',
    'view_own',
    'test',
  ];
  $access_types_to_remove = [
    'roles',
    'permissions',
  ];
  foreach ($permissions_to_edit as $permission) {
    foreach ($access_types_to_remove as $type) {
      $form['access'][$permission][$type]['#access'] = FALSE;
    }
  }
}

/**
 * Form validate handler.
 */
function _uw_cfg_common_form_webform_settings_access_form_validate(array $form, FormStateInterface $form_state): void {
  // Validate UW settings.
  $access_control_method = [
    'access',
    'create',
    'uw_access_control_method',
  ];
  $access_control_method = $form_state->getValue($access_control_method);
  switch ($access_control_method) {
    // Validate AD groups.
    case 'group':
      $fields = [
        'ad_require_groups' => NULL,
        'ad_deny_groups' => NULL,
      ];
      foreach (array_keys($fields) as $field) {
        // Get groups.
        $setting = [
          'access',
          'create',
          'uw_ad_access',
          $field,
        ];
        $fields[$field] = uw_cfg_common_array_split_clean($form_state->getValue($setting));
        $form_state->setValue($setting, $fields[$field]);

        // Raise error for invalid groups.
        foreach ($fields[$field] as $group) {
          if (!preg_match('/^[A-Za-z0-9_& -]+$/', $group)) {
            $form_state->setError(
              $form['access']['create']['uw_ad_access'][$field],
              t(
                'Invalid group: %group.',
                ['%group' => substr($group, 0, 100)]
              )
            );
            break;
          }
        }
      }

      // Raise error if no groups are entered.
      if (!array_filter($fields)) {
        $form_state->setError(
          $form['access']['create']['uw_ad_access'],
          t('Provide at least one group to use for access control.')
        );
      }
      // Fall-through.
    case 'all':
    case 'auth':
    case 'anon':
      // Except for case 'user', ensure no user access constraint is set.
      $form_state->setValue(['access', 'create', 'users'], NULL);
      break;
  }
}

/**
 * Form submit handler.
 */
function _uw_cfg_common_form_webform_settings_access_form_submit(
  array $form,
  FormStateInterface $form_state
): void {

  // Save UW settings.
  if ($webform = $form_state->getFormObject()->getEntity()) {

    // Access control method.
    $access_control_method = [
      'access',
      'create',
      'uw_access_control_method',
    ];

    $access_control_method = $form_state->getValue($access_control_method);
    $webform->setThirdPartySetting('uw_cfg_common', 'access_control_method', $access_control_method);

    // Only save groups if that is the access method. Otherwise, they would be
    // saved without having been validated.
    if ($access_control_method === 'group') {

      // AD group access.
      foreach (['ad_require_groups', 'ad_deny_groups'] as $field) {
        $setting = [
          'access',
          'create',
          'uw_ad_access',
          $field,
        ];

        $setting = $form_state->getValue($setting);
        $webform->setThirdPartySetting('uw_cfg_common', $field, $setting);
      }
    }

    $webform->save();
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Configure admin/structure/webform/manage/WEBFORM_ID/settings/confirmation.
 */
function uw_cfg_common_form_webform_settings_confirmation_form_alter(
  array &$form,
  FormStateInterface $form_state,
  string $form_id
): void {

  // Remove undesirable Webform submission confirmation types.
  // The 'modal' type is just a different way to display the message. Disable
  // for consistency.
  unset($form['confirmation_type']['confirmation_type']['#options']['modal']);
  // The 'none' type is only useful along with a custom handler which provides
  // the confirmation message.
  unset($form['confirmation_type']['confirmation_type']['#options']['none']);

  // Remove undesired features.
  $form['confirmation_attributes_container']['#access'] = FALSE;
  $form['back']['back_container']['confirmation_back_attributes_container']['#access'] = FALSE;
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Configure admin/structure/webform/manage/WEBFORM_ID/settings.
 */
function uw_cfg_common_form_webform_settings_form_alter(
  array &$form,
  FormStateInterface $form_state,
  string $form_id
): void {

  // Remove undesired features.
  $form['ajax_settings']['#access'] = FALSE;
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Configure admin/structure/webform/manage/WEBFORM_ID/handlers.
 */
function uw_cfg_common_form_webform_handler_form_alter(
  array &$form,
  FormStateInterface $form_state,
  string $form_id
): void {

  // Add help text to 'SEND FROM (WEBSITE/DOMAIN)' in webform email handler.
  if ($form['#webform_handler_plugin_id'] === 'email') {
    $form['settings']['from']['#description'] = t('This must be an <strong>@uwaterloo.ca</strong> email address for sending to succeed. Please consider using the reply-to email option instead when emails are not limited to campus accounts.');

    // To email: remove 'Site email address' option under Other.
    array_shift($form['settings']['to']['to_mail']['to_mail']['#options']['Other']);

    // From email: remove 'Site email address' option under Other.
    array_shift($form['settings']['from']['from_mail']['from_mail']['#options']['Other']);

    // CC email: remove 'Site email address' option under Other.
    array_shift($form['settings']['to']['cc_mail']['cc_mail']['#options']['Other']);

    // BCC email: remove 'Site email address' option under Other.
    array_shift($form['settings']['to']['bcc_mail']['bcc_mail']['#options']['Other']);

    // Reply-to email: remove 'Site email address' option under Other.
    array_shift($form['settings']['reply_to']['reply_to']['reply_to']['#options']['Other']);

    // Return path: remove 'Site email address' option under Other.
    array_shift($form['settings']['additional']['return_path']['return_path']['#options']['Other']);

    // Sender email: remove 'Site email address' option under Other.
    array_shift($form['settings']['additional']['sender_mail']['sender_mail']['#options']['Other']);
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Configure admin/structure/webform/manage/WEBFORM_ID/settings/form.
 */
function uw_cfg_common_form_webform_settings_form_form_alter(
  array &$form,
  FormStateInterface $form_state,
  string $form_i
): void {
  // Unset the source entity settings in webforms.
  $form['form_behaviors']['form_prepopulate_source_entity']['#access'] = FALSE;
  $form['form_behaviors']['form_prepopulate_source_entity_required']['#access'] = FALSE;
  $form['form_behaviors']['form_prepopulate_source_entity_type']['#access'] = FALSE;

  // Remove undesired features.
  $form['access_denied']['#access'] = FALSE;
  $form['custom_settings']['#access'] = FALSE;
  $form['form_behaviors']['form_autofocus']['#access'] = FALSE;
  $form['form_behaviors']['form_disable_back']['#access'] = FALSE;
  $form['form_behaviors']['form_novalidate']['#access'] = FALSE;
  $form['form_behaviors']['form_required']['#access'] = FALSE;
  $form['form_behaviors']['form_reset']['#access'] = FALSE;
  $form['form_behaviors']['form_submit_back']['#access'] = FALSE;
  $form['form_settings']['form_attributes']['#access'] = FALSE;
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Configure admin/structure/webform/manage/WEBFORM_ID/settings/submissions.
 */
function uw_cfg_common_form_webform_settings_submissions_form_alter(
  array &$form,
  FormStateInterface $form_state,
  string $form_id
): void {

  // Remove undesired features.
  $form['access_denied']['#access'] = FALSE;
  $form['submission_behaviors']['form_convert_anonymous']['#access'] = FALSE;
  $form['submission_behaviors']['submission_log']['#access'] = FALSE;
  $form['submission_behaviors']['token_update']['#access'] = FALSE;
  $form['views_settings']['#access'] = FALSE;
}

/**
 * Implements hook_ENTITY_TYPE_create().
 */
function uw_cfg_common_webform_create(WebformInterface $webform) {

  // Submission purge settings. Set the default to purge drafts after 28 days.
  $webform->setSetting('purge', WebformSubmissionStorageInterface::PURGE_DRAFT);
  $webform->setSetting('purge_days', 28);

  // On admin/structure/webform/manage/FORM/settings/confirmation, default
  // "Confirmation type" to "inline".
  $webform->setSetting('confirmation_type', WebformInterface::CONFIRMATION_INLINE);

  // Set so that uw_cfg_common_webform_build_access_denied_alter() will run.
  // This value is tested for in Webform::preRenderWebformElement().
  // This value appears on
  // admin/structure/webform/manage/WEBFORM_ID/settings/form.
  $webform->setSetting('form_access_denied', WebformInterface::ACCESS_DENIED_PAGE);
}

/**
 * Implements hook_ENTITY_TYPE_access() for webform entities.
 */
function uw_cfg_common_webform_access(
  WebformInterface $webform,
  string $operation,
  AccountInterface $account
): AccessResult {

  // Always allow access for Form editor so they can see the forms they create.
  if ($account->hasPermission('create webform')) {
    return AccessResult::neutral();
  }

  // Allow access to submissions for Form results access.
  if ($account->hasPermission('view any webform submission') && $operation === 'submission_view_any') {
    return AccessResult::neutral();
  }

  switch ($webform->getThirdPartySetting('uw_cfg_common', 'access_control_method')) {
    case 'anon':
      if (!$account->isAnonymous()) {
        return AccessResult::forbidden();
      }
      break;

    case 'auth':
      if (!$account->isAuthenticated()) {
        return AccessResult::forbidden();
      }
      break;

    case 'group':
      // Must be authenticated for group auth.
      if (!$account->isAuthenticated()) {
        return AccessResult::forbidden();
      }
      // Access control by Active Directory group.
      // Convert all groups to lowercase for case-insensitive matching.
      $user_ad_groups = uw_cfg_common_get_user_ad_groups() ?: [];
      $user_ad_groups = array_map('mb_strtolower', $user_ad_groups);
      // Required group. If at least one is provided, the user must be in it.
      $ad_require_groups = $webform->getThirdPartySetting('uw_cfg_common', 'ad_require_groups');
      $ad_require_groups = array_map('mb_strtolower', $ad_require_groups);
      if ($ad_require_groups && !array_intersect($ad_require_groups, $user_ad_groups)) {
        return AccessResult::forbidden();
      }
      // Deny group. If at least one is provided, the user must not be in it.
      $ad_deny_groups = $webform->getThirdPartySetting('uw_cfg_common', 'ad_deny_groups');
      $ad_deny_groups = array_map('mb_strtolower', $ad_deny_groups);
      if ($ad_deny_groups && array_intersect($ad_deny_groups, $user_ad_groups)) {
        return AccessResult::forbidden();
      }
      break;

    case 'user':
      // Must be authenticated for group auth.
      if (!$account->isAuthenticated()) {
        return AccessResult::forbidden();
      }

      // Get all users when selecting 'Users specified below' under
      // admin/structure/webform/manage/WEBFORM_ID/access.
      $create_user_ids = $webform->getAccessRules()['create']['users'];

      // If the logged in user is not a specified user, get access denied.
      if (!in_array($account->id(), $create_user_ids)) {
        return AccessResult::forbidden();
      }
      break;
  }

  return AccessResult::neutral();
}
