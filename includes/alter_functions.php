<?php

/**
 * @file
 * Alter functions file.
 */

use Drupal\Core\Url;

/**
 * Implements hook_sendgrid_integration_categories_alter().
 *
 * Set the category for all sendmail as WCMS.
 */
function uw_cfg_common_sendgrid_integration_categories_alter(
  array $message,
  array $categories
): array {

  $categories = ['WCMS'];

  return $categories;
}

/**
 * Implements hook_toolbar_alter().
 *
 * Remove the Manage link from the toolbar for authenticated users.
 */
function uw_cfg_common_toolbar_alter(array &$items) {

  // Get the current user.
  $current_user = \Drupal::currentUser();

  // Remove the "manage" people for non-admin users.
  if (!$current_user->hasPermission('access manage toolbar item')) {
    // Remove "Manage" toolbar item.
    unset($items['administration']);
  }

  // Add "people" and "reports" links to "Workbench".
  // Note: 'dashboards' is renamed in
  // uw_dashboard_toolbar_alter().
  $links = [
    'entity.user.collection' => t('People'),
    'system.admin_reports' => t('Reports'),
  ];

  foreach ($links as $route => $title) {

    $url = Url::fromRoute($route);

    if ($url->access()) {
      $items['dashboards']['tray']['dashboards']['#links'][] = [
        'title' => $title,
        'url' => $url,
      ];
    }
  }
}

/**
 * Implements hook_views_plugins_field_alter().
 */
function uw_cfg_common_views_plugins_field_alter(array &$plugins): void {

  // Replace Drupal\views\Plugin\views\field\Dropbutton with UW version.
  $plugins['dropbutton']['class'] = 'Drupal\uw_cfg_common\Plugin\views\field\UWDropbutton';
}

/**
 * Implements hook_entity_type_alter().
 */
function uw_cfg_common_entity_type_alter(array &$entity_types) {

  // Add validation constraint to the node entity.
  $entity_types['node']->addConstraint('UwMedia');
}
