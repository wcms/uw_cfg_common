<?php

/**
 * @file
 * Form id functions file.
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\uw_cfg_common\Service\UWService;

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Remove the None option from layout builder styles.
 */
function uw_cfg_common_form_layout_builder_configure_section_alter(
  array &$form,
  FormStateInterface $form_state,
  string $form_id
): void {

  // Column separator group should not appear one column.
  if (isset($form['layout_builder_style_column_separator'])) {
    // Ensuring that none is selected for column separator by default.
    if (!isset($form['layout_builder_style_column_separator']['#default_value'])) {
      $form['layout_builder_style_column_separator']['#default_value'] = 'uw_lbs_column_separator_none';
    }
  }

  // Ensuring that none is selected for section separator by default.
  if (!isset($form['layout_builder_style_section_separator']['#default_value'])) {
    $form['layout_builder_style_section_separator']['#default_value'] = 'uw_lbs_section_separator_none';
  }

  // Ensuring that default is selected for section spacing by default.
  if (!isset($form['layout_builder_style_section_spacing']['#default_value'])) {
    $form['layout_builder_style_section_spacing']['#default_value'] = 'uw_lbs_section_spacing_default';
  }

  // Ensuring that the contained width is selected by default.
  if (!isset($form['layout_builder_style_default']['#default_value'])) {
    $form['layout_builder_style_default']['#default_value'] = 'uw_lbs_contained_width';
  }

  // Layout builder style - background style default and removing
  // empty(none) option.
  if ($form['layout_builder_style_section_background']['#default_value'] === NULL) {
    $form['layout_builder_style_section_background']['#default_value'] = 'uw_lbs_section_background_full_width';
  }

  // Ensuring that the top align by is set by default.
  if (!isset($form['layout_builder_style_section_alignment']['#default_value'])) {
    $form['layout_builder_style_section_alignment']['#default_value'] = 'uw_lbs_section_alignment_top_align_content';
  }

  unset($form['layout_builder_style_section_background']['#empty_option']);

  $config = $form_state->getFormObject()->getLayout()->getConfiguration();
  $set_background_options = TRUE;

  /** @var \Drupal\layout_builder\Form\UpdateBlockForm $form_object */
  $form_object = $form_state->getFormObject();

  // Get contexts from form state.
  $contexts = $form_object->getSectionStorage()->getContexts();

  if (isset($contexts['entity'])) {

    // Get entity from the contexts.
    $entity = $contexts['entity']->getContextValue();

    if ($entity instanceof EntityInterface) {

      // No set background options for ec content type.
      if ($entity->getEntityTypeId() === 'node' && $entity->bundle() === 'uw_ct_expand_collapse_group') {

        $set_background_options = FALSE;
      }
    }
  }

  // Set background options for all except ec content type.
  if ($set_background_options) {

    $form['layout_builder_background_type'] = [
      '#type' => 'select',
      '#title' => t('Background'),
      '#options' => [
        'none' => t('None'),
        'solid_color' => t('Solid colour'),
        'image' => t('Image'),
      ],
      '#weight' => 80,
      '#default_value' => $config['layout_builder_background_type'] ?? 'none',
    ];

    $form['layout_builder_background_color'] = [
      '#type' => 'select',
      '#title' => t('Background colour'),
      '#options' => [
        'neutral' => t('Light grey'),
        'org-default' => t('UW Gold'),
        'org-default-b' => t('UW Black'),
        'org-art' => t('Orange (Arts)'),
        'org-eng' => t('Purple (Engineering)'),
        'org-env' => t('Green (Environment)'),
        'org-ahs' => t('Teal (Health)'),
        'org-mat' => t('Pink (Mathematics)'),
        'org-sci' => t('Blue (Science)'),
        'org-school' => t('Red (school)'),
        'org-cgc' => t('Red (Conrad Grebel University College)'),
        'org-ren' => t('Green (Renison University College)'),
        'org-stj' => t('Green (St. Jerome’s University)'),
        'org-stp' => t('Blue (United College)'),
      ],
      '#weight' => 81,
      '#default_value' => $config['layout_builder_background_color'] ?? 'neutral',
      '#states' => [
        'visible' => [
          ':input[name="layout_builder_background_type"]' => ['value' => 'solid_color'],
        ],
      ],
    ];

    $form['layout_builder_background_image'] = [
      '#type' => 'media_library',
      '#allowed_bundles' => ['uw_mt_image'],
      '#title' => t('Background image'),
      '#default_value' => $config['layout_builder_background_image'] ?? NULL,
      '#description' => t('This image should be at least 1000x400 pixels, and will be proportionally scaled as needed to ensure it covers the entire section. This may mean that the top and bottom or left and right of the image get cut off. Images should not contain text or any other content that would require a description, and must be selected so that text over top can maintain sufficient colour contrast.'),
      '#weight' => 82,
      '#states' => [
        'visible' => [
          ':input[name="layout_builder_background_type"]' => ['value' => 'image'],
        ],
      ],
    ];

    $form['layout_builder_background_image_image_tint'] = [
      '#type' => 'select',
      '#title' => t('Image tint'),
      '#options' => [
        'none' => t('None'),
        'black' => t('Black (darken)'),
        'white' => t('White (lighten)'),
      ],
      '#weight' => 83,
      '#default_value' => $config['layout_builder_background_image_image_tint'] ?? 'none',
      '#description' => t('For some images, tinting the image darker or lighter can improve the accessibility and readability of the overlaid content.'),
      '#states' => [
        'visible' => [
          ':input[name="layout_builder_background_type"]' => ['value' => 'image'],
        ],
      ],
    ];

    $form['layout_builder_background_image_text_color'] = [
      '#type' => 'select',
      '#title' => t('Text colour'),
      '#options' => [
        'white' => t('White'),
        'white-black-shadow' => t('White with black shadow'),
        'black' => t('Black'),
        'black-white-shadow' => t('Black with white shadow'),
      ],
      '#weight' => 84,
      '#default_value' => $config['layout_builder_background_image_text_color'] ?? 'white',
      '#description' => t('Make sure to select the option that provides the best contrast from the image underneath.'),
      '#states' => [
        'visible' => [
          ':input[name="layout_builder_background_type"]' => ['value' => 'image'],
        ],
      ],
    ];
  }

  // Adding #states to layout builder styles created using UI.
  $form['layout_builder_style_section_background']['#states'] = [
    'visible' => [
      ':input[name="layout_builder_background_type"]' => ['value' => 'image'],
      ':input[name="layout_builder_style_default"]' => ['!value' => 'uw_lbs_full_width'],
    ],
  ];

  $form['#validate'][] = 'uw_cfg_common_validate_section_configuration_validate';

  array_unshift($form['#submit'], 'uw_cfg_common_submit_section_configuration_submit');
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Set the default of preview mode disabled.
 */
function uw_cfg_common_form_node_type_add_form_alter(
  &$form,
  FormStateInterface $form_state,
  string $form_id
) {

  $form['submission']['preview_mode']['#default_value'] = 0;
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Node edit form: node/NID/edit.
 *
 * Prevent certain changes to the home page.
 */
function uw_cfg_common_form_node_uw_ct_web_page_edit_form_alter(
  array &$form,
  FormStateInterface $form_state,
  string $form_id
): void {

  // No changes for those with access.
  if (\Drupal::currentUser()->hasPermission('bypass home page protection')) {
    return;
  }

  // Do not allow the home page to be parent of any item.
  unset($form['menu']['link']['menu_parent']['#options']['main:uw_base_profile.front_page']);

  // Early return if not editing home page.
  $nid = (int) \Drupal::routeMatch()->getRawParameter('node');
  if (!UWService::nodeIsHomePage($nid)) {
    return;
  }

  // Remove access to certain controls.
  $form['path']['#access'] = FALSE;
  $form['promote']['#access'] = FALSE;
  $form['sticky']['#access'] = FALSE;

  // For 'menu', setting #access did not work for non-admins. So, also hide the
  // sub-components and make it a container so that nothing appears on the page.
  $form['menu']['#access'] = FALSE;
  $form['menu']['#type'] = 'container';
  $form['menu']['enabled']['#access'] = FALSE;
  $form['menu']['link']['#access'] = FALSE;

  // Hide delete link if no access. This should happen by itself, but does not.
  if (!$form['actions']['delete']['#url']->access()) {
    $form['actions']['delete']['#access'] = FALSE;
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Menu edit form: admin/structure/menu/manage/{menu}.
 */
function uw_cfg_common_form_menu_edit_form_alter(
  array &$form,
  FormStateInterface $form_state,
  string $form_id
): void {

  // Hide links to menu edit and delete for non-admin.
  if (!\Drupal::currentUser()->hasPermission('administer menu')) {
    foreach (Element::children($form['links']['links']) as $element_key) {
      $form['links']['links'][$element_key]['operations']['#access'] = FALSE;
    }
  }

  // Prevent certain changes to the home page.
  //
  // No changes for those with access.
  if (\Drupal::currentUser()->hasPermission('bypass home page protection')) {
    return;
  }

  // Return early if not editing "Main navigation" menu.
  if (!isset($form['links']['links']['menu_plugin_id:uw_base_profile.front_page'])) {
    return;
  }

  // Remove access to home page controls.
  $form['links']['links']['menu_plugin_id:uw_base_profile.front_page']['enabled']['#access'] = FALSE;
  $form['links']['links']['menu_plugin_id:uw_base_profile.front_page']['operations']['#access'] = FALSE;
  $form['links']['links']['menu_plugin_id:uw_base_profile.front_page']['weight']['#access'] = FALSE;

  // Make home page not draggable.
  $key = array_search('draggable', $form['links']['links']['menu_plugin_id:uw_base_profile.front_page']['#attributes']['class'], TRUE);
  unset($form['links']['links']['menu_plugin_id:uw_base_profile.front_page']['#attributes']['class'][$key]);
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Menu link edit form: admin/structure/menu/item/ID/edit.
 *
 * Do not allow the home page to be parent of any item.
 */
function uw_cfg_common_form_menu_link_content_menu_link_content_form_alter(
  array &$form,
  FormStateInterface $form_state,
  string $form_id
): void {

  // No changes for those with access.
  if (\Drupal::currentUser()->hasPermission('bypass home page protection')) {
    return;
  }

  // Do not allow the home page to be parent of any item.
  unset($form['menu_parent']['#options']['main:uw_base_profile.front_page']);
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Disable user url alias.
 */
function uw_cfg_common_form_user_register_form_alter(
  array &$form,
  FormStateInterface $form_state,
  string $form_id
): void {

  // Disable the user url alias, we do not want to remove it
  // or visual hide it as we still want it to be submitted
  // and saved.
  $form['path']['#access'] = FALSE;
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Configure admin/structure/fillpdf/FID.
 */
function uw_cfg_common_form_fillpdf_form_edit_form_alter(
  array &$form,
  FormStateInterface $form_state,
  string $form_id
): void {

  // We only use FillPDF with Webform, so hide the entity type selector and
  // change the title of the entity selector.
  $form['default_entity_type']['#access'] = FALSE;
  $form['default_entity_id']['#title'] = t('Default Webform');
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Configure admin/config/services/mailchimp.
 */
function uw_cfg_common_form_mailchimp_admin_settings_alter(
  array &$form,
  FormStateInterface $form_state,
  string $form_id
): void {
  // Set 'Use OAuth Authentication' is checked.
  $form['use_oauth']['#default_value'] = TRUE;

  // Set 'Mailchimp API Key' empty as default due to use oauth.
  $form['api_key']['#default_value'] = '';

  // Set 5 as mailchimp API timeout default value.
  $form['api_timeout']['#default_value'] = 5;

  // Site owner has 'administer mailchimp' role, but still cannot
  // see the below four fields. Only administrators can view all fields.
  // And site owner can see 'Use OAuth Authentication', but readonly.
  if (!(\Drupal::currentUser()->hasPermission('administer site configuration'))) {
    $form['use_oauth']['#disabled'] = TRUE;
    $form['api_timeout']['#access'] = FALSE;
    $form['connected_sites']['#access'] = FALSE;
    $form['batch']['#access'] = FALSE;
    $form['webhook_hash']['#access'] = FALSE;
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Configure admin/config/services/mailchimp/oauth.
 */
function uw_cfg_common_form_mailchimp_admin_oauth_settings_alter(
  array &$form,
  FormStateInterface $form_state,
  string $form_id
): void {

  /* Set the domain value as default in website domain field
   * checking whether we are on Pantheon first.
   */
  if (isset($_ENV['PANTHEON_ENVIRONMENT'])) {
    switch ($_ENV['PANTHEON_ENVIRONMENT']) {
      case 'dev':
        $form['domain']['#default_value'] = 'wcms.dev';
        break;

      case 'test':
        $form['domain']['#default_value'] = 'staging.uwaterloo.ca';
        break;

      case 'live':
        $form['domain']['#default_value'] = 'uwaterloo.ca';
        break;

    }
  }
  // Site owner can see domain field, but readonly.
  if (!(\Drupal::currentUser()->hasPermission('administer site configuration'))) {
    $form['domain']['#disabled'] = TRUE;
  }
}
