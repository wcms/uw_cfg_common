<?php

/**
 * @file
 * Views functions file.
 */

use Drupal\Core\Url;
use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\ViewExecutable;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Implements hook_views_pre_build().
 */
function uw_cfg_common_views_pre_build(ViewExecutable $view) {

  // If on an events view, ensure that parameters are correct.
  if ($view->id() == 'uw_view_events') {

    /** @var \Symfony\Component\HttpFoundation\Request $request */
    $request = $view->getRequest();

    /** @var \Drupal\Core\Http\InputBag $query */
    $query = $request->query;

    // Get all the parameters from the request.
    $parameters = $query->all();

    // Flag to see if parameter is removed.
    $parameter_remove = FALSE;

    // Step through each of the parameters to see
    // if we need to remove them.
    foreach ($parameters as $index => $value) {

      // If this is a date and not an array, we need
      // to remove the parameter.
      if ($index == 'date' && !is_array($value)) {

        // Remove the date parameter and set the flag.
        unset($parameters['date']);
        $parameter_remove = TRUE;
      }

      // If the parameter is type or audience, ensure that it is
      // an array for the wcms3 filters.
      if (
        ($index == 'type' || $index == 'audience') &&
        !is_array($parameters[$index])
      ) {

        // Unset the parameter and set the flag.
        unset($parameters[$index]);
        $parameter_remove = TRUE;
      }

      // If the flag is set that parameters were removed,
      // then set a redirect to the same page with the
      // correct parameters.
      if ($parameter_remove) {

        // Get the request uri and remove everything after the ?.
        $url = strtok($request->getRequestUri(), '?');

        // Set the internal url using the parameters.
        $url = Url::fromUri('internal:' . $url, ['query' => $parameters]);

        // Set up the redirect and send it.
        $redirect = new RedirectResponse($url->toString());
        $redirect->send();
      }
    }
  }
}

/**
 * Implements hook_views_query_alter().
 */
function uw_cfg_common_views_query_alter(
  ViewExecutable $view,
  QueryPluginBase $query
) {

  // Redirect page default sort updated, added redirect id to sort, on top of
  // created datetime sort.
  if ($view->id() === 'redirect') {
    $query->addOrderBy('redirect', 'rid');
  }
}
