<?php

/**
 * @file
 * Fill pdf functions file.
 */

use Drupal\fillpdf\Form\FillPdfFormForm;

/**
 * Implements hook_fillpdf_form_form_pre_form_build_alter().
 */
function uw_cfg_common_fillpdf_form_form_pre_form_build_alter(
  FillPdfFormForm $fillpdf_form_form
): void {

  // We only use FillPDF with Webform, so set the default entity type for any
  // FillPDF form that does not have one to 'webform'.
  $fillpdf_form = $fillpdf_form_form->getEntity();
  $default_entity_type = $fillpdf_form->getDefaultEntityType();

  if (!$default_entity_type) {
    $fillpdf_form->set('default_entity_type', 'webform')->save();
  }
}
