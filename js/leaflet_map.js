/**
 * @file
 * Javascript for leaflet map.
 */

(function (Drupal, drupalSettings) {
  Drupal.behaviors.leafletCustom = {
    attach: function (context, settings) {
      const mapContainer = document.getElementById('map');
      if (mapContainer && typeof map !== 'undefined') {
        const markers = document.querySelectorAll('.leaflet-marker-icon');

        // Access the coordinates from drupalSettings.
        const latitude = drupalSettings.uw_cfg_common.latitude;
        const longitude = drupalSettings.uw_cfg_common.longitude;
        const address_line1 = drupalSettings.uw_cfg_common.address_line1;
        const organization = drupalSettings.uw_cfg_common.organization;
        const locality = drupalSettings.uw_cfg_common.locality;
        const administrative_area = drupalSettings.uw_cfg_common.administrative_area;
        const contry_code = drupalSettings.uw_cfg_common.contry_code;

        if (latitude && longitude) {
          markers.forEach(marker => {
            if (organization) {
              marker.alt = organization;
            } else if (address_line1) {
              marker.alt = address_line1 + ', ' + locality + ', ' + administrative_area + ' ' + contry_code;
            } else {
              marker.alt = latitude + ', ' + longitude;
            }
          });
        }
      }
      else {
        console.error('Map container not found or map is not defined');
      }
    }
  };
})(Drupal, drupalSettings);
