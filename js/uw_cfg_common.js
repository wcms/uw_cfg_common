/**
 * @file
 * Javascript for uw_cfg_common.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.uw_cfg_common = {
    attach: function (context, drupalSettings) {

      // On document ready, check if the page has the page title
      // as big text and if so enable/disable the big text
      // in the banner image.
      $(document).ready(function () {
        setBigText();
      });

      // When the big text in banner images is ready, check if it
      // needs to be able enable/disabled based on page title.
      $('input[name*="[subform][field_uw_ban_big_text]"]').ready(function () {
        setBigText();
      });

      // When the page title option has been changed set the big text
      // in banner based on inputs.
      $('#edit-field-uw-page-title-big-text').change(function () {
        setBigText();
      });

      // When the text overlay option has been changed set the big text
      // in banner based on inputs.
      $('#edit-field-uw-text-overlay-style').change(function () {
        setBigText();
      });

      // Function to set big text in banner image.
      function setBigText() {

        // If not single page style banner and page title big text
        // then disable big text in banner and set value to blank.
        // Otherwise enable big text in banner.
        if (
          $('#edit-field-uw-page-title-big-text').val() == 1
        ) {
          $('input[name*="[subform][field_uw_ban_big_text]"]').each(function () {
            $(this).prop("disabled", true);
            $(this).val('');
          });
        }
        else {
          $('input[name*="[subform][field_uw_ban_big_text]"]').each(function () {
            $(this).prop("disabled", false);
          });
        }

        // Set all the other text to hidden.
        $('div[id*="subform-field-uw-other-text-wrapper"]').each(function () {
          $(this).hide();
        });

        // Show all the small text.
        $('div[id*="subform-field-uw-ban-image-wrapper"] ~ div[id*="subform-field-uw-ban-small-text-wrapper"]').each(function () {
          $(this).show();
        });

        // If this is a split (single page) style, hide the small text and
        // show the other text.
        if (
          $('#edit-field-uw-text-overlay-style').val() == 'split'
        ) {

          // Show the other text.
          $('div[id*="subform-field-uw-ban-image-wrapper"] ~ div[id*="subform-field-uw-other-text-wrapper"]').each(function () {
            $(this).show();
          });

          // Hide the small text.
          $('div[id*="subform-field-uw-ban-image-wrapper"] ~ div[id*="subform-field-uw-ban-small-text-wrapper"]').each(function () {
            $(this).hide();
          });
        }
      }
    }
  }
})(jQuery, Drupal, drupalSettings);
