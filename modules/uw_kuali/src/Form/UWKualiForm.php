<?php

namespace Drupal\uw_kuali\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\uw_kuali\Service\UWKualiServicesInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the uw kuali entity edit forms.
 */
class UWKualiForm extends ContentEntityForm {

  /**
   * Kuali service.
   *
   * @var \Drupal\uw_kuali\Service\UWKualiServicesInterface
   */
  protected UWKualiServicesInterface $kualiServices;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected AccountProxyInterface $currentUser;

  /**
   * Constructs a ContentEntityForm object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\uw_kuali\Service\UWKualiServicesInterface $kualiServices
   *   Kuali service.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   Current user.
   */
  public function __construct(
    EntityRepositoryInterface $entity_repository,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    TimeInterface $time,
    UWKualiServicesInterface $kualiServices,
    AccountProxyInterface $current_user,
  ) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->currentUser = $current_user;
    $this->kualiServices = $kualiServices;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('uw_kuali.catalogs'),
      $container->get('current_user'),
    );
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $entity = $this->buildEntity($form, $form_state);
    $url = $entity->get('url')->value;

    if (UrlHelper::isExternal($url)) {
      $form_state->setErrorByName('url', 'The URL cannot be external.');
    }

    // Check if url has valid format, this is based on regex only.
    // Valid values per RFC 3986.
    if (!preg_match(UWKualiSettingsForm::PATH_REGEX_FORMAT, $url)) {
      $form_state->setErrorByName('url', 'The URL must be a valid format. Allowed subsets are small letter, numbers, dash (-), and underscore(_).');
    }

    if ($catalog_id = $entity->get('catalog_id')->value) {
      if (!preg_match('/^[a-z0-9]+$/i', $catalog_id)) {
        $form_state->setErrorByName('catalog_id', 'The catalog ID must be a valid format. Allowed subsets are small and capital letters, and numbers.');
      }
    }

    return parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\uw_kuali\UWKualiInterface $entity */
    $entity = $this->getEntity();

    // Update entity created_by field with current user.
    $entity->setUpdatedById(NULL);

    $form_state->setRedirect($this->getKualiRedirect($entity->get('career')->value, $entity->get('status')->value));

    // Check if catalog is live, and new. Then depending on that do redirect,
    // and also update previously live catalog to archive.
    if (($entity->get('status')->value === 'live') && $entity->isNew()) {
      $old_live = $this->kualiServices->getActiveCatalog($entity->getAcademicCareer());

      if ($old_live) {
        $old_live->setUpdatedById($this->currentUser->id());
        $old_live->set('status', 'archive');
        $old_live->save();
      }
    }

    $result = parent::save($form, $form_state);

    if ($result) {
      $this->kualiServices->modifyKualiMenuItem();
    }

    $message_arguments = ['%label' => $entity->label()];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()
          ->addStatus($this->t('New kuali %label has been created.', $message_arguments));
        $this->logger('uw_kuali')
          ->notice('Created new kuali %label', $message_arguments);
        break;

      case SAVED_UPDATED:
        $this->messenger()
          ->addStatus($this->t('The kuali %label has been updated.', $message_arguments));
        $this->logger('uw_kuali')
          ->notice('Updated kuali %label.', $message_arguments);
        break;
    }

    return $result;
  }

  /**
   * Get the Kuali redirect URL based on career and status.
   *
   * @param string $career
   *   The career of the user.
   * @param string $status
   *   The status of the redirect.
   *
   * @return string
   *   The Kuali redirect URL.
   */
  private function getKualiRedirect(string $career, string $status): string {

    if ($career === 'UG') {
      if ($status === 'live') {
        $redirect = 'uw_kuali.live_catalog_ug';
      }
      else {
        $redirect = 'view.kuali.kuali_ug';
      }
    }
    else {
      if ($status === 'live') {
        $redirect = 'uw_kuali.live_catalog_grd';
      }
      else {
        $redirect = 'view.kuali.kuali_grd';
      }
    }

    return $redirect;
  }

}
