<?php

namespace Drupal\uw_kuali\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\uw_kuali\Service\UWKualiServicesInterface;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Uw Kuali settings for this site.
 */
class UWKualiSettingsForm extends ConfigFormBase {

  public const SETTINGS = 'uw_kuali.settings';

  // Validation rule for live catalogs.
  public const PATH_REGEX_FORMAT = '/^[a-z0-9][a-z\-_0-9\/]*[a-z0-9]$/';

  // Paths that cannot be used for live catalogs.
  public const RESERVED_PATHS = [
    'admin', 'node', 'profile', 'service',
    'user', 'blog', 'contact', 'news', 'events',
  ];

  /**
   * Route builder.
   *
   * @var \Drupal\Core\Routing\RouteBuilderInterface
   */
  protected RouteBuilderInterface $routeBuilder;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected AccountProxyInterface $currentUser;

  /**
   * Route provider, used to check if route exists.
   *
   * @var \Drupal\Core\Routing\RouteProviderInterface
   */
  protected RouteProviderInterface $routeProvider;

  /**
   * Kuali service.
   *
   * @var \Drupal\uw_kuali\Service\UWKualiServicesInterface
   */
  protected UWKualiServicesInterface $kualiService;

  /**
   * Constructs a UW Kuali object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Configuration.
   * @param \Drupal\Core\Routing\RouteBuilderInterface $route_builder
   *   Route builder service from core.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   Current logged-in user.
   * @param \Drupal\Core\Routing\RouteProviderInterface $routeProvider
   *   Route provider to check for routes.
   * @param \Drupal\uw_kuali\Service\UWKualiServicesInterface $kualiServices
   *   Kuali service for menu items.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    RouteBuilderInterface $route_builder,
    AccountProxyInterface $current_user,
    RouteProviderInterface $routeProvider,
    UWKualiServicesInterface $kualiServices,
  ) {
    parent::__construct($config_factory);
    $this->routeBuilder = $route_builder;
    $this->currentUser = $current_user;
    $this->routeProvider = $routeProvider;
    $this->kualiService = $kualiServices;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'uw_kuali_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    // Module's configuration.
    $settings = $this->config(static::SETTINGS);
    $parent_form_used = FALSE;

    $form['kuali'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => 'layout-container',
      ],
    ];

    $form['kuali']['ug'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Undergraduate studies'),
      '#attributes' => [
        'class' => 'layout-column layout-column--half',
      ],
    ];

    // For administrators, use states to show/hide parts of module's config.
    if ($this->currentUser->hasPermission('enable uw_kuali')) {
      // This appends "Save configuration" button to the form for admins.
      $form = parent::buildForm($form, $form_state);
      $parent_form_used = TRUE;

      $form['kuali']['ug']['kuali_ug_enabled'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Enabled'),
        '#default_value' => $settings->get('kuali_ug_enabled'),
      ];

      // Using #states to control show/hide a container with all config,
      // this is only for admin users.
      $form['kuali']['ug']['container'] = [
        '#type' => 'container',
        '#states' => [
          'visible' => [
            ':input[name="kuali_ug_enabled"]' => ['checked' => TRUE],
          ],
        ],
      ];
    }
    else {
      $kuali_status = $settings->get('kuali_ug_enabled');

      if ($kuali_status) {
        $kuali_status_desc = $this->t('Kuali integration is enabled.');

        // Add form defaults, including "Save configuration" only when kuali
        // is enabled for non admin users (site owner).
        $form = parent::buildForm($form, $form_state);
        $parent_form_used = TRUE;
      }
      else {
        $kuali_status_desc = $this->t('Kuali undergraduate integration is not enabled. To enable it, <a href="https://uwaterloo.atlassian.net/servicedesk/customer/portal/117">submit a request to WCMS support</a>.');
      }

      $form['kuali']['ug']['kuali_ug_status'] = [
        '#markup' => $kuali_status_desc,
      ];

      // Controlling hide/show for the container, all kuali config is inside
      // the container. If kuali is disabled, for non admin users parent
      // container will be hidden, not allowing any changes.
      $form['kuali']['ug']['container'] = [
        '#type' => $kuali_status ? 'container' : 'hidden',
      ];
    }
    $form['kuali']['ug']['container']['ug_page_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Page title / Menu item'),
      '#default_value' => $settings->get('ug_page_title') ?? 'Undergraduate Studies',
      '#description' => $this->t('Enter text for the title of the Kuali page. This also sets the title of the menu item.'),
      '#required' => TRUE,
    ];

    $form['kuali']['ug']['container']['ug_url_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL for live catalog'),
      '#default_value' => $settings->get('ug_url_path') ?? 'undergraduate-studies',
      '#required' => TRUE,
    ];

    $form['kuali']['ug']['container']['ug_archive_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Archives path'),
      '#default_value' => $settings->get('ug_archive_path') ?? 'archive',
      '#description' => $this->t('The path to archive the kuali catalogs. Example: "archives". Leading and trailing slash will be added automatically.'),
      '#required' => TRUE,
    ];

    $form['kuali']['grd'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Graduate studies'),
      '#attributes' => [
        'class' => 'layout-column layout-column--half',
      ],
    ];

    // For administrators, use states to show/hide parts of module's config.
    if ($this->currentUser->hasPermission('enable uw_kuali')) {
      // This appends "Save configuration" button to the form for admins.
      if (!$parent_form_used) {
        $form = parent::buildForm($form, $form_state);
      }

      $form['kuali']['grd']['kuali_grd_enabled'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Enabled'),
        '#default_value' => $settings->get('kuali_grd_enabled'),
      ];

      // Using #states to control show/hide a container with all config,
      // this is only for admin users.
      $form['kuali']['grd']['container'] = [
        '#type' => 'container',
        '#states' => [
          'visible' => [
            ':input[name="kuali_grd_enabled"]' => ['checked' => TRUE],
          ],
        ],
      ];
    }
    else {
      $kuali_grd_status = $settings->get('kuali_grd_enabled');

      if ($kuali_grd_status) {
        $kuali_grd_status_desc = $this->t('Kuali GRD integration is enabled.');

        // Add form defaults, including "Save configuration" only when kuali
        // is enabled for non admin users (site owner).
        if (!$parent_form_used) {
          $form = parent::buildForm($form, $form_state);
        }
      }
      else {
        $kuali_grd_status_desc = $this->t('Kuali graduate integration is not enabled. To enable it, <a href="https://uwaterloo.atlassian.net/servicedesk/customer/portal/117">submit a request to WCMS support</a>.');
      }

      $form['kuali']['grd']['kuali_grd_status'] = [
        '#markup' => $kuali_grd_status_desc,
      ];

      // Controlling hide/show for the container, all kuali config is inside
      // the container. If kuali is disabled, for non admin users parent
      // container will be hidden, not allowing any changes.
      $form['kuali']['grd']['container'] = [
        '#type' => $kuali_grd_status ? 'container' : 'hidden',
      ];
    }

    $form['kuali']['grd']['container']['grd_page_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Page title / Menu item'),
      '#default_value' => $settings->get('grd_page_title') ?? 'Graduate Studies',
      '#description' => $this->t('Enter text for the title of the Kuali page. This also sets the title of the menu item.'),
      '#required' => TRUE,
    ];

    $form['kuali']['grd']['container']['grd_url_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL for live catalog'),
      '#default_value' => $settings->get('grd_url_path') ?? 'graduate-studies',
      '#required' => TRUE,
    ];

    $form['kuali']['grd']['container']['grd_archive_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Archives path'),
      '#default_value' => $settings->get('grd_archive_path') ?? 'archive',
      '#description' => $this->t('The path to archive the kuali catalogs. Example: "archives". Leading and trailing slash will be added automatically.'),
      '#required' => TRUE,
    ];

    // "Save configuration" will be visible only when kuali is enabled, or
    // when user has role administrator. This is controlled by calling parent,
    // check code above. This was to avoid sending empty form, and then
    // validating fields in validate/submit handler.
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    $values = $form_state->getValues();

    $check = [
      'ug_url_path' => 'live catalog',
      'ug_archive_path' => 'archives',
      'grd_url_path' => 'live catalog',
      'grd_archive_path' => 'archives',
    ];

    foreach ($check as $field => $message) {
      $path = $values[$field];
      if (trim($path) !== $path) {
        $form_state->setErrorByName($field, $this->t('The URL for @field cannot contain spaces.', ['@field' => $message]));
      }

      if (trim($path, '/\\') !== $path) {
        $form_state->setErrorByName($field, $this->t('The URL for @field cannot contain leading and/or trailing slash.', ['@field' => $message]));
      }

      if (!preg_match(self::PATH_REGEX_FORMAT, $path)) {
        $form_state->setErrorByName($field, $this->t('The URL must be a valid format. Allowed subsets are small letter, numbers, dash (-), and underscore(_).'));
      }

      $pattern = '/^(?:' . implode('|', self::RESERVED_PATHS) . ')(?:$|)/';

      if (preg_match($pattern, $path)) {
        $form_state->setErrorByName($field, $this->t('The URL must be changed - "@url" starts with /admin or /node.', ['@url' => $path]));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    // Overwrite enabled checkbox only if user has permission to enable kuali.
    $update = $this->config(static::SETTINGS);
    if ($this->currentUser->hasPermission('enable uw_kuali')) {
      $update->set('kuali_ug_enabled', $form_state->getValue('kuali_ug_enabled'));
      $update->set('kuali_grd_enabled', $form_state->getValue('kuali_grd_enabled'));
    }

    $update->set('ug_page_title', $form_state->getValue('ug_page_title'));
    $update->set('grd_page_title', $form_state->getValue('grd_page_title'));
    $update->set('ug_url_path', $form_state->getValue('ug_url_path'));
    $update->set('ug_archive_path', $form_state->getValue('ug_archive_path'));
    $update->set('grd_url_path', $form_state->getValue('grd_url_path'));
    $update->set('grd_archive_path', $form_state->getValue('grd_archive_path'));

    // Save kuali configuration object.
    $update->save();

    // Force route rebuild, to rebuild routes that depend on config settings.
    $this->routeBuilder->rebuild();

    // Create/delete menu items.
    $this->kualiService->modifyKualiMenuItem($form_state->getValues());

    // Clear kuali views cache.
    if ($view = Views::getView('kuali')) {
      $view->storage->invalidateCaches();
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('router.builder'),
      $container->get('current_user'),
      $container->get('router.route_provider'),
      $container->get('uw_kuali.catalogs'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      static::SETTINGS,
    ];
  }

}
