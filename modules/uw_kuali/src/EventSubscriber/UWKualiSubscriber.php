<?php

namespace Drupal\uw_kuali\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Drupal\core_event_dispatcher\EntityHookEvents;
use Drupal\core_event_dispatcher\Event\Entity\EntityDeleteEvent;
use Drupal\core_event_dispatcher\Event\Entity\EntityInsertEvent;
use Drupal\core_event_dispatcher\Event\Entity\EntityUpdateEvent;
use Drupal\uw_kuali\Service\UWKualiServicesInterface;
use Drupal\views_event_dispatcher\Event\Views\ViewsPreRenderEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * UW Kuali event subscriber.
 */
class UWKualiSubscriber implements EventSubscriberInterface {

  /**
   * Constructs an UwKualiSubscriber object.
   *
   * @param \Drupal\Core\Routing\RouteBuilderInterface $route_builder
   *   The router builder service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\uw_kuali\Service\UWKualiServicesInterface $kualiService
   *   Kuali service.
   */
  public function __construct(
    protected RouteBuilderInterface $route_builder,
    protected EntityTypeManagerInterface $entity_type_manager,
    protected UWKualiServicesInterface $kualiService,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // Views Hook module is not enabled, and that causes an issue,
    // update hook can enable the module, but code will error out before,
    // saying class is not found. There is a temporary fix for this
    // in the .module file, using hook_views_pre_render.
    return [
      EntityHookEvents::ENTITY_INSERT => ['onEntityInsert'],
      EntityHookEvents::ENTITY_UPDATE => ['onEntityUpdate'],
      EntityHookEvents::ENTITY_DELETE => ['onEntityDelete'],
      // ViewsHookEvents::VIEWS_PRE_RENDER => ['onViewsPreRender'],.
    ];
  }

  /**
   * Entity insert/create event.
   *
   * @param \Drupal\core_event_dispatcher\Event\Entity\EntityInsertEvent $event
   *   Event.
   */
  public function onEntityInsert(EntityInsertEvent $event) {
    if ($event->getEntity()->getEntityType()->id() === 'uw_kuali') {
      $this->rebuildRoutes();
    }
  }

  /**
   * Rebuilds routes.
   */
  private function rebuildRoutes(): void {
    $this->route_builder->rebuild();
  }

  /**
   * Entity update event.
   *
   * @param \Drupal\core_event_dispatcher\Event\Entity\EntityUpdateEvent $event
   *   Event.
   */
  public function onEntityUpdate(EntityUpdateEvent $event) {
    if ($event->getEntity()->getEntityType()->id() === 'uw_kuali') {
      $this->rebuildRoutes();
    }
  }

  /**
   * Entity delete event.
   *
   * @param \Drupal\core_event_dispatcher\Event\Entity\EntityDeleteEvent $event
   *   Event.
   */
  public function onEntityDelete(EntityDeleteEvent $event) {
    if ($event->getEntity()->getEntityType()->id() === 'uw_kuali') {
      $this->rebuildRoutes();
    }
  }

  /**
   * Rewrite of Kuali entity label field.
   *
   * This could not have been done on view level, since there is service
   * that will generate URL.
   *
   * This depends on a module views_event_dispatcher.
   *
   * @param \Drupal\views_event_dispatcher\Event\Views\ViewsPreRenderEvent $event
   *   Event that holds view.
   */
  public function onViewsPreRender(ViewsPreRenderEvent $event) {
    $view = $event->getView();

    if ($view->id() === 'kuali') {
      foreach ($view->result as $row) {
        $row->_entity->set('label', $this->kualiService->generateKualiLink($row->_entity));
      }
    }
  }

}
