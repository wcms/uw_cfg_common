<?php

namespace Drupal\uw_kuali\Controller;

/**
 * UW Kuali controller interface.
 *
 * Listing allowed actions on kuali integration.
 */
interface UWKualiControllerInterface {

  /**
   * Kuali page integration.
   *
   * @param int $kuali_id
   *   Kuali entity id.
   *
   * @return array
   *   Render array for kuali integration.
   */
  public function content(int $kuali_id): array;

}
