<?php

namespace Drupal\uw_kuali\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityFormBuilderInterface;
use Drupal\uw_kuali\Form\UWKualiSettingsForm;
use Drupal\uw_kuali\Service\UWKualiServicesInterface;
use Drupal\uw_kuali\UWKualiInterface;

/**
 * Kuali archive controller.
 */
class UWKualiArchiveController extends ControllerBase implements UWKualiArchiveControllerInterface {

  /**
   * Entity form builder service from the core.
   *
   * @var \Drupal\Core\Entity\EntityFormBuilderInterface
   */
  protected $entityFormBuilder;

  /**
   * Kuali service.
   *
   * @var \Drupal\uw_kuali\Service\UWKualiServicesInterface
   */
  protected UWKualiServicesInterface $kualiService;

  /**
   * Constructs a UWKualiController object.
   */
  public function __construct(EntityFormBuilderInterface $entityFormBuilder, UWKualiServicesInterface $kualiService) {
    $this->entityFormBuilder = $entityFormBuilder;
    $this->kualiService = $kualiService;
  }

  /**
   * {@inheritDoc}
   */
  public function archiveLanding(string $career = 'UG'): array {
    $rows = [];
    $archives = $this->kualiService->getArchives($career);

    /** @var \Drupal\uw_kuali\UWKualiInterface $archive */
    foreach ($archives as $archive) {
      $rows[] = [
        'name' => $this->kualiService->generateKualiLink($archive),
      ];
    }

    return [
      '#type' => 'table',
      '#header' => [
        'name' => $this->t('Name'),
      ],
      '#rows' => $rows,
      '#empty' => $this->t('There are no archives available.'),
      '#title' => $this->t('@prefix Studies archives', ['@prefix' => $career === 'UG' ? 'Undergraduate' : 'Graduate']),

    ];
  }

  /**
   * {@inheritDoc}
   */
  public function archiveForm(UWKualiInterface $kuali): array {
    return ['form' => $this->buildArchiveForm($kuali)];
  }

  /**
   * Builds entity form with some fields hidden.
   *
   * @param \Drupal\uw_kuali\UWKualiInterface $kuali
   *   Kuali instance.
   *
   * @return array
   *   Render array form.
   */
  private function buildArchiveForm(UWKualiInterface $kuali): array {
    // When Kuali is not enabled, display a message.
    if (!$this->kualiEnabled($kuali->getAcademicCareer())) {
      return [
        '#markup' => $this->t('Kuali integration is not enabled.'),
      ];
    }

    $form = $this->entityFormBuilder()->getForm($kuali, 'add');

    // Hiding fields.
    $form['status']['#access'] = FALSE;
    $form['career']['#access'] = FALSE;

    // Remove buttons.
    unset($form['actions']['cancel'], $form['actions']['delete']);

    return $form;
  }

  /**
   * Check if Kuali is enabled.
   *
   * @param string $career
   *   Academic career.
   *
   * @return bool
   *   True when enabled, false otherwise.
   */
  private function kualiEnabled(string $career): bool {
    // Check if global settings have enabled Kuali for the career.
    $kuali_config = $this->config(UWKualiSettingsForm::SETTINGS);

    if (!in_array($career, ['UG', 'GRD'])) {
      return FALSE;
    }

    return (bool) $kuali_config->get('kuali_' . strtolower($career) . '_enabled');
  }

  /**
   * {@inheritDoc}
   */
  public function archiveAdd(string $career = 'UG'): array {
    // When Kuali is not enabled, display a message.
    if (!$this->kualiEnabled($career)) {
      return [
        '#markup' => $this->t('Kuali integration is not enabled.'),
      ];
    }

    $kuali = $this->entityTypeManager()->getStorage('uw_kuali')->create([
      'type' => 'uw_kuali',
      'career' => $career,
      'status' => 'archive',
      'environment' => 'uwaterloocm',
    ]);

    return ['form' => $this->buildArchiveForm($kuali)];
  }

  /**
   * {@inheritdoc}
   */
  public static function create($container) {
    return new self(
      $container->get('entity.form_builder'),
      $container->get('uw_kuali.catalogs'),
    );
  }

}
