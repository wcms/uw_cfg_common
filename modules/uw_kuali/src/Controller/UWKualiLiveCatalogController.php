<?php

namespace Drupal\uw_kuali\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\uw_kuali\Form\UWKualiSettingsForm;
use Drupal\uw_kuali\Service\UWKualiServicesInterface;
use Drupal\uw_kuali\UWKualiInterface;

/**
 * UW Kuali live catalog controller.
 */
class UWKualiLiveCatalogController extends ControllerBase implements UWKualiLiveCatalogControllerInterface {

  /**
   * Constructs a UWKualiController object.
   */
  public function __construct(
    protected UWKualiServicesInterface $kualiService,
  ) {}

  /**
   * {@inheritDoc}
   */
  public function getLiveCatalog(string $career = 'UG'): array {
    // When Kuali is not enabled, display a message.
    if (!$this->kualiEnabled($career)) {
      return [
        '#markup' => $this->t('Kuali integration is not enabled.'),
      ];
    }

    // Based on career, find active kuali, and load edit form.
    $kuali = $this->kualiService->getActiveCatalog($career);

    if (!$kuali) {
      /** @var \Drupal\uw_kuali\UWKualiInterface $kuali */
      $kuali = $this->entityTypeManager()->getStorage('uw_kuali')->create([
        'type' => 'uw_kuali',
        'career' => $career,
        'status' => 'live',
        'environment' => 'uwaterloocm',
      ]);
    }

    return ['form' => $this->buildLiveCatalogForm($kuali)];
  }

  /**
   * Check if Kuali is enabled.
   *
   * @param string $career
   *   Academic career.
   *
   * @return bool
   *   True when enabled, false otherwise.
   */
  private function kualiEnabled(string $career): bool {
    // Check if global settings have enabled Kuali for the career.
    $kuali_config = $this->config(UWKualiSettingsForm::SETTINGS);

    if (!in_array($career, ['UG', 'GRD'])) {
      return FALSE;
    }

    return (bool) $kuali_config->get('kuali_' . strtolower($career) . '_enabled');
  }

  /**
   * {@inheritdoc}
   */
  public static function create($container) {
    return new self(
      $container->get('uw_kuali.catalogs'),
    );
  }

  /**
   * Builds entity form with some fields hidden.
   *
   * @param \Drupal\uw_kuali\UWKualiInterface $kuali
   *   Kuali instance.
   *
   * @return array
   *   Render array with form.
   */
  private function buildLiveCatalogForm(UWKualiInterface $kuali): array {
    $form = $this->entityFormBuilder()->getForm($kuali, 'add');

    // Hiding fields.
    $form['status']['#access'] = FALSE;
    $form['career']['#access'] = FALSE;

    // Remove buttons.
    unset($form['actions']['cancel'], $form['actions']['delete']);

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function createNewLiveCatalog(string $career = 'UG'): array {
    // When Kuali is not enabled, display a message.
    if (!$this->kualiEnabled($career)) {
      return [
        '#markup' => $this->t('Kuali integration is not enabled.'),
      ];
    }

    $kuali = $this->entityTypeManager()->getStorage('uw_kuali')->create([
      'type' => 'uw_kuali',
      'career' => $career,
      'status' => 'live',
      'environment' => 'uwaterloocm',
    ]);

    return $this->buildLiveCatalogForm($kuali);
  }

}
