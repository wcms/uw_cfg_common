<?php

namespace Drupal\uw_kuali\Controller;

/**
 * Kuali live catalog controller interface.
 */
interface UWKualiLiveCatalogControllerInterface {

  /**
   * Prepare active catalog (if found) form.
   *
   * @param string $career
   *   Academic career UG or GRD. Defaults to UG.
   *
   * @return array
   *   Render array.
   */
  public function getLiveCatalog(string $career = 'UG'): array;

  /**
   * Creates new live catalog.
   *
   * @param string $career
   *   Academic career.
   *
   * @return array
   *   Entity form as render array.
   */
  public function createNewLiveCatalog(string $career = 'UG'): array;

}
