<?php

namespace Drupal\uw_kuali\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityFormBuilderInterface;
use Drupal\uw_kuali\Form\UWKualiSettingsForm;
use Drupal\uw_kuali\Service\UWKualiServicesInterface;

/**
 * UW Kuali controller.
 */
class UWKualiController extends ControllerBase implements UWKualiControllerInterface {

  /**
   * Entity form builder service from the core.
   *
   * @var \Drupal\Core\Entity\EntityFormBuilderInterface
   */
  protected $entityFormBuilder;

  /**
   * Kuali service.
   *
   * @var \Drupal\uw_kuali\Service\UWKualiServicesInterface
   */
  protected UWKualiServicesInterface $kualiService;

  /**
   * Constructs a UWKualiController object.
   */
  public function __construct(EntityFormBuilderInterface $entityFormBuilder, UWKualiServicesInterface $kualiService) {
    $this->entityFormBuilder = $entityFormBuilder;
    $this->kualiService = $kualiService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create($container) {
    return new self(
      $container->get('entity.form_builder'),
      $container->get('uw_kuali.catalogs'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function content(int $kuali_id = NULL): array {
    $kuali_module = $this->config(UWKualiSettingsForm::SETTINGS);
    $kuali = $this->entityTypeManager()
      ->getStorage('uw_kuali')
      ->load($kuali_id);

    $output = [];
    $script_tag_content = '';

    $kuali_suffix = '';

    if ($domain = $kuali->get('environment')->value) {
      $script_tag_content = "window.subdomain = 'https://{$domain}.kuali.co';";

      if ($catalog = $kuali->get('catalog_id')->value) {
        $script_tag_content .= "window.catalogId = '{$catalog}'";
      }
    }

    if ($script_tag_content) {
      $output['label'] = [
        '#markup' => $this->t('Loading @catalog. JavaScript must be enabled for this to work.', ['@catalog' => $kuali_module->get('kuali_page_title' . $kuali_suffix)]),
        '#attached' => [
          'library' => ['uw_kuali/uw-kuali-' . $domain],
        ],
        '#prefix' => '<div id="kuali-catalog">',
        '#suffix' => '</div>',
      ];

      $output['script'] = [
        '#markup' => '<script>' . $script_tag_content . '</script>',
        '#allowed_tags' => ['script'],
      ];
    }
    else {
      $output['message'] = ['#markup' => $this->t('No Kuali integration configured.')];
    }

    return $output;
  }

}
