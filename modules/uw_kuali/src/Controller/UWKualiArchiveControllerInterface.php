<?php

namespace Drupal\uw_kuali\Controller;

use Drupal\uw_kuali\UWKualiInterface;

/**
 * Kuali archive controller interface.
 */
interface UWKualiArchiveControllerInterface {

  /**
   * Prepare the landing page for the archived catalog.
   *
   * @param string $career
   *   Academic career UG or GRD. Defaults to UG.
   *
   * @return array
   *   The rendered landing page for the archived catalog.
   */
  public function archiveLanding(string $career = 'UG'): array;

  /**
   * Modified an entity form.
   */
  public function archiveForm(UWKualiInterface $kuali): array;

  /**
   * Controller based form for UG archives.
   *
   * @param string $career
   *   Academic career.
   *
   * @return array
   *   Modified entity form.
   */
  public function archiveAdd(string $career = 'UG'): array;

}
