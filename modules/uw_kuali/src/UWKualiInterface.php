<?php

namespace Drupal\uw_kuali;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a kuali entity type.
 */
interface UWKualiInterface extends ContentEntityInterface, EntityChangedInterface {

  /**
   * Get status field value.
   *
   * @return string|null
   *   Status label.
   */
  public function getStatus(): ?string;

  /**
   * Get status field key value.
   *
   * @return string|null
   *   Status key.
   */
  public function getStatusKey(): ?string;

  /**
   * Get UW term code.
   *
   * @return string
   *   Term code.
   */
  public function getTermCode(): string;

  /**
   * Get academic career level.
   *
   * @return string
   *   UG or GRD.
   */
  public function getAcademicCareer(): string;

  /**
   * Update updated by field.
   *
   * @param int|null $uid
   *   User id that is making a change.
   *
   * @return \Drupal\uw_kuali\UWKualiInterface
   *   Kuali object.
   */
  public function setUpdatedById(?int $uid): UWKualiInterface;

  /**
   * Get the ID of the user who last updated the record.
   *
   * @return int|null
   *   The ID of the user who last updated the record,
   *   or null if it has not been updated.
   */
  public function getUpdatedById(): ?int;

  /**
   * Get the ID of the user who created this entity.
   *
   * @return int|null
   *   The ID of the user who created this entity, or null if not set.
   */
  public function getCreatedById(): ?int;

  /**
   * Set the user ID who created the entity.
   *
   * @param int|null $uid
   *   The user ID.
   *
   * @return UWKualiInterface
   *   The updated KualiInterface object.
   */
  public function setCreatedById(?int $uid): UWKualiInterface;

  /**
   * Returns the label for a given term code.
   *
   * @param string $term
   *   The term code that passed a validation.
   *
   * @return string
   *   The term label.
   */
  public function termCodeLabel(string $term): string;

}
