<?php

namespace Drupal\uw_kuali\Service;

use Drupal\Component\Utility\Html;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\menu_link_content\MenuLinkContentInterface;
use Drupal\uw_kuali\Form\UWKualiSettingsForm;
use Drupal\uw_kuali\UWKualiInterface;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

/**
 * UW Kuali services.
 */
class UWKualiServices implements UWKualiServicesInterface {

  /**
   * Default constructor using property promotion.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service from core.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   Config factory service.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   Current user.
   * @param \Drupal\Core\Routing\RouteProviderInterface $route_provider
   *   Route provider.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entity_type_manager,
    protected ConfigFactoryInterface $config,
    protected AccountProxyInterface $current_user,
    protected RouteProviderInterface $route_provider,
  ) {}

  /**
   * {@inheritDoc}
   */
  public function getActiveCatalog(string $career = 'UG'): ?UWKualiInterface {
    $query = $this->entity_type_manager->getStorage('uw_kuali')->getQuery();

    $result = $query->accessCheck(FALSE)
      ->condition('career', $career)
      ->condition('status', 'live')
      ->execute();

    if (!empty($result)) {
      $kuali_multiple = $this->entity_type_manager->getStorage('uw_kuali')
        ->loadMultiple($result);
      return current($kuali_multiple);
    }

    return NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function getArchives(string $career = 'UG'): array {
    $query = $this->entity_type_manager->getStorage('uw_kuali')->getQuery();

    $result = $query->accessCheck(FALSE)
      ->condition('career', $career)
      ->condition('status', 'archive')
      ->execute();

    if (!empty($result)) {
      return $this->entity_type_manager->getStorage('uw_kuali')
        ->loadMultiple($result);
    }

    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function generateKualiUrl(UWKualiInterface $kuali): string {
    $url = '';

    // Get config paths form settings form.
    [$liveUrlPath, $archivePath] = $this->getKualiPaths($kuali->getAcademicCareer());

    if (!$liveUrlPath && !$archivePath) {
      return $url;
    }

    // URL will be generated only for 'live' & 'archived' kualis. There are
    // 'unpublished' and 'future'.
    if ($kuali->getStatusKey() === 'live') {
      $url = $liveUrlPath;
    }
    elseif ($kuali->getStatusKey() === 'archive') {
      $url = $liveUrlPath . '/' . $archivePath . '/' . $kuali->get('url')->value;
    }

    return $url;
  }

  /**
   * {@inheritDoc}
   */
  public function generateKualiLink(UWKualiInterface $kuali): Link|string {
    // If global Kuali is enabled, generate Link to it.
    if ($uri = $this->generateKualiUrl($kuali)) {
      return Link::fromTextAndUrl($kuali->label(), Url::fromUri('internal:/' . $uri));
    }

    // In case global Kuali is not enabled, allow Kuali entities to be created.
    // Just instead of link, use label/string.
    return $kuali->label();
  }

  /**
   * Get Kuali paths based on academic career.
   *
   * @param string $career
   *   Get live and archive configured path.
   *
   * @return string[]|null
   *   Array of both values, or NULL.
   */
  private function getKualiPaths(string $career): ?array {
    $config = $this->config->get(UWKualiSettingsForm::SETTINGS);
    $career = strtolower($career);

    if (
      !$config->get('kuali_' . $career . '_enabled') ||
      empty($config) ||
      !in_array($career, ['ug', 'grd'])
    ) {
      return NULL;
    }

    $url_path = $career . '_url_path';
    $archive_path = $career . '_archive_path';

    return [$config->get($url_path), $config->get($archive_path)];
  }

  /**
   * {@inheritDoc}
   */
  public function modifyKualiMenuItem(array $config = NULL): void {
    if (!$config) {
      $config = $this->config->get(UWKualiSettingsForm::SETTINGS)->get();
    }

    $this->handleMenuItems($config);
  }

  /**
   * Create or delete menu items based on settings.
   *
   * @param array $config
   *   Existing configuration.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function handleMenuItems(array $config): void {
    if ($config['kuali_ug_enabled']) {
      // Check if route exits first, and then create main menu item.
      if ($this->checkKualiRoute('uw_kuali.ug_live_catalog')) {
        $kuali_menu = $this->getKualiMainMenuItem($config['ug_page_title'], 'route:uw_kuali.ug_live_catalog');

        // Rename here if needed.
        if ($kuali_menu) {
          $kuali_menu->set('title', $config['ug_page_title']);
          $kuali_menu->save();
        }
      }
    }
    elseif ($this->current_user->hasPermission('enable uw_kuali')) {
      if ($kuali_menu = $this->getKualiMainMenuItem($config['ug_page_title'], 'route:uw_kuali.ug_live_catalog', FALSE)) {
        $kuali_menu->delete();
      }
    }

    if ($config['kuali_grd_enabled']) {
      if ($this->checkKualiRoute('uw_kuali.grd_live_catalog')) {
        $kuali_menu = $this->getKualiMainMenuItem($config['grd_page_title'], 'route:uw_kuali.grd_live_catalog');

        // Rename here if needed.
        if ($kuali_menu) {
          $kuali_menu->set('title', $config['grd_page_title']);
          $kuali_menu->save();
        }
      }
    }
    elseif ($this->current_user->hasPermission('enable uw_kuali')) {
      if ($kuali_menu = $this->getKualiMainMenuItem($config['grd_page_title'], 'route:uw_kuali.grd_live_catalog', FALSE)) {
        $kuali_menu->delete();
      }
    }
  }

  /**
   * Check if route exists, this throws an exception.
   *
   * @param string $route
   *   Route to check.
   *
   * @return bool
   *   True if route is registered, false oterwise.
   */
  private function checkKualiRoute(string $route): bool {
    try {
      $exists = (bool) $this->route_provider->getRouteByName($route);
    }
    catch (RouteNotFoundException $e) {
      $exists = FALSE;
    }

    return $exists;
  }

  /**
   * Load kuali menu item. If not found, create one.
   *
   * Logic expects that there will be the only one kuali menu item.
   *
   * @param string $title
   *   Meni item title.
   * @param string $route
   *   Register route, example format: route:uw_kuali.url.
   * @param bool $create_if_not_found
   *   Boolean, this will create menu item if not found.
   *
   * @return \Drupal\menu_link_content\MenuLinkContentInterface|null
   *   Meni item, loaded or created.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function getKualiMainMenuItem(string $title, string $route, bool $create_if_not_found = TRUE): ?MenuLinkContentInterface {
    $kuali_menu_item = NULL;

    $menus = $this->entity_type_manager->getStorage('menu_link_content')
      ->loadByProperties([
        'link' => $route,
      ]);

    if ($menus) {
      /** @var \Drupal\menu_link_content\MenuLinkContentInterface $kuali_menu_item */
      $kuali_menu_item = current($menus);
    }
    elseif ($create_if_not_found) {
      $kuali_menu_item = MenuLinkContent::create([
        'title' => Html::escape($title),
        'link' => ['uri' => $route],
        'menu_name' => 'main',
        'weight' => 100,
        'enabled' => TRUE,
      ]);

      $kuali_menu_item->save();
    }

    return $kuali_menu_item;
  }

}
