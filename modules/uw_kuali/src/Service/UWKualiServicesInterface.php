<?php

namespace Drupal\uw_kuali\Service;

use Drupal\Core\Link;
use Drupal\uw_kuali\UWKualiInterface;

/**
 * Kuali services.
 */
interface UWKualiServicesInterface {

  /**
   * Loads Kuali from database.
   *
   * @param string $career
   *   Academic career.
   *
   * @return \Drupal\uw_kuali\UWKualiInterface|null
   *   If found Kuali entity, otherwise NULL.
   */
  public function getActiveCatalog(string $career = 'UG'): ?UWKualiInterface;

  /**
   * Get archives for a specific career.
   *
   * @param string $career
   *   The career name (default: 'UG')
   *
   * @return array
   *   An array of archives
   */
  public function getArchives(string $career = 'UG'): array;

  /**
   * Generate URL from Kuali instance.
   *
   * @param \Drupal\uw_kuali\UWKualiInterface $kuali
   *   Kuali to generate URL to.
   *
   * @return string
   *   generated URL.
   */
  public function generateKualiUrl(UWKualiInterface $kuali): string;

  /**
   * Generate Kuali link as render array.
   *
   * @param \Drupal\uw_kuali\UWKualiInterface $kuali
   *   Kuali to generate link to.
   *
   * @return \Drupal\Core\Link|string
   *   Link with title and url.
   */
  public function generateKualiLink(UWKualiInterface $kuali): Link|string;

  /**
   * Create or delete menu item.
   *
   * @param array|null $config
   *   Kuali Settings form.
   */
  public function modifyKualiMenuItem(array $config = NULL): void;

}
