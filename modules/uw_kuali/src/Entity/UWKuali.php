<?php

namespace Drupal\uw_kuali\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\Entity\User;
use Drupal\uw_kuali\UWKualiInterface;

/**
 * Defines the Kuali entity class.
 *
 * @ContentEntityType(
 *   id = "uw_kuali",
 *   label = @Translation("UW Kuali"),
 *   label_collection = @Translation("UW Kuali archives"),
 *   label_singular = @Translation("UW Kuali"),
 *   label_plural = @Translation("UW Kualis"),
 *   label_count = @PluralTranslation(
 *     singular = "@count UW Kualis",
 *     plural = "@count UW Kualis",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\uw_kuali\UWKualiListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\uw_kuali\Form\UWKualiForm",
 *       "edit" = "Drupal\uw_kuali\Form\UWKualiForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "uw_kuali",
 *   admin_permission = "configure uw_kuali",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "career" = "career",
 *     "term" = "term",
 *     "environment" = "environment",
 *     "published" = "status",
 *     "created_by" = "created_by",
 *     "updated_by" = "updated_by",
 *   },
 *   links = {
 *     "collection" = "/admin/kualis",
 *     "add-form" = "/admin/kuali/add",
 *     "canonical" = "/admin/kuali/{uw_kuali}",
 *     "edit-form" = "/admin/kuali/{uw_kuali}/edit",
 *     "delete-form" = "/admin/kuali/{uw_kuali}/delete",
 *   },
 *   constraints = {
 *     "UWKualiUniqueness" = {}
 *   }
 * )
 *
 * @see https://www.drupal.org/node/2770845
 * @see https://www.drupal.org/docs/drupal-apis/update-api/updating-entities-and-fields-in-drupal-8
 * @see https://www.drupal.org/docs/drupal-apis/entity-api/entity-validation-api/defining-constraints-validations-on-entities-andor-fields
 */
class UWKuali extends ContentEntityBase implements UWKualiInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Label / page title when archived'))
      ->setSetting('max_length', 255)
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -50,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -50,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['term'] = BaseFieldDefinition::create('string')
      ->setLabel(t('UW term code'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->addConstraint('UWKualiTermCode')
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'settings' => [
          'size' => 10,
        ],
        'weight' => -40,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -40,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['catalog_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Catalog ID'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -30,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -30,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['url'] = BaseFieldDefinition::create('string')
      ->setLabel(t('URL when archived'))
      ->setSetting('max_length', 255)
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['career'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Academic career'))
      ->setRequired(TRUE)
      ->setSetting('allowed_values', [
        'UG' => t('Undergraduate'),
        'GRD' => t('Graduate'),
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'list_default',
        'weight' => -10,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['environment'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Environment'))
      ->setRequired(TRUE)
      ->setSetting('allowed_values', [
        'uwaterloocm-sbx' => t('Sandbox'),
        'uwaterloocm-stg' => t('Staging'),
        'uwaterloocm' => t('Production'),
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'list_default',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Status'))
      ->setRequired(TRUE)
      ->setSetting('allowed_values', [
        'live' => t('Live'),
        'archive' => t('Archive'),
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'list_default',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created_by'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Created by'))
      ->setSetting('target_type', 'user');

    $fields['updated_by'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Updated by'))
      ->setSetting('target_type', 'user');

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the kuali was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Updated'))
      ->setDescription(t('The time that the kuali was last edited.'));

    return $fields;
  }

  /**
   * {@inheritDoc}
   */
  public function termCodeLabel(string $term): string {
    $month = match ($term[-1]) {
      '1' => 'Winter',
      '5' => 'Summer',
      '9' => 'Fall',
      default => '',
    };

    // If for any reason term code was not valid return empty string.
    if (empty($month)) {
      return $month;
    }

    $year = '20' . substr($term, 1, -1);

    return $month . ' ' . $year;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    $uid = \Drupal::currentUser()->id();

    // Initially set created by for new kuali.
    if (empty($this->getEntityKey('created_by')) && $this->isNew()) {
      $this->setCreatedById($uid);
    }

    // For new entities create changed by.
    if (empty($this->getEntityKey('updated_by'))) {
      $this->setUpdatedById($uid);
    }

    // If label is empty, create it from term code.
    if (!$this->getEntityKey('label')) {
      $this->set('label', $this->termCodeLabel($this->getEntityKey('term')));
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getStatus(): ?string {
    if ($this->hasField('status')) {
      $options = $this->getFieldDefinition('status')->getSetting('allowed_values');
      $field_value = $this->getStatusKey();

      return $options[$field_value]?->render() ?? $field_value;
    }

    return NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function getStatusKey(): ?string {
    if ($this->hasField('status')) {
      return $this->get('status')->value;
    }

    return NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function getTermCode(): string {
    if ($this->hasField('term')) {
      return $this->get('term')->value;
    }

    throw new \RuntimeException("No term code found");
  }

  /**
   * {@inheritDoc}
   */
  public function getAcademicCareer(): string {
    if ($this->hasField('career')) {
      return $this->get('career')->value;
    }

    throw new \RuntimeException("No academic career found");
  }

  /**
   * {@inheritDoc}
   */
  public function setUpdatedById(?int $uid): UWKualiInterface {
    if ($this->hasField('updated_by')) {
      $valueToStore = $uid && $this->validateUid($uid) ? $uid : \Drupal::currentUser()->id();
      $this->set('updated_by', $valueToStore);
    }
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function getUpdatedById(): ?int {
    if ($this->hasField('updated_by')) {
      return $this->get('updated_by')?->entity?->id();
    }

    throw new \RuntimeException("No updated by field found");
  }

  /**
   * {@inheritDoc}
   */
  public function getCreatedById(): ?int {
    if ($this->hasField('created_by')) {
      return $this->get('created_by')?->entity?->id();
    }

    throw new \RuntimeException("No updated by field found");
  }

  /**
   * {@inheritDoc}
   */
  public function setCreatedById(?int $uid): UWKualiInterface {
    if ($this->hasField('created_by')) {
      $valueToStore = $uid && $this->validateUid($uid) ? $uid : \Drupal::currentUser()->id();
      $this->set('created_by', $valueToStore);
    }
    return $this;
  }

  /**
   * Validates the user ID.
   *
   * @param int $uid
   *   The user ID to validate.
   *
   * @return bool
   *   Returns true if the user ID is valid and the user is active,
   *   false otherwise.
   */
  protected function validateUid(int $uid): bool {
    $user = User::load($uid);

    return ($user && $user->isActive());
  }

}
