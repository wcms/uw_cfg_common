<?php

namespace Drupal\uw_kuali\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Provides an Example constraint.
 *
 * @Constraint(
 *   id = "UWKualiTermCode",
 *   label = @Translation("UW Term code", context = "Validation"),
 *   type = { "string" }
 * )
 */
class UWKualiTermCodeConstraint extends Constraint {

  /**
   * Incorrect format for term code.
   *
   * @var string
   */
  public string $incorrectFormatMessage = "Term code '%value' format is incorrect. Please use <a href='@link'>correct format</a>.";

}
