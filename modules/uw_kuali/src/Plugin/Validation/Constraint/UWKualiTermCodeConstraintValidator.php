<?php

namespace Drupal\uw_kuali\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the Example constraint.
 */
class UWKualiTermCodeConstraintValidator extends ConstraintValidator {

  /**
   * Regular expression for uw term code.
   *
   * @var string
   *
   * @see https://uwaterloo.ca/quest/graduate-students/glossary-of-terms#t
   */
  public static string $format = '/^1\d{2}[159]$/';

  /**
   * Url explaining term code.
   */
  public const URL = 'https://uwaterloo.ca/quest/graduate-students/glossary-of-terms#t';

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint): void {

    // This will validate format for term code using regex.
    $item = trim($value->value);
    $matches = [];

    if (!preg_match(self::$format, $item, $matches)) {

      $this->context->addViolation(
        $constraint->incorrectFormatMessage,
        ['%value' => $item, '@link' => self::URL]
      );
    }

  }

}
