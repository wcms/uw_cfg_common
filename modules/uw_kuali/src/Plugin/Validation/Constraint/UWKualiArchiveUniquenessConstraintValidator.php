<?php

namespace Drupal\uw_kuali\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the Example constraint.
 */
class UWKualiArchiveUniquenessConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint): void {

    $query = $this->entity_type_manager->getStorage('uw_kuali')->getQuery();

    $query->condition('term', $value->get('term')->value);
    $query->condition('career', $value->get('career')->value);
    $result = $query->execute();

    if (!empty($result) && is_array($result)) {

      if (count($result) === 1) {
        $currently_active = current($result);

        if ($value->id() === $currently_active) {
          return;
        }
      }
      else {
        $this->context->addViolation($constraint->multipleActive);
      }

      $this->context->addViolation(
        $constraint->notUniqueMessage,
        [
          '%term' => $value->get('term')->value,
          '%career' => $value->get('career')->value,
        ]
      );
    }

  }

  /**
   * Default constructor using property promotion.
   */
  public function __construct(protected readonly EntityTypeManagerInterface $entity_type_manager) {}

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {

    return new static(
      $container->get('entity_type.manager')
    );
  }

}
