<?php

namespace Drupal\uw_kuali\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Provides an UW Kuali uniqueness constraint.
 *
 * @Constraint(
 *   id = "UWKualiUniqueness",
 *   label = @Translation("Kuali uniqueness", context = "Validation"),
 *   type = { "entity" }
 * )
 *
 * To apply this constraint, see https://www.drupal.org/docs/drupal-apis/entity-api/entity-validation-api/providing-a-custom-validation-constraint.
 */
class UWKualiArchiveUniquenessConstraint extends Constraint {

  /**
   * Error message for a non-unique Kuali archive.
   *
   * @var string
   */
  public string $notUniqueMessage = 'Kuali archive error: Archive already exists for term: %term and %career.';

  /**
   * Error message when there are multiple active kualis.
   *
   * @var string
   */
  public string $multipleActive = 'It appears there are multiple Kuali entries with "active" status. Which is not good.';

}
