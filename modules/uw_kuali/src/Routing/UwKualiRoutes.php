<?php

namespace Drupal\uw_kuali\Routing;

use Drupal\Component\Utility\Html;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\uw_kuali\Form\UWKualiSettingsForm;
use Drupal\uw_kuali\Service\UWKualiServicesInterface;
use Drupal\uw_kuali\UWKualiInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;

/**
 * Dynamic routes for UW Kuali module.
 */
class UwKualiRoutes implements ContainerInjectionInterface {

  /**
   * Config for Kuali.
   *
   * @var array
   */
  protected array $config;

  /**
   * Kuali services.
   *
   * @var \Drupal\uw_kuali\Service\UWKualiServicesInterface
   */
  protected UWKualiServicesInterface $kualiService;

  /**
   * Default constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory service.
   * @param \Drupal\uw_kuali\Service\UWKualiServicesInterface $kuali_service
   *   Kuali service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, UWKualiServicesInterface $kuali_service) {
    if ($kuali_config = $config_factory->get(UWKualiSettingsForm::SETTINGS)) {
      $this->config = $kuali_config->get();
    }

    $this->kualiService = $kuali_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('uw_kuali.catalogs'),
    );
  }

  /**
   * Returns dynamic routes.
   *
   * Registers routes for Kuali, using settings.
   *
   * @return array
   *   Routes as array.
   */
  public function routes(): array {
    $routes = [];

    if (!empty($this->config)) {
      if ($this->config['kuali_ug_enabled'] ?? NULL) {
        $ug_live = $this->kualiService->getActiveCatalog('UG');
        if ($ug_live) {
          $routes = array_merge($routes, $this->createLiveRoutes($ug_live, $this->config['ug_url_path']));
        }

        $routes = array_merge($routes, $this->createArchiveRoutes('UG', $this->config['ug_url_path'], $this->config['ug_archive_path']));
      }
      if ($this->config['kuali_grd_enabled'] ?? NULL) {
        $grd_live = $this->kualiService->getActiveCatalog('GRD');
        if ($grd_live) {
          $routes = array_merge($routes, $this->createLiveRoutes($grd_live, $this->config['grd_url_path']));
        }

        $routes = array_merge($routes, $this->createArchiveRoutes('GRD', $this->config['grd_url_path'], $this->config['grd_archive_path']));
      }
    }

    return $routes;
  }

  /**
   * Create routes for the specified Kuali catalog type.
   *
   * @param \Drupal\uw_kuali\UWKualiInterface $liveCatalog
   *   The live catalog object.
   * @param string $liveUrlPath
   *   The live URL path.
   *
   * @return array
   *   An array of routes.
   */
  protected function createLiveRoutes(UWKualiInterface $liveCatalog, string $liveUrlPath): array {
    $routes = [];

    $career = strtolower($liveCatalog->getAcademicCareer());

    $kauliId = $liveCatalog->id();
    $routes["uw_kuali." . $career . "_live_catalog"] = new Route(
      $liveUrlPath,
      [
        '_controller' => '\Drupal\uw_kuali\Controller\UWKualiController::content',
        '_title' => $career === 'ug' ? 'Undergraduate studies' : 'Graduate studies',
        'kuali_id' => $kauliId,
      ],
      [
        '_permission' => 'access content',
      ]
    );

    return $routes;
  }

  /**
   * Archive routes, even if live catalog does not exist.
   *
   * @param string $career
   *   Academic career.
   * @param string $liveUrlPath
   *   Used for archive listing page only. May not be needed.
   * @param string $archivePath
   *   Archive url prefix.
   *
   * @return array
   *   Routes array.
   */
  public function createArchiveRoutes(string $career, string $liveUrlPath, string $archivePath): array {
    $archives = $this->kualiService->getArchives($career);
    $career = strtolower($career);

    // Register landing page for archives.
    if (!empty($archives)) {
      $routes["uw_kuali." . $career . "_archives"] = new Route(
        "/" . $liveUrlPath . "/" . $archivePath,
        [
          '_controller' => '\Drupal\uw_kuali\Controller\UWKualiArchiveController::archiveLanding',
          '_title' => 'Archive landing',
          'career' => $career,
        ],
        [
          '_permission' => 'access content',
        ],
      );
    }

    /** @var \Drupal\uw_kuali\UWKualiInterface $archive */
    foreach ($archives as $id => $archive) {
      $routePath = $this->kualiService->generateKualiUrl($archive);
      $routes["uw_kuali." . $career . "_" . $id . "_archive"] = new Route(
        $routePath,
        [
          '_controller' => '\Drupal\uw_kuali\Controller\UWKualiController::content',
          '_title' => Html::escape($archive->label()) ?? 'Academic catalog',
          'kuali_id' => $id,
        ],
        [
          '_permission' => 'access content',
        ]
      );
    }

    return $routes ?? [];
  }

}
