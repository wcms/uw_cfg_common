<?php

/**
 * @file
 * Post update hooks for uw_kuali module.
 *
 * One major advantage the post-update hook has over the update hook
 * is that Drupal is fully functional at the time post-update is run.
 *
 * @see https://www.specbee.com/blogs/update-and-post-update-hooks-for-successful-drupal-site-updates
 */

use Drupal\uw_kuali\Form\UWKualiSettingsForm;

/**
 * Update Kuali configuration.
 */
function uw_kuali_post_update_configuration_update(&$sandbox) {
  $save_config = FALSE;

  // Copy/modify old kuali settings to new.
  // Load an existing Kuali configuration.
  $old_config = \Drupal::config(UWKualiSettingsForm::SETTINGS);

  // If enabled primary kuali, create live catalog for UG.
  $new_config = \Drupal::configFactory()->getEditable(UWKualiSettingsForm::SETTINGS);

  if ($old_config->get('kuali_enabled')) {
    $new_config->set('kuali_ug_enabled', $old_config->get('kuali_enabled'));
    $new_config->set('ug_page_title', $old_config->get('kuali_page_title'));
    $new_config->set('ug_url_path', trim($old_config->get('kuali_url'), '/\\ '));
    $new_config->set('ug_archive_path', 'archive');
    $save_config = TRUE;
  }

  if ($old_config->get('kuali_enabled_secondary')) {
    $new_config->set('kuali_grd_enabled', $old_config->get('kuali_enabled_secondary'));
    $new_config->set('grd_page_title', $old_config->get('kuali_page_title_secondary'));
    $new_config->set('grd_url_path', trim($old_config->get('kuali_url_secondary'), '/\\ '));
    $new_config->set('grd_archive_path', 'archive');
    $save_config = TRUE;
  }

  if ($save_config) {
    // Save updated config.
    $new_config->save(TRUE);
  }

  $etm = \Drupal::entityTypeManager()->getStorage('uw_kuali');

  // If enabled primary kuali, create live catalog for UG.
  if ($old_config->get('kuali_enabled')) {
    $ug_live = [
      'label' => 'Undergraduate Studies',
      'type' => 'uw_kuali',
      'career' => 'UG',
      'status' => 'live',
      'term' => '1249',
      'environment' => $old_config->get('kuali_domain'),
      'catalog_id' => $old_config->get('kuali_catalog_id'),
      'url' => '2024-2025',
    ];

    $etm->create($ug_live)->save();
  }

  // If enabled secondary kuali, create live catalog for GRD.
  if ($old_config->get('kuali_enabled_secondary')) {
    $grd_live = [
      'label' => 'Graduate Studies',
      'type' => 'uw_kuali',
      'career' => 'GRD',
      'status' => 'live',
      'term' => '1249',
      'environment' => $old_config->get('kuali_domain_secondary'),
      'catalog_id' => $old_config->get('kuali_catalog_id_secondary'),
      'url' => 'fall-2024',
    ];

    $etm->create($grd_live)->save();
  }

  // Delete old settings.
  $new_config
    ->clear('kuali_enabled')
    ->clear('kuali_domain')
    ->clear('kuali_catalog_id')
    ->clear('kuali_page_title')
    ->clear('kuali_url')
    ->clear('kuali_enabled_secondary')
    ->clear('kuali_domain_secondary')
    ->clear('kuali_catalog_id_secondary')
    ->clear('kuali_page_title_secondary')
    ->clear('kuali_url_secondary')
    ->save(TRUE);

  // Regenerate routes.
  \Drupal::service('router.builder')->rebuild();

  // Update old menus items. If old menu items are loaded that means
  // kuali is enabled, and route can be updated. This way menu item
  // remains in the same spot (same weight).
  foreach ([
    'route:uw_kuali.url' => 'route:uw_kuali.ug_live_catalog',
    'route:uw_kuali.url_secondary' => 'route:uw_kuali.grd_live_catalog',
  ] as $old_menu => $new_path) {
    $menus = \Drupal::entityTypeManager()->getStorage('menu_link_content')
      ->loadByProperties([
        'link' => $old_menu,
      ]);

    if ($menus) {
      $old_menu_item = current($menus);
      $old_menu_item->set('link', ['uri' => $new_path]);
      $old_menu_item->save();
    }
  }

}
