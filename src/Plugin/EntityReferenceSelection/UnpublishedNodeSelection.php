<?php

namespace Drupal\uw_cfg_common\Plugin\EntityReferenceSelection;

use Drupal\Core\Entity\Plugin\EntityReferenceSelection\DefaultSelection;

/**
 * Entity reference selection plugin to include unpublished nodes.
 *
 * This file should be removed once issue is merged in:
 * See https://www.drupal.org/project/drupal/issues/2845144
 * Remove in Drupal 9.3 (or later).
 *
 * @EntityReferenceSelection(
 *   id = "uw_unpublished",
 *   label = @Translation("Unpublished included"),
 *   entity_types = {"node"},
 *   group = "uw_unpublished",
 *   weight = 0,
 * )
 */
class UnpublishedNodeSelection extends DefaultSelection {}
