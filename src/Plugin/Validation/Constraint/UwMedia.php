<?php

namespace Drupal\uw_cfg_common\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks that the submitted value is a unique integer.
 *
 * @Constraint(
 *   id = "UwMedia",
 *   label = @Translation("Media constraint", context = "Validation"),
 *   type = "string"
 * )
 */
class UwMedia extends Constraint {

  /**
   * Variable for missing media.
   *
   * @var string
   */
  public $mediaMissing = '%value needs to have a media item.';

}
