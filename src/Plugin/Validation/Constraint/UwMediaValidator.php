<?php

namespace Drupal\uw_cfg_common\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the Uw Media constraint.
 */
class UwMediaValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {

    // If the node has a type of media field, then continue
    // to check that it has some media.
    if ($uw_type_of_media = $items->field_uw_type_of_media) {

      // Switch on the type of media select list.
      switch ($uw_type_of_media->value) {

        // If its a hero image, ensure that an image is selected.
        case 'image':

          // If there is no hero image, add the violation.
          if (!$items->field_uw_hero_image->getValue()) {
            $this->context->addViolation($constraint->mediaMissing, ['%value' => 'Hero Image']);
          }
          break;
      }
    }
  }

}
