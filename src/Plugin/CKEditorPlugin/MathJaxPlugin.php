<?php

namespace Drupal\uw_cfg_common\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\editor\Entity\Editor;
use Drupal\Core\Asset\LibrariesDirectoryFileFinder;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the "mathjax" plugin.
 *
 * @CKEditorPlugin(
 *   id = "mathjax",
 *   label = @Translation("Mathjax")
 * )
 */
class MathJaxPlugin extends CKEditorPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Library file finder.
   *
   * @var \Drupal\Core\Asset\LibrariesDirectoryFileFinder
   */
  protected $libFileFinder;

  /**
   * Constructs a BlockComponentRenderArray object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Asset\LibrariesDirectoryFileFinder $libFileFinder
   *   The library file finder.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LibrariesDirectoryFileFinder $libFileFinder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->libFileFinder = $libFileFinder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition, $container->get('library.libraries_directory_file_finder'));
  }

  /**
   * {@inheritdoc}
   */
  public function getDependencies(Editor $editor) {
    return [
      'clipboard',
      'dialog',
      'lineutils',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [
      'mathJaxLib' => '//cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.7/MathJax.js?config=TeX-AMS_HTML',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      'Mathjax' => [
        'label' => $this->t('Math'),
        'image' => $this->libFileFinder->find('ckeditor-mathjax') . '/icons/mathjax.png',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return $this->libFileFinder->find('ckeditor-mathjax') . '/plugin.js';
  }

}
