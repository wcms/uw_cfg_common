<?php

namespace Drupal\uw_cfg_common\Plugin\views\field;

use Drupal\views\Plugin\views\field\Dropbutton;
use Drupal\views\ResultRow;

/**
 * Provides a handler that renders links as dropbutton with access check.
 *
 * Identical to parent class except that render() removes links for which the
 * user has no access.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("uwdropbutton")
 */
class UWDropbutton extends Dropbutton {

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {

    // Have at least empty array to return.
    $links = [];

    // Get the base path of the site.
    $base_path = \Drupal::request()->getBasePath();

    // Step through the links and check the access,
    // also, fix any that need to be fixed.
    foreach ($this->getLinks() as $link) {

      // Ensure that the user has access to the link.
      if ($link['url']->access()) {

        // If there is a base path ensure the destination
        // url is correct, this is more for pantheon.
        if ($base_path !== '') {

          // If there are query parameters, check for the
          // destination parameter, and fix if required.
          if (isset($link['query'])) {

            // If there is a destination url, ensure that it is correct.
            if (isset($link['query']['destination'])) {

              // If the base path is not in the start of the destination
              // url, then add the base path.
              if (!str_starts_with($link['query']['destination'], $base_path)) {
                $link['query']['destination'] = $base_path . $link['query']['destination'];
              }
            }
          }
        }

        // Add the modified link to the links array.
        $links[] = $link;
      }
    }

    // If there are links, return the correct theming,
    // if not, then return empty so nothing is printed
    // on the screen.
    if (!empty($links)) {
      return [
        '#type' => 'dropbutton',
        '#links' => $links,
      ];
    }
    else {
      return '';
    }
  }

}
