<?php

namespace Drupal\uw_cfg_common\Commands;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\ProxyClass\Extension\ModuleInstaller;
use Drupal\Core\State\State;
use Drupal\uw_cfg_common\Service\UWMissingBlocks;
use Drupal\uw_cfg_common\UwPermissions\UwPermissions;
use Drupal\uw_cfg_common\UwRoles\UwRoles;
use Drush\Commands\DrushCommands;
use Drush\Utils\StringUtils;

/**
 * Drush commands for uw_cfg_common module.
 *
 * @package Drupal\uw_cfg_common\Commands
 */
class UwDrushCommands extends DrushCommands {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Service to deal with missing blocks.
   *
   * @var \Drupal\uw_cfg_common\Service\UWMissingBlocks
   */
  protected $missingBlocks;

  /**
   * Config factory from core.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Module installer.
   *
   * @var \Drupal\Core\ProxyClass\Extension\ModuleInstaller
   */
  protected $moduleInstaller;

  /**
   * State from core.
   *
   * @var \Drupal\Core\State\State
   */
  protected $state;

  /**
   * {@inheritDoc}
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    UWMissingBlocks $missingBlocks,
    ConfigFactoryInterface $configFactory,
    ModuleHandlerInterface $moduleHandler,
    ModuleInstaller $moduleInstaller,
    State $state
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->missingBlocks = $missingBlocks;
    $this->configFactory = $configFactory;
    $this->moduleHandler = $moduleHandler;
    $this->moduleInstaller = $moduleInstaller;
    $this->state = $state;
  }

  /**
   * Drush command to set permissions to what is in user role yml files.
   *
   * Yml files are located: /src/UwRoles. This command will only set
   * permissions to what is in the yml file. It will not remove any
   * permission. To remove a permission you need an update hook as well as
   * remove it from the yml file.
   *
   * @command uw:permissions
   * @aliases uwperm
   * @usage uwperm
   */
  public function updatePermissions() {

    // Get all the role ids in the system.
    $rids = UwRoles::getAllRoles();

    // Step through each rid and set the permissions.
    $all = UwPermissions::setAccessPermissions();

    foreach ($rids as $rid) {
      // Get the info about the role.
      $uw_role = UwRoles::getUwRole($rid);

      // Array to hold additional access content permissions for each role.
      $additional = [];
      if ($uw_role['label'] && !empty($all[$uw_role['label']])) {
        $additional = $all[$uw_role['label']];
      }

      // Set the permissions for the role.
      UwRoles::setUwPermissions($uw_role, $additional);

      // Set message for specific role setting permissions.
      // The logger() may not exist if this is called outside of Drush.
      if ($this->logger()) {
        $this->logger()->success('Permissions set for ' . $uw_role['label'] . '.');
      }
    }

    // Set message for command completed.
    if ($this->logger()) {
      $this->logger()->success('All permissions set.');
    }
  }

  /**
   * Removes missing blocks from unsaved layout.
   *
   * If not existing block is in temp storage for layout build (unsaved)
   * changes and layout tab is opened this can result in server error. This
   * function will check each block existence (loading block revision) and
   * if block is loaded successfully it will leave it in temp storage.
   * Otherwise, block will be removed from unsaved changes.
   *
   * @param string|null $nids
   *   List (CSV) of nodes to check, if empty command will check all nodes.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *
   * @command uw:missing-blocks-unsaved
   * @aliases uwblocksunsaved
   * @usage drush uwblocksunsaved "102,201,304"
   *   - Checks nodes with id 102,201,304 and remove not existing blocks.
   */
  public function removeBlocksFromUnsavedLayout(string $nids = NULL): void {
    $nodes = NULL;

    if ($nids) {
      $nodes = StringUtils::csvToArray($nids);
    }

    $this->missingBlocks->removeMissingBlocksFromUnsaved($nodes);
    $this->logger()->success('Missing blocks checks complete on unsaved.');
  }

  /**
   * Removes missing blocks from saved layouts.
   *
   * Scans all nodes, checks each for missing blocks in layout builder.
   * Removes missing blocks if found, and saves node. Checks revisions also.
   *
   * @param string|null $nids
   *   List (CVS) of nodes to check, if empty command will check all nodes.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   *
   * @command uw:missing-blocks-saved
   * @aliases uwblockssaved
   * @usage drush uwblockssaved 102
   *   - Checks node (nid: 102) for any missing blocks.
   * @usage drush uwblockssaved "103,104,105,106"
   *   - Checks nodes 103, 104, 105, 106 for missing blocks.
   */
  public function removeBlockFromLayout(string $nids = NULL): void {
    $nodes = NULL;

    if ($nids) {
      $nodes = StringUtils::csvToArray($nids);
    }

    $this->missingBlocks->removeMissingBlocksFromSaved($nodes);
    $this->logger()->success('Missing blocks checks complete on saved.');
  }

  /**
   * Command to combine saved and unsaved removal of missing blocks.
   *
   * @param string|null $nids
   *   List of node ids to process only.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   *
   * @command uw:missing-blocks
   * @aliases uwblocks
   * @usage drush uwblocks
   *   - Run checkup on all nodes. This may take some time to complete.
   * @usage drush uwblocks 102
   *   - Checks node (nid: 12) for any missing blocks(covers saved and unsaved).
   * @usage drush uwblocks "103,104,105,106"
   *   - Checks nodes 103, 104, 105, 106 for missing blocks (saved and unsaved).
   */
  public function removeMissingBlocks(string $nids = NULL): void {
    $nodes = NULL;

    if ($nids) {
      $nodes = StringUtils::csvToArray($nids);
    }

    $this->logger()->notice('Running clean up for saved nodes.');
    $this->missingBlocks->removeMissingBlocksFromSaved($nodes);
    $this->logger()->notice('Running clean up for unsaved nodes.');
    $this->missingBlocks->removeMissingBlocksFromUnsaved($nodes);

    $this->logger()->success('Missing blocks removed from saved and unsaved layouts.');
  }

  /**
   * Command to retrieve scheduler's lightweight cron key.
   *
   * @return string
   *   Lightweight cron key.
   *
   * @command uw:lightweight-cron-key
   * @aliases uw-lwc-key
   * @usage drush uw-lwc-key
   *   - Return light weight cron key.
   */
  public function getLightWeightCronKey(): string {
    return $this->state->get('scheduler_lightweight_cron_access_key');
  }

  /**
   * Command to retrieve scheduler's lightweight cron path.
   *
   * @return string
   *   Lightweight cron path.
   *
   * @command uw:lightweight-cron-path
   * @aliases uw-lwc-path
   * @usage drush uw-lwc-path
   *   - Return light weight cron url with key.
   */
  public function getLightWeightCronPath(): string {
    $path = '';

    if ($key = $this->getLightWeightCronKey()) {
      $path = '/scheduler/cron/' . $key;
    }

    return $path;
  }

  /**
   * Drush command to cleanup after a migration.
   *
   * @command mim:cleanup
   * @aliases mimcu
   * @usage mimcu
   */
  public function migrationCleanUp() {

    // Modules to uninstall.
    $modules = [
      'uw_migrate',
      'webform_migrate',
      'webform_node',
    ];

    // Step through each of the modules, ensure that they
    // are enabled, and if enabled, uninstall.
    foreach ($modules as $module) {

      // If the module is enabled, uninstall it.
      if ($this->moduleHandler->moduleExists($module)) {

        // Uninstall the module.
        $this->moduleInstaller->uninstall([$module]);

        // Log to the screen.
        $this->logger()->success('Uninstalled: ' . $module);
      }
    }
  }

}
