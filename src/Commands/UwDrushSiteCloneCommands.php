<?php

namespace Drupal\uw_cfg_common\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\uw_cfg_common\Service\UWSiteCloneFieldCandidatesInterface;
use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class UwDrushSiteCloneCommands extends DrushCommands {

  /**
   * Default constructor using constructor property promotion.
   *
   * @param \Drupal\uw_cfg_common\Service\UWSiteCloneFieldCandidatesInterface $fieldCandidates
   *   List of fields that can hold URL.
   */
  public function __construct(protected UWSiteCloneFieldCandidatesInterface $fieldCandidates) {
    parent::__construct();
  }

  /**
   * Print a list of field candidates that may hold absolute links.
   *
   * @command uw:site-clone-field-candidates
   * @aliases uwfica
   *
   * @usage drush fica --fields=group --format=csv
   *  Available formats: csv, table(default), json, yaml.
   */
  public function siteCloneFieldCandidates($options = [
    'format' => 'table',
    'fields' => [],
  ]): ?RowsOfFields {
    $fields = $this->fieldCandidates->getFieldCandidates();
    $rows = [];

    $this->logger()
      ->notice(dt('Displaying field names that might be candidates to hold absolute url.'));

    foreach ($fields as $ct_name => $ct_fields) {
      foreach ($ct_fields as $f_name => $field) {
        foreach ($field as $name) {
          $rows[] = [
            'group' => $ct_name,
            'type' => $f_name,
            'field' => $name,
          ];
        }
      }
    }

    return !empty($rows) ? new RowsOfFields($rows) : $this->logger()->warning(dt('No results to display.'));
  }

}
