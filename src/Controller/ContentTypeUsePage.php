<?php

namespace Drupal\uw_cfg_common\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\uw_cfg_common\Service\UWServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Provides route responses for the uw_cfg_common module.
 */
/**
 * Provides route responses for the Example module.
 */
class ContentTypeUsePage extends ControllerBase {
  /**
   * Entity type manager from the core.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface\
   */
  protected $entityTypeManager;

  /**
   * UW service.
   *
   * @var Drupal\uw_cfg_common\Service\UWServiceInterface
   */
  protected $uwService;

  /**
   * ContentTypeUseController constructor.
   *
   * @param Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager from the core.
   * @param Drupal\uw_cfg_common\Service\UWServiceInterface $uwService
   *   UW Service.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    UWServiceInterface $uwService
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->uwService = $uwService;
  }

  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Drupal service container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('uw_cfg_common.uw_service')
    );
  }

  /**
   * Returns a json of the content type usage.
   *
   * @return Symfony\Component\HttpFoundation\JsonResponse
   *   A simple renderable array.
   */
  public function contentTypeUse(): JsonResponse {

    // Get the content types.
    $content_types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();

    // Fetch the content types in type array.
    foreach ($content_types as $content_type) {
      $cts[] = $content_type->get('type');
    }

    // Call to the service to get the count of content types used.
    $list = $this->uwService->getContentTypeUsage($cts, TRUE);

    return new JsonResponse($list);
  }

}
