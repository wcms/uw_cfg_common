<?php

namespace Drupal\uw_cfg_common\Controller;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;
use Drupal\path_alias\AliasManager;
use Drupal\transliterate_filenames\SanitizeName;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides CSV files for specific UW sites.
 */
class UwDownloadCsvController extends ControllerBase {

  /**
   * Entity type manager from the core.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The path alias.
   *
   * @var \Drupal\path_alias\AliasManager
   */
  protected $pathAlias;

  /**
   * Sanitize name.
   *
   * @var Drupal\transliterate_filenames\SanitizeName
   */
  protected $sanitizeName;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * CSV Report constructor.
   *
   * @param Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param Drupal\path_alias\AliasManager $pathAlias
   *   The path alias.
   * @param Drupal\transliterate_filenames\SanitizeName $sanitizeName
   *   The sanitize name.
   * @param Drupal\Core\Config\ConfigFactory $configFactory
   *   The config factory.
   * @param \Drupal\Core\Database\Connection $database
   *   The database.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    AliasManager $pathAlias,
    SanitizeName $sanitizeName,
    ConfigFactory $configFactory,
    Connection $database
  ) {

    $this->entityTypeManager = $entityTypeManager;
    $this->pathAlias = $pathAlias;
    $this->sanitizeName = $sanitizeName;
    $this->configFactory = $configFactory;
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('path_alias.manager'),
      $container->get('transliterate_filenames.sanitize_name'),
      $container->get('config.factory'),
      $container->get('database')
    );
  }

  /**
   * Provides a CSV file for content on a site.
   *
   * @return Symfony\Component\HttpFoundation\Response
   *   The page response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function uwContentReport() {

    // Get all the nodes.
    $nodes = $this->entityTypeManager->getStorage('node')->loadMultiple();

    // Just in case there are no nodes, there should always
    // be a home page though, this is just to ensure no errors.
    $response = NULL;

    if ($nodes) {

      // The header of the CSV.
      $data = [
        $this->t('Node ID'),
        $this->t('Title'),
        $this->t('Content type'),
        $this->t('Path'),
        $this->t('Published'),
        $this->t('Created'),
        $this->t('Last updated'),
      ];

      // Put header for CSV into format to be used in CSV.
      $rows[] = implode(',', $data);

      // Step through each of the nodes and get the info.
      foreach ($nodes as $node) {

        // Pull out the info about the node, making sure the title is CSV-safe.
        $data = [
          'nid' => $node->id(),
          'title' => '"' . str_replace('"', '""', $node->getTitle()) . '"',
          'type' => $node->type->entity->label(),
          'path' => $this->pathAlias->getAliasByPath('/node/' . $node->id()),
          'published' => $node->status->value == 1 ? 'yes' : 'no',
          'created' => date('Y-m-d G:i', $node->getCreatedTime()),
          'updated' => date('Y-m-d G:i', $node->getChangedTime()),
        ];

        // Store the node info for use later in the CSV.
        $rows[] = implode(',', $data);
      }

      // Put the info into a large CSV format.
      $content = implode("\n", $rows);

      // Create the response for the CSV page.
      $response = new Response($content);

      // Determine the filename for the CSV.
      $sitename = $this->configFactory->get('system.site')->get('name');
      $filename = $sitename . '_content_report.csv';

      // Make sure we are using a safe filename.
      $filename = $this->sanitizeName->sanitizeFilename($filename);

      // Set the headers for the CSV.
      $response->headers->set('Content-Type', 'text/csv');
      $response->headers->set('Content-Disposition', 'attachment; filename="' . $filename . '"');
    }

    return $response;
  }

  /**
   * Provides a CSV file for menus on a site.
   *
   * @return Symfony\Component\HttpFoundation\Response
   *   The page response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function uwMenuReport() {

    // This is just a fail-safe in case there are no menus,
    // which should never happen, but just so page won't crash.
    $content = NULL;

    // The menus to be used.
    $menu_names = [
      'main',
      'uw-menu-audience-menu',
    ];

    // The header.
    $data = [
      $this->t('Menu name'),
      $this->t('Link ID'),
      $this->t('Parent link ID'),
      $this->t('Alias'),
      $this->t('Link path'),
      $this->t('Title'),
      $this->t('Has children'),
      $this->t('Part 1'),
      $this->t('Part 2'),
      $this->t('Part 3'),
      $this->t('Part 4'),
      $this->t('Part 5'),
      $this->t('Part 6'),
      $this->t('Part 7'),
      $this->t('Part 8'),
      $this->t('Part 9'),
    ];

    // Put header for CSV into format to be used in CSV.
    $rows[] = implode(',', $data);

    // Step through each of the menu names and get the info.
    foreach ($menu_names as $menu_name) {

      // Query to get the info about the menu.
      $query = $this->database->select('menu_tree', 'mt')
        ->fields(
          'mt',
          [
            'id',
            'menu_name',
            'mlid',
            'title',
            'parent',
            'has_children',
            'route_name',
            'url',
            'p1',
            'p2',
            'p3',
            'p4',
            'p5',
            'p6',
            'p7',
            'p8',
            'p9',
          ]
        )
        ->condition('menu_name', $menu_name);

      // Execute the query.
      $links = $query->execute()->fetchAll();

      // Step through all the links of the menu and get
      // the info about the link.
      foreach ($links as $link) {

        // If the link id contains menu_link_content, we need to
        // get the uri of the link from another table.
        if (str_contains($link->id, 'menu_link_content:')) {

          // Remove the menu_link_content: from the link id.
          $uuid = str_replace('menu_link_content:', '', $link->id);

          // Query to get the link__uri.
          $query = $this->database->select('menu_link_content', 'mlc');
          $query->join('menu_link_content_data', 'mlcd', 'mlc.id = mlcd.id');
          $query->fields('mlcd', ['link__uri'])
            ->condition('mlc.uuid', $uuid);

          // Execute the query.
          $menu_link_content = current($query->execute()->fetchAll());

          // Set the link url from the menu link content db query.
          $link_url = $menu_link_content->link__uri;

          $alias = Url::fromUri($link_url)->toString();
        }
        else {

          // If there is no url and there ius a route name,
          // use the route name as the url.  If there is
          // a url just use the url.
          if (!$link->url && $link->route_name) {
            $link_url = $link->route_name;
          }
          else {
            $link_url = $link->url;
          }

          // Just give the alias a null value, since if we
          // get to here there will be no alias.
          $alias = NULL;
        }

        // Set the parent id to the one from the link.
        // We may change this later, depending on what
        // the parent like is.
        $parent_id = $link->parent;

        // If there is a parent link, check if we need to get
        // the id or not.
        if ($parent_id) {

          // If the parent id has menu_link_content in it, we need
          // to get the id of that parent link using the uuid.
          if (str_contains($link->parent, 'menu_link_content:')) {

            // Remove the menu_link_content: from the parent id.
            $uuid = str_replace('menu_link_content:', '', $link->parent);

            // Get the parent link id from the database using the uuid.
            $parent = current(
              $this->database->select('menu_link_content', 'mlc')
                ->fields('mlc', ['id'])
                ->condition('uuid', $uuid)
                ->execute()
                ->fetchAll()
            );

            // Set the parent id to the one we got using uuid.
            $parent_id = $parent->id;
          }
        }

        // Set the data about the menu link, making sure the title is CSV-safe.
        $data = [
          'menu_name' => $link->menu_name,
          'link_id' => $link->mlid,
          'parent_link_id' => $parent_id,
          'alias' => $alias,
          'link_path' => $link_url,
          'title' => '"' . str_replace('"', '""', unserialize($link->title)) . '"',
          'has_children' => $link->has_children == 1 ? 'yes' : 'no',
          'p1' => $link->p1,
          'p2' => $link->p2,
          'p3' => $link->p3,
          'p4' => $link->p4,
          'p5' => $link->p5,
          'p6' => $link->p6,
          'p7' => $link->p7,
          'p8' => $link->p8,
          'p9' => $link->p9,
        ];

        // Store the node info for use later in the CSV.
        $rows[] = implode(',', $data);
      }

      // Put the info into a large CSV format.
      $content = implode("\n", $rows);
    }

    // Create the response for the CSV page.
    $response = new Response($content);

    // Determine the filename for the CSV.
    $sitename = $this->configFactory->get('system.site')->get('name');
    $filename = $sitename . '_menu_report.csv';

    // Make sure we are using a safe filename.
    $filename = $this->sanitizeName->sanitizeFilename($filename);

    // Set the headers for the CSV.
    $response->headers->set('Content-Type', 'text/csv');
    $response->headers->set('Content-Disposition', 'attachment; filename="' . $filename . '"');

    return $response;
  }

}
