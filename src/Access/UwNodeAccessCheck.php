<?php

namespace Drupal\uw_cfg_common\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\menu_admin_per_menu\Access\MenuAdminPerMenuAccess;

/**
 * Checks access for displaying configuration translation page.
 */
class UwNodeAccessCheck implements AccessInterface {

  /**
   * A custom access check.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   Route matching.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function access(RouteMatchInterface $route_match, AccountInterface $account): AccessResult {
    switch ($route_match->getRouteName()) {
      // Menu link edit pages.
      case 'menu_ui.link_edit':
        $menu_link_plugin = $route_match->getParameter('menu_link_plugin');
        // Only those with permission may edit home page menu entry.
        if ($menu_link_plugin->getPluginId() === 'uw_base_profile.front_page') {
          return $account->hasPermission('bypass home page protection') ? AccessResult::allowed() : AccessResult::forbidden();
        }
        // Otherwise, default to access set in menu_admin_per_menu.
        $menu_admin_per_menu = new MenuAdminPerMenuAccess();
        return $menu_admin_per_menu->menuLinkAccess($account, $menu_link_plugin);

      // Dashboard config: admin/config/dashboards/dashboardssettings.
      case 'dashboards.dashboards_settings_form':
        return $account->hasPermission('access dashboard config') ? AccessResult::allowed() : AccessResult::forbidden();

      // Menu link add, edit, and delete pages.
      case 'entity.menu.add_link_form':
      case 'entity.menu_link_content.canonical':
      case 'entity.menu_link_content.edit_form':
      case 'entity.menu_link_content.delete_form':
        return $account->hasPermission('administer menu') ? AccessResult::allowed() : AccessResult::forbidden();

    }

    // Get the node object, which is in the route match variable.
    $node = $route_match->getParameter('node');

    // Check if this is a sidebar content type and if the user has permission
    // to edit the content type. Return access denied when user has no edit
    // permission.
    if ($node && $node->bundle() == 'uw_ct_sidebar' && !$account->hasPermission('edit any uw_ct_sidebar content')) {
      return AccessResult::forbidden();
    }

    // Check if this is a footer content type and if the user has permission
    // to edit the content type. Return access denied when user has no edit
    // permission.
    if ($node && $node->bundle() == 'uw_ct_site_footer' && !$account->hasPermission('edit any uw_ct_site_footer content')) {
      return AccessResult::forbidden();
    }

    // The Expand/collapse group nodes should only be visible if the logged-in
    // users have 'Expand/collapse group: Edit any content' permission.
    if ($node && $node->bundle() == 'uw_ct_expand_collapse_group' && !$account->hasPermission('edit any uw_ct_expand_collapse_group content')) {
      return AccessResult::forbidden();
    }

    // We have to return some type of access, so we are going to return
    // allowed, if they do not have access, the new exception is going to be
    // thrown above.
    return AccessResult::allowed();
  }

}
