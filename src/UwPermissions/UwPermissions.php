<?php

namespace Drupal\uw_cfg_common\UwPermissions;

use Drupal\user\Entity\Role;
use Symfony\Component\Yaml\Yaml;

/**
 * Class UwPermissions.
 *
 * UW Specific permissions.
 */
class UwPermissions {

  /**
   * Get UW roles.
   *
   * @return array
   *   An array of the UW roles to be used on this form.
   */
  public static function getRoles(): array {

    // UW site manager role.
    $uw_roles['Site manager'] = [
      'name' => 'Site manager',
      'id' => 'uw_role_site_manager',
      'object' => Role::load('uw_role_site_manager'),
    ];

    // UW content editor role.
    $uw_roles['Content editor'] = [
      'name' => 'Content editor',
      'id' => 'uw_role_content_editor',
      'object' => Role::load('uw_role_content_editor'),
    ];

    // UW content author role.
    $uw_roles['Content author'] = [
      'name' => 'Content author',
      'id' => 'uw_role_content_author',
      'object' => Role::load('uw_role_content_author'),
    ];

    return $uw_roles;
  }

  /**
   * Get Uw content permissions array.
   *
   * @return array
   *   The array of all permissions for uw content access form.
   */
  public static function getPermissionsArray(): array {

    $uw_permissions = [

      // Blog permissions.
      'Blog' => [
        'Use content type' =>
        UwPermissions::buildRolePermissionsListContentType(
          'uw_ct_blog'
        ),
        'Create/edit tags' =>
        UwPermissions::buildRolePermissionsListTaxonomyTerm(
          'uw_vocab_blog_tags',
          ['create', 'edit']
        ),
        'Delete tags' =>
        UwPermissions::buildRolePermissionsListTaxonomyTerm(
          'uw_vocab_blog_tags',
          ['delete']
        ),
      ],

      // Contact permissions.
      'Contact' => [
        'Use content type' =>
        UwPermissions::buildRolePermissionsListContentType(
         'uw_ct_contact'
        ),
        'Create/edit groups' =>
        UwPermissions::buildRolePermissionsListTaxonomyTerm(
          'uw_vocab_contact_group',
          ['create', 'edit']
        ),
        'Delete groups' =>
        UwPermissions::buildRolePermissionsListTaxonomyTerm(
          'uw_vocab_contact_group',
          ['delete']
        ),
        'Rearrange' =>
        UwPermissions::buildRolePermissionsListCustom(
          'rearrange contacts'
        ),
      ],

      // Catalog permissions.
      'Catalog' => [
        'Use content type' =>
        UwPermissions::buildRolePermissionsListContentType(
          'uw_ct_catalog_item'
        ),
        'Create/edit audience' =>
        UwPermissions::buildRolePermissionsListTaxonomyTerm(
          'uw_vocab_audience',
          ['create', 'edit']
        ),
        'Delete audience' =>
        UwPermissions::buildRolePermissionsListTaxonomyTerm(
          'uw_vocab_audience',
          ['delete']
        ),
        'Create/edit categories' =>
        UwPermissions::buildRolePermissionsListTaxonomyTerm(
          'uw_vocab_catalog_categories',
          ['create', 'edit']
        ),
        'Delete categories' =>
        UwPermissions::buildRolePermissionsListTaxonomyTerm(
          'uw_vocab_catalog_categories',
          ['delete']
        ),
        'Create/edit catalogs' =>
        UwPermissions::buildRolePermissionsListTaxonomyTerm(
          'uw_vocab_catalogs',
          ['create', 'edit']
        ),
        'Delete catalogs' =>
        UwPermissions::buildRolePermissionsListTaxonomyTerm(
          'uw_vocab_catalogs',
          ['delete']
        ),
      ],

      // Event permissions.
      'Event' => [
        'Use content type' =>
        UwPermissions::buildRolePermissionsListContentType(
          'uw_ct_event'
        ),
        'Create/edit tags' =>
        UwPermissions::buildRolePermissionsListTaxonomyTerm(
          'uw_tax_event_tags',
          ['create', 'edit']
        ),
        'Delete tags' =>
        UwPermissions::buildRolePermissionsListTaxonomyTerm(
          'uw_tax_event_tags',
          ['delete']
        ),
        'Create/edit types' =>
        UwPermissions::buildRolePermissionsListTaxonomyTerm(
          'uw_tax_event_type',
          ['create', 'edit']
        ),
        'Delete types' =>
        UwPermissions::buildRolePermissionsListTaxonomyTerm(
          'uw_tax_event_type',
          ['delete']
        ),
      ],

      // Expand/Collapse.
      'Expand/Collapse Group' => [
        'Use content type' =>
        UwPermissions::buildRolePermissionsListContentType(
          'uw_ct_expand_collapse_group'
        ),
      ],

      // News permissions.
      'News' => [
        'Use content type' =>
        UwPermissions::buildRolePermissionsListContentType(
          'uw_ct_news_item'
        ),
        'Create/edit tags' =>
        UwPermissions::buildRolePermissionsListTaxonomyTerm(
          'uw_vocab_news_tags',
          ['create', 'edit']
        ),
        'Delete tags' =>
        UwPermissions::buildRolePermissionsListTaxonomyTerm(
          'uw_vocab_news_tags',
          ['delete']
        ),
      ],

      // Opportunity permissions.
      'Opportunity' => [
        'Use content type' =>
        UwPermissions::buildRolePermissionsListContentType(
          'uw_ct_opportunity'
        ),
      ],

      // Profile permissions.
      'Profile' => [
        'Use content type' =>
        UwPermissions::buildRolePermissionsListContentType(
         'uw_ct_profile'
        ),
        'Create/edit types' =>
        UwPermissions::buildRolePermissionsListTaxonomyTerm(
         'uw_vocab_profile_type',
          ['create', 'edit']
        ),
        'Delete types' =>
        UwPermissions::buildRolePermissionsListTaxonomyTerm(
         'uw_vocab_profile_type',
          ['delete']
        ),
        'Rearrange' =>
        UwPermissions::buildRolePermissionsListCustom(
          'rearrange profiles'
        ),
      ],

      // Project permissions.
      'Project' => [
        'Use content type' =>
        UwPermissions::buildRolePermissionsListContentType(
        'uw_ct_project'
        ),
        'Create/edit roles' =>
        UwPermissions::buildRolePermissionsListTaxonomyTerm(
        'uw_vocab_project_role',
        ['create', 'edit']
        ),
        'Delete roles' =>
        UwPermissions::buildRolePermissionsListTaxonomyTerm(
         'uw_vocab_project_role',
        ['delete']
        ),
        'Create/edit topics' =>
        UwPermissions::buildRolePermissionsListTaxonomyTerm(
        'uw_vocab_project_topic',
        ['create', 'edit']
        ),
        'Delete topics' =>
        UwPermissions::buildRolePermissionsListTaxonomyTerm(
        'uw_vocab_project_topic',
        ['delete']
        ),
      ],

      // Publication references permissions.
      'Publication references' => [
        'Use' => UwPermissions::buildBibcitePermissions('use'),
        'Delete' => UwPermissions::buildBibcitePermissions('delete'),
        'Export' => UwPermissions::buildBibcitePermissions('export'),
        'Import' => UwPermissions::buildBibcitePermissions('import'),
      ],

      // Service permissions.
      'Service' => [
        'Use content type' =>
        UwPermissions::buildRolePermissionsListContentType(
        'uw_ct_service'
        ),
        'Create/edit categories' =>
        UwPermissions::buildRolePermissionsListTaxonomyTerm(
        'uw_vocab_service_categories',
          ['create', 'edit']
        ),
        'Delete categories' =>
        UwPermissions::buildRolePermissionsListTaxonomyTerm(
        'uw_vocab_service_categories',
          ['delete']
        ),
      ],

      // Sidebar permissions.
      'Sidebar' => [
        'Use content type' =>
        UwPermissions::buildRolePermissionsListContentType(
          'uw_ct_sidebar'
        ),
      ],

      // Site footer permissions.
      'Site footer' => [
        'Use content type' =>
        UwPermissions::buildRolePermissionsListContentType(
          'uw_ct_site_footer'
        ),
      ],

      'Section library' => [
        'Manage templates' => UwPermissions::buildRolePermissionsSectionLibrary('manage'),
        'Create templates' => UwPermissions::buildRolePermissionsSectionLibrary('create'),
        'Use templates' => UwPermissions::buildRolePermissionsSectionLibrary('use'),
      ],

      // Special alert permissions.
      'Special alert' => [
        'Use' =>
        UwPermissions::buildRolePermissionsListCustom(
          'administer special alert'
        ),
      ],

      // Web page permissions.
      'Web page' => [
        'Use content type' =>
        UwPermissions::buildRolePermissionsListContentType(
          'uw_ct_web_page'
        ),
      ],

      // CSV downloads permissions.
      'CSV downloads' => [
        'Access the CSV content and menu reports' => [
          'Site manager' => ['view UW CSV reports'],
          'Content editor' => ['view UW CSV reports'],
          'Content author' => ['view UW CSV reports'],
        ],
      ],
    ];

    // Sort the array.
    ksort($uw_permissions, SORT_NATURAL | SORT_FLAG_CASE);

    // Add any help text as required.
    $uw_permissions['help_text']['CSV downloads'] = 'Note: that these contain all content regardless of the user’s other permissions.';

    return $uw_permissions;
  }

  /**
   * Function to get the permissions for bibcite.
   *
   * @param string $type
   *   The type of permission to get.
   *
   * @return array
   *   Array of permissions for the specified type.
   */
  public static function buildBibcitePermissions(string $type): array {

    // The array of permissions.
    $permissions = [];

    // The roles used for the uw permissions.
    $uw_roles = UwPermissions::getRoles();

    switch ($type) {
      case 'use':

        // The permissions to add for use content type.
        $perms_to_add = [
          'create bibcite_contributor',
          'create bibcite_keyword',
          'create bibcite_reference',
          'edit bibcite_contributor',
          'edit bibcite_keyword',
          'edit any bibcite_reference',
          'revert all bibcite_reference revisions',
          'administer bibcite_contributor',
          'administer bibcite_keyword',
          'administer bibcite_reference',
          'view all bibcite_reference revisions',
        ];

        break;

      case 'delete':

        // The permissions to add for delete bibcite.
        $perms_to_add = [
          'delete bibcite_contributor',
          'delete bibcite_keyword',
          'delete all bibcite_reference revisions',
          'delete any bibcite_reference',
        ];

        break;

      case 'export':

        // The permissions to add for export bibcite.
        $perms_to_add = [
          'access bibcite export all',
        ];

        break;

      case 'import':

        // The permissions to add for import bibcite.
        $perms_to_add = [
          'bibcite import',
          'bibcite populate',
        ];

        break;
    }

    // If there are permissions to add, then add them
    // to the permissions array.
    if (!empty($perms_to_add)) {

      // Step through each role and add the permissions
      // to the array.
      foreach ($uw_roles as $uw_role) {
        $permissions[$uw_role['name']] = $perms_to_add;
      }
    }

    return $permissions;
  }

  /**
   * Convert the permissions array and send it to grantRevoke.
   */
  public static function setAccessPermissions(): array {

    // Load and transform content-access permissions.
    $all_permissions = UwPermissions::getPermissionsArray();
    $module_handler = \Drupal::service('module_handler');
    $module_path = $module_handler->getModule('uw_cfg_common')->getPath();
    $yaml_perm = Yaml::parseFile($module_path . '/src/UwRoles/access_content_permissions.yml');
    $all = [];
    foreach ($yaml_perm as $ct => $actions) {

      foreach ($actions as $name => $roles) {

        foreach (['Site manager', 'Content author', 'Content editor'] as $role) {

          if (!empty($all_permissions[$ct][$name][$role])) {
            if (!isset($all[$role])) {
              $all[$role] = [];
            }

            $all[$role] = array_merge($all[$role], $all_permissions[$ct][$name][$role]);
          }
        }
      }
    }

    return $all;
  }

  /**
   * Build uw role permissions list for content types.
   *
   * @param string $ct_name
   *   The machine name of the content type.
   *
   * @return array
   *   An array of the uw permissions.
   */
  public static function buildRolePermissionsListContentType(string $ct_name): array {

    // Build the permissions list for the content type.
    $content_type_permissions_list = [
      'Site manager' => [
        'create ' . $ct_name . ' content',
        'delete any ' . $ct_name . ' content',
        'delete own ' . $ct_name . ' content',
        'edit any ' . $ct_name . ' content',
        'edit own ' . $ct_name . ' content',
        'revert ' . $ct_name . ' revisions',
        'view ' . $ct_name . ' revisions',
      ],
      'Content editor' => [
        'create ' . $ct_name . ' content',
        'edit any ' . $ct_name . ' content',
        'edit own ' . $ct_name . ' content',
        'revert ' . $ct_name . ' revisions',
        'view ' . $ct_name . ' revisions',
      ],
      'Content author' => [
        'create ' . $ct_name . ' content',
        'edit any ' . $ct_name . ' content',
        'edit own ' . $ct_name . ' content',
        'revert ' . $ct_name . ' revisions',
        'view ' . $ct_name . ' revisions',
      ],
    ];

    return $content_type_permissions_list;
  }

  /**
   * Build role permissions list for a custom permission.
   *
   * @param string $permission_name
   *   The machine name of the taxonomy term.
   *
   * @return array
   *   An array of the uw permissions.
   */
  public static function buildRolePermissionsListCustom(string $permission_name): array {

    // The roles used for the uw permissions.
    $uw_roles = UwPermissions::getRoles();

    // Step through each role and add permission.
    foreach ($uw_roles as $uw_role) {

      // Set the permission.
      $uw_permissions[$uw_role['name']][] = $permission_name;
    }

    return $uw_permissions;
  }

  /**
   * Build role permissions list for taxonomy terms.
   *
   * @param string $tax_name
   *   The machine name of the taxonomy term.
   * @param array $permission_types
   *   The list of permissions for the taxonomy term (create, edit, and/or
   *   delete).
   *
   * @return array
   *   An array of the uw permissions.
   */
  public static function buildRolePermissionsListTaxonomyTerm(string $tax_name, array $permission_types): array {

    // The roles used for the uw permissions.
    $uw_roles = UWPermissions::getRoles();

    // Step through each of the uw roles and setup list of permissions.
    foreach ($uw_roles as $uw_role) {

      // Step through each permission types and setup list of permissions.
      foreach ($permission_types as $permission_type) {

        // Set the permission.
        $uw_permissions[$uw_role['name']][] = $permission_type . ' terms in ' . $tax_name;
      }
    }

    return $uw_permissions;
  }

  /**
   * Save UW permissions.
   *
   * @parm array $uw_roles
   *   The array of roles to be saved.
   */
  public static function save(array $uw_roles) {

    // Step through each of the roles and save the role object,
    // so that the permissions get saved.
    foreach ($uw_roles as $uw_role) {

      // Save the role object.
      $uw_role['object']->save();
    }
  }

  /**
   * Grant/revoke UW permission to roles and save.
   *
   * @param array $permissions_to_process
   *   The array of permissions to be granted or revoked.
   * @param string $type
   *   The type of permissions to be processed (grant/revoke).
   */
  public static function grantRevoke(array $permissions_to_process, string $type): void {

    // Get the UW roles.
    $uw_roles = UwPermissions::getRoles();

    // The array of uw permissions.
    $uw_permissions = UwPermissions::getPermissionsArray();

    // Step through each of the permissions to grant/revoke.
    foreach ($permissions_to_process as $feature => $permission_to_process) {

      // Ensure that we have at least permissions to process.
      if ($permission_to_process) {

        // Step through the feature and get the roles.
        foreach ($permission_to_process as $permission => $roles) {

          // Ensure that we have roles to process.
          if ($roles) {

            // Step through each of the roles and grant/revoke the permission.
            foreach ($roles as $role) {

              // Step through each of the permissions that need to be
              // granted/revoke and grant/revoke for the specified
              // uw role.
              foreach ($uw_permissions[$feature][$permission][$role] as $uw_perm) {

                // If type is grant, grant the permission for the role.
                if ($type == 'grant') {

                  // Grant the permission for the specified role.
                  $uw_roles[$role]['object']->grantPermission($uw_perm);
                }
                // If the type is revoke, revoke the permission for the role.
                elseif ($type == 'revoke') {

                  // Revoke the permission for the specified role.
                  $uw_roles[$role]['object']->revokePermission($uw_perm);
                }
              }
            }
          }
        }
      }
    }

    // Save the permissions.
    UwPermissions::save($uw_roles);
  }

  /**
   * Set default permissions for Section library (layout builder).
   *
   * @return array[]
   *   Permissions in UW format.
   */
  public static function buildRolePermissionsSectionLibrary(string $group): array {
    return match($group) {
      'manage' => [
        'Site manager' => [
          'delete section library templates',
          'edit section library templates',
          'view section library templates',
        ],
        'Content editor' => [
          'delete section library templates',
          'edit section library templates',
          'view section library templates',
        ],
        'Content author' => [
          'delete section library templates',
          'edit section library templates',
          'view section library templates',
        ],
      ],
      'create' => [
        'Site manager' => [
          'add section library templates',
        ],
        'Content editor' => [
          'add section library templates',
        ],
        'Content author' => [
          'add section library templates',
        ],
      ],
      'use' => [
        'Site manager' => [
          'import template from section library',
        ],
        'Content editor' => [
          'import template from section library',
        ],
        'Content author' => [
          'import template from section library',
        ],
      ],
      default => [],
    };
  }

}
