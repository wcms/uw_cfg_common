<?php

namespace Drupal\uw_cfg_common\Service;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\KeyValueStore\KeyValueExpirableFactory;
use Drupal\layout_builder\LayoutTempstoreRepositoryInterface;

/**
 * Service to fix missing blocks in saved and unsaved nodes.
 */
class UWMissingBlocks {

  /**
   * Entity type manager from core.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Key value expirable service from core.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueExpirableFactory
   */
  protected $keyValue;

  /**
   * Layout builder temporary repository service from core.
   *
   * @var \Drupal\layout_builder\LayoutTempstoreRepositoryInterface
   */
  protected $temporaryRepository;

  /**
   * Default class constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager service from core.
   * @param \Drupal\Core\KeyValueStore\KeyValueExpirableFactory $keyValue
   *   Key value expirable service from core.
   * @param \Drupal\layout_builder\LayoutTempstoreRepositoryInterface $temporaryRepository
   *   Layout builder temporary repository service from core.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, KeyValueExpirableFactory $keyValue, LayoutTempstoreRepositoryInterface $temporaryRepository) {
    $this->entityTypeManager = $entityTypeManager;
    $this->keyValue = $keyValue;
    $this->temporaryRepository = $temporaryRepository;
  }

  /**
   * Checks nodes for missing blocks in unsaved changes.
   *
   * It will load only nodes that have unsaved changes, based on result from
   * temporary repository table (key_value_expire) with name
   * (tempstore.shared.layout_builder.section_storage.overrides).
   *
   * @param array|null $nodes
   *   List of nodes to check, defaults to every node.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function removeMissingBlocksFromUnsaved(array $nodes = NULL): void {
    $temp_nodes = $this->keyValue->get('tempstore.shared.layout_builder.section_storage.overrides')->getAll();

    foreach ($temp_nodes as $override_id => $storage) {
      $ids = explode('.', $override_id);

      if ((!empty($nodes) && in_array($ids[1], $nodes)) || empty($nodes)) {
        $layout = $storage->data['section_storage'] ?? NULL;

        if ($this->removeMissingBlocksProcessLayout($layout)) {
          $storage->data['section_storage'] = $layout;
          $this->keyValue->get('tempstore.shared.layout_builder.section_storage.overrides')->set($override_id, $storage);
        }
      }
    }
  }

  /**
   * Checks saved nodes for missing blocks (includes revisions).
   *
   * @param array|null $only_nodes
   *   List of nodes to check. If omitted it will load all blocks and perform
   *   checks on them. This may be timely process.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function removeMissingBlocksFromSaved(array $only_nodes = NULL): void {
    // Loading either all nodes if only_nodes is empty, or just nodes that
    // are passed as arguments. Non-existing node ids will not be loaded.
    $nodes = $this->entityTypeManager->getStorage('node')->loadMultiple($only_nodes);

    foreach ($nodes as $node) {
      $layout = $node->get('layout_builder__layout');

      if ($this->removeMissingBlocksProcessLayout($layout)) {
        $node->save();
      }

      // Revisions.
      $vids = $this->entityTypeManager->getStorage('node')->revisionIds($node);

      foreach ($vids as $vid) {
        $revision_node = $this->entityTypeManager->getStorage('node')->loadRevision($vid);
        $layout = $revision_node->get('layout_builder__layout');

        if ($this->removeMissingBlocksProcessLayout($layout)) {
          $revision_node->save();
        }
      }
    }
  }

  /**
   * Checks layout for missing blocks.
   *
   * @param \Drupal\layout_builder\Field\LayoutSectionItemList|\Drupal\layout_builder\Plugin\SectionStorage\OverridesSectionStorage $layout
   *   Layout to check.
   *
   * @return bool
   *   Were there any changes that would require save on entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function removeMissingBlocksProcessLayout($layout): bool {
    $save_required = FALSE;

    $sections = $layout->getSections();

    foreach ($sections as $section) {
      $components = $section->getComponents();

      foreach ($components as $component) {
        $config = $component->get('configuration');

        if (!empty($config['block_revision_id'])) {
          $block_revision = $this->entityTypeManager->getStorage('block_content')
            ->loadRevision($config['block_revision_id']);

          if (!$block_revision) {
            $section->removeComponent($component->getUuid());
            $save_required = TRUE;
          }
        }
      }
    }

    return $save_required;
  }

}
