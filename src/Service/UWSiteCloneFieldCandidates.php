<?php

namespace Drupal\uw_cfg_common\Service;

use Drupal\Core\Entity\EntityFieldManager;

/**
 * Class UWSiteCloneFieldCandidates.
 *
 * This class is responsible for retrieving the field candidates for
 * the UWSiteClone module.
 */
class UWSiteCloneFieldCandidates implements UWSiteCloneFieldCandidatesInterface {

  /**
   * Default constructor using constructor property promotion.
   *
   * @param \Drupal\Core\Entity\EntityFieldManager $entityFieldManager
   *   Core service from module's services.yml file.
   */
  public function __construct(protected EntityFieldManager $entityFieldManager) {}

  /**
   * {@inheritDoc}
   */
  public function getFieldCandidates(): array {
    $candidates = [];

    // Check following field types.
    $types = ['text_long', 'text_with_summary', 'link'];

    foreach ($types as $type) {
      $content_fields = $this->entityFieldManager->getFieldMapByFieldType($type);

      foreach ($content_fields as $ct_name => $fields) {
        // We are interested only in fields in following types.
        if (!in_array($ct_name, ['node', 'paragraph', 'block_content'])) {
          continue;
        }

        foreach ($fields as $name => $field) {
          // Now check if field is custom with prefix 'field_uw'.
          if (str_starts_with($name, 'field_uw')) {
            $candidates[$ct_name][$type][] = str_replace('field_', '', $name);
          }
        }
      }
    }

    return $candidates;
  }

}
