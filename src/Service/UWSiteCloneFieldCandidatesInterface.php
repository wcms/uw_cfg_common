<?php

namespace Drupal\uw_cfg_common\Service;

/**
 * Represents an interface for retrieving field candidates from a reporter.
 */
interface UWSiteCloneFieldCandidatesInterface {

  /**
   * Return a list of fields.
   *
   * @return array
   *   A list of fields.
   */
  public function getFieldCandidates(): array;

}
