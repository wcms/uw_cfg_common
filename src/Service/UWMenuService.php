<?php

declare(strict_types=1);

namespace Drupal\uw_cfg_common\Service;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Http\RequestStack;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Menu\MenuLinkTreeInterface;
use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class UWMenuService.
 *
 * Functions to handle uw menus.
 *
 * @package Drupal\uw_cfg_common\Service
 */
class UWMenuService {

  use StringTranslationTrait;

  /**
   * MenuLinkTree definition.
   *
   * @var \Drupal\Core\Menu\MenuLinkTreeInterface
   */
  protected MenuLinkTreeInterface $menuLinkTree;

  /**
   * Active menu tree definition.
   *
   * @var \Drupal\Core\Menu\MenuLinkTreeInterface[]
   */
  protected array $activeMenuTree;

  /**
   * The request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected Request $request;

  /**
   * The UW service.
   *
   * @var \Drupal\uw_cfg_common\Service\UWService
   */
  protected $uwService;

  /**
   * Language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * Default language for the site.
   *
   * @var string
   */
  protected string $defaultLanguage;

  /**
   * MenuItems constructor.
   *
   * @param \Drupal\Core\Menu\MenuLinkTreeInterface $menu_link_tree
   *   The MenuLinkTree service.
   * @param \Drupal\Core\Http\RequestStack $request
   *   The request stack.
   * @param \Drupal\uw_cfg_common\Service\UWService $uwService
   *   The uw service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   Language manager from the core.
   */
  public function __construct(
    MenuLinkTreeInterface $menu_link_tree,
    RequestStack $request,
    UWService $uwService,
    LanguageManagerInterface $languageManager,
  ) {

    $this->menuLinkTree = $menu_link_tree;
    $this->request = $request->getCurrentRequest();
    $this->uwService = $uwService;
    $this->languageManager = $languageManager;
    $this->defaultLanguage = $this->languageManager->getDefaultLanguage()->getId();
  }

  /**
   * Map menu tree into an array.
   *
   * @param array $links
   *   The array of menu tree links.
   * @param string $submenuKey
   *   The key for the submenu to simplify.
   * @param bool $translated
   *   Flag if the menu is translatable.
   *
   * @return array
   *   The simplified menu tree array.
   */
  protected function simplifyMenuLinks(
    array $links,
    string $submenuKey = 'submenu',
    bool $translated = FALSE,
  ): array {

    // Have at least empty array to return.
    $result = [];

    foreach ($links as $item) {

      // Per DefaultMenuLinkTreeManipulators::checkAccess(), which we run in
      // getMenuTree, "inaccessible links are *not* removed; it's up to the code
      // doing something with the tree to exclude inaccessible links, just like
      // MenuLinkTree::build() does" - whose code we replicate here.
      /**
       * @var \Drupal\Core\Menu\MenuLinkInterface $link
       */
      $link = $item->link;

      // Generally we only deal with visible links, but just in case.
      if (!$link->isEnabled()) {
        continue;
      }

      if ($item->access !== NULL && !$item->access instanceof AccessResultInterface) {
        throw new \DomainException('MenuLinkTreeElement::access must be either NULL or an AccessResultInterface object.');
      }

      // Only render accessible links.
      if ($item->access instanceof AccessResultInterface && !$item->access->isAllowed()) {
        continue;
      }

      if ($translated) {
        $title = $link->getTitle();

        // Don't translate the node titles or the home page link.
        if ($link->getRouteName() !== '<front>' && $link->getRouteName() !== 'entity.node.canonical') {
          // phpcs:ignore Drupal.Semantics.FunctionT.NotLiteralString
          $title = $this->t($title, options: ['langcode' => $this->defaultLanguage]);
        }
      }
      else {
        $title = $link->getPluginDefinition()['title'];

        // isTranslatable may not be needed here, we don't care about it.
        if ($title instanceof TranslatableMarkup) {
          $title = $title->getUntranslatedString();
        }
      }

      // Build the link item.
      $simplifiedMenuLink = [
        'text' => $title,
        'url' => $item->link->getUrlObject()->toString(),
        'active_trail' => FALSE,
        'active' => FALSE,
      ];

      $current_path = $this->request->getRequestUri();
      if ($current_path === $simplifiedMenuLink['url']) {
        $simplifiedLink['active'] = TRUE;
      }

      /**
       * @var string $plugin_id
       */
      $plugin_id = $item->link->getPluginId();
      if (isset($this->activeMenuTree[$plugin_id]) && $this->activeMenuTree[$plugin_id]) {
        $simplifiedMenuLink['active_trail'] = TRUE;
      }
      if ($item->hasChildren) {
        $simplifiedMenuLink[$submenuKey] = $this->simplifyMenuLinks($item->subtree);
      }
      $result[] = $simplifiedMenuLink;
    }

    return $result;
  }

  /**
   * Get header menu links.
   *
   * @param string $menuId
   *   Menu drupal id.
   * @param bool $translated
   *   If the menu is translatable.
   * @param bool $remove_home_link
   *   Flag to remove the home link.
   *
   * @return array
   *   Render array of menu items.
   */
  public function getMenuTree(
    string $menuId = 'main',
    bool $translated = FALSE,
    bool $remove_home_link = FALSE,
  ): array {

    $this->setActiveMenuTree($menuId);

    $parameters = new MenuTreeParameters();
    $parameters->onlyEnabledLinks();
    $manipulators = [
      ['callable' => 'menu.default_tree_manipulators:checkAccess'],
      ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
    ];

    $headerTreeLoad = $this->menuLinkTree->load($menuId, $parameters);
    $headerTransform = $this->menuLinkTree->transform($headerTreeLoad, $manipulators);

    // Get the menus with or without translation.
    $menus = $this->simplifyMenuLinks($headerTransform, translated: $translated);

    // If the remove home link flag is set, remove it.
    if ($remove_home_link) {

      // Get the home link.
      $home_link = $this->uwService->getHomeMenuLinkName();

      // Step through the menu and do a few things:
      // (a) Remove the Home link (this will be a house icon)
      // (b) Add the number of menu items per menu link.
      foreach ($menus as $index => $menu) {

        // If this is the Home link, then remove it.
        if ($menu['text'] == $home_link) {

          // Remove the home link.
          unset($menus[$index]);
        }
      }
    }

    return $menus;
  }

  /**
   * Loads up the current active menu tree and sets it to a variable.
   *
   * @param string $menu_id
   *   The id of the menu to check for active links.
   */
  public function setActiveMenuTree(string $menu_id): void {
    $parameters = $this->menuLinkTree->getCurrentRouteMenuTreeParameters($menu_id);
    $loaded_tree = $this->menuLinkTree->load($menu_id, $parameters);
    $this->activeMenuTree = $this->checkActiveTrail($loaded_tree);
  }

  /**
   * Loops through a menu tree array to flag menu items in the active trail.
   *
   * @param array $menuTree
   *   An array returned from loading a menu tree.
   *
   * @return array
   *   The menu items keyed by their plugin IDs.
   *   Set to TRUE if in the active trail.
   */
  protected function checkActiveTrail(array $menuTree): array {

    $active = [];

    foreach ($menuTree as $index => $tree) {
      if ($tree->inActiveTrail) {
        $active[$index] = TRUE;
      }
      else {
        $active[$index] = FALSE;
      }
      if ($tree->hasChildren) {
        $active += $this->checkActiveTrail($tree->subtree);
      }
    }
    return $active;
  }

}
