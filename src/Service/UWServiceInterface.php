<?php

namespace Drupal\uw_cfg_common\Service;

use Drupal\Core\Entity\EntityInterface;
use Drupal\node\Entity\Node;

/**
 * Interface UWServiceInterface.
 *
 * Interface that is collection of common functions used in custom blocks.
 *
 * @package Drupal\uw_cfg_common\Service
 */
interface UWServiceInterface {

  /**
   * Get the image styles used in UW crops.
   *
   * @param string $type
   *   The type of styles to get.
   * @param bool $get_all
   *   Flag to get all the image styles.
   *
   * @return string[]
   *   Array of image styles that are used.
   */
  public function getCropImageStyles(string $type, bool $get_all = FALSE): array;

  /**
   * Prepares responsive image.
   *
   * @param \Drupal\Core\Entity\EntityInterface|null $entity
   *   Image entity or null.
   * @param string $image_style
   *   Image style to be used for responsive image.
   * @param bool $absolute_url
   *   Flag to make the urls absolute.
   *
   * @return array
   *   Array with details for responsive image.
   */
  public function prepareResponsiveImage(?EntityInterface $entity, string $image_style, bool $absolute_url = FALSE): array;

  /**
   * Get the UW images styles used in UW responsive image.
   *
   * @return array
   *   Array of image styles.
   */
  public function uwGetResponsiveImageStyles(): array;

  /**
   * Gets content types that have feature images.
   *
   * @param string $type
   *   The type of preprocess (node, teaser, featured_image, etc).
   *
   * @return array
   *   Array of content types that has featured images.
   */
  public function uwGetNodePreprocessing(string $type): array;

  /**
   * Gets the flags for showing media on nodes.
   *
   * @param \Drupal\node\Node $node
   *   Node entity.
   * @param string $view_mode
   *   The view mode of the node.
   *
   * @return array
   *   Array of flags for media.
   */
  public function uwGetMediaFlags(Node $node, string $view_mode = NULL): array;

  /**
   * Gets dates from node.
   *
   * @param \Drupal\node\Node $node
   *   Node entity.
   *
   * @return string
   *   Yes or no.
   */
  public function uwCheckNodeForMedia(Node $node): string;

  /**
   * A function to get or check the attached sidebar.
   *
   * If the type is set to check, then the function will return one
   * of two things. It will either return a 0, which means that the
   * attached_page_nid is not attached to any other sidebar in the
   * system.  Or it will return the entity_id of the sidebar (sidebar_nid)
   * that it is currently attached to.
   *
   * For example:
   *   If the attached_page_nid is 3 and the sidebar_nid is 7, and
   *   the query finds that 3 is already attached to sidebar_nid 6, then 6
   *   will be returned.
   *
   *   If the attached_page_nid is 3 and the sidebar_nid is 7, and
   *   the query finds no other sidebar_nids that 3 is attached to, then 0
   *   will be returned.
   *
   * @param int $attached_page_nid
   *   An integer value that represents the nid of the page to be attached to.
   * @param int|null $sidebar_nid
   *   An integer value that represents the nid of the current sidebar.
   * @param string|null $type
   *   A string value that represents either "get" or "check".
   *
   * @return int
   *   A value that is either 0 (no nids) or the entity_id of a sidebar.
   */
  public function getOrCheckAttachedSidebar(int $attached_page_nid, int $sidebar_nid = NULL, string $type = NULL): int;

  /**
   * A function to get an array of UW content types, with or without sidebar.
   *
   * @param bool $with_sidebar
   *   A boolean to state if we want content types that can have sidebars.
   *
   * @return array
   *   An array of the machine names of the UW content types.
   */
  public function getUwContentTypes(bool $with_sidebar = FALSE): array;

  /**
   * A function to get an array of menu.
   *
   * @param string $menu_name
   *   A string to the machine name of the menu to get.
   * @param bool $count_menu_items
   *   A boolean on whether to count the number of menu items.
   * @param bool $include_parent_in_count
   *   A boolean on whether to include the parent in the menu items count.
   *
   * @return array
   *   An array of the menu.
   */
  public function uwGetMenu(string $menu_name = 'main', bool $count_menu_items = FALSE, bool $include_parent_in_count = FALSE): array;

  /**
   * Function to check that all menu links are published.
   *
   * @param array $menu
   *   The array of menus.
   */
  public function uwCheckMenuItems(array &$menu): void;

  /**
   * A function to setup the menu for UW display.
   *
   * This function will count of the number of menu items,
   * and use a recursive function (uwCountMenuItems) to count
   * the total number of menu items.
   *
   * The variable include_parent_in_count tells us if we need
   * to add one to the total count of menu items.  This is required
   * for displaying some menus as the parent is also included
   * in the displaying of the menu (i.e. the tray of the main and
   * information for menus).
   *
   * For example, the menu structure will look like:
   * Parent
   *   Child #1
   *     Grandchild #1-1
   *     Grandchild #1-2
   *   Child #2
   *   Child #3
   *     Grandchild #3-1
   *       Great grandchild #3-1-1
   *       Great grandchild #3-1-2
   *     Grandchild #3-2
   *     Grandchild #3-3
   *       Great grandchild #3-3-1
   * So it should with the recursive function it will return 11.
   * If we include the parent the count will be 12.
   *
   * @param array $menus
   *   A reference to the array list of menu items.
   * @param bool $include_parent_in_count
   *   A boolean on whether to include the parent in the menu items count.
   *
   * @return array
   *   An array of the updated menu items.
   */
  public function uwSetMenuItems(array $menus, bool $include_parent_in_count = FALSE): array;

  /**
   * A function to recursively count the number of menu items in the submenu.
   *
   * @param array $menu
   *   An array that contains the submenu.
   * @param int &$menu_items_count
   *   A reference to the integer that is storing the number of menu items.
   */
  public function uwCountMenuItems(array $menu, int &$menu_items_count): void;

  /**
   * A function to return the month short name.
   *
   * @param int $month
   *   An integer of the month.
   *
   * @return mixed
   *   A mixed variable that will either be a string of the short
   *   month with a period or array of all short months.
   */
  public function uwMonthNameShort(int $month = NULL);

  /**
   * Function to get the content type usage.
   *
   * @param array $content_types
   *   Array of content types.
   * @param bool $show_null_content_types
   *   Flag to show null content type usage.
   *
   * @return array
   *   Array of content type usage.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getContentTypeUsage(array $content_types, bool $show_null_content_types): array;

  /**
   * Function to check if an external url is valid.
   *
   * @param string $url
   *   The URL to be checked.
   *
   * @return bool
   *   Whether url is valid or not.
   */
  public function validExternalUrl(string $url): bool;

  /**
   * Function to do the ksort recursively with options.
   *
   * @param array $array
   *   Array to sort.
   * @param string $sort_flags
   *   The options for the sort.
   *
   * @return bool
   *   Whether the sort was successful.
   */
  public function ksortRecursive(array &$array, string $sort_flags = SORT_REGULAR): bool;

  /**
   * Function to do the reverse ksort recursively with options.
   *
   * @param array $array
   *   Array to sort.
   * @param string $sort_flags
   *   The options for the sort.
   *
   * @return bool
   *   Whether the sort was successful.
   */
  public function krsortRecursive(array &$array, string $sort_flags = SORT_REGULAR): bool;

}
