<?php

namespace Drupal\uw_cfg_common\Service;

use Drupal\Core\Routing\AdminContext;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Class UWAnalytics.
 *
 * Service to check if analytics scripts should be included on the page.
 *
 * @package Drupal\uw_cfg_common\Service
 */
class UWAnalytics {

  /**
   * Admin context service.
   *
   * @var \Drupal\Core\Routing\AdminContext
   */
  protected $adminContext;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * UWAnalytics default constructor.
   *
   * @param \Drupal\Core\Routing\AdminContext $adminContext
   *   Admin context, route matcher included.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   Route match service.
   */
  public function __construct(AdminContext $adminContext, RouteMatchInterface $routeMatch) {
    $this->adminContext = $adminContext;
    $this->routeMatch = $routeMatch;
  }

  /**
   * Is current page admin page, with additional checks.
   *
   * @return bool
   *   TRUE if admin page, FALSE otherwise.
   */
  public function administrationPage(): bool {
    // Check if requested route is admin route. This should cover /node/1/edit,
    // node/1/revisions, term/1/edit, webform results page.
    $result = $this->adminContext->isAdminRoute();

    // When current route is not detected as admin path, check if route is
    // 403 or 404. Skip injecting GA code snippets on those pages.
    if (!$result) {
      $route_name = $this->routeMatch->getRouteName();

      // Check for default 403/404 pages. This will not cover any custom 403/404
      // pages that may be webpages (e.g. /node/3).
      if (in_array($route_name, ['system.403', 'system.404'])) {
        $result = TRUE;
      }
      // Adding exception for layout builder.
      elseif (substr($route_name, 0, 15) == 'layout_builder.') {
        $result = TRUE;
      }
    }

    return $result;
  }

}
