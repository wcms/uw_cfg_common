<?php

namespace Drupal\uw_cfg_common\Service;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\node\Entity\Node;

/**
 * Class UwNodeContent.
 *
 * Gets the content out of a node.
 *
 * @package Drupal\uw_cfg_common\Service
 */
class UwNodeContent {

  /**
   * The UW node data service.
   *
   * @var UwNodeData
   */
  protected $uwNodeData;

  /**
   * The variable for route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Default constructor.
   *
   * @param UwNodeData $uwNodeData
   *   The UW node data service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The route match.
   */
  public function __construct(
    UwNodeData $uwNodeData,
    RouteMatchInterface $routeMatch
  ) {
    $this->uwNodeData = $uwNodeData;
    $this->routeMatch = $routeMatch;
  }

  /**
   * Gets the content of a node.
   *
   * @param \Drupal\node\Entity\Node $node
   *   The node.
   * @param string $view_mode
   *   The view mode.
   * @param string $content
   *   The content to get (either layout builder or summary).
   *
   * @return array
   *   Array of content to get from the node.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function getNodeContent(Node $node, string $view_mode, string $content): array {

    // Get the flags for the node.
    $node_flags = $this->getNodeFlags($node, $view_mode, $content);

    // Setup the node data array, based on flags.
    switch ($node->getType()) {

      case 'uw_ct_blog':
        $content_data = $this->getBlogContent($node_flags, $view_mode);
        break;

      case 'uw_ct_event':
        $content_data = $this->getEventContent($node_flags, $view_mode);
        break;

      case 'uw_ct_expand_collapse_group':
        $content_data = $this->getExpandCollapseGroupContent($node_flags);
        break;

      case 'uw_ct_news_item':
        $content_data = $this->getNewsContent($node_flags, $view_mode);
        break;

      case 'uw_ct_web_page':
        $content_data = $this->getWebPageContent($node_flags);
        break;

      case 'uw_ct_catalog_item':
        $content_data = $this->getCatalogItemContent($node_flags);
        break;

      case 'uw_ct_contact':
        $content_data = $this->getContactContent($node_flags, $view_mode);
        break;

      case 'uw_ct_profile':
        $content_data = $this->getProfileContent($node_flags, $view_mode);
        break;

      case 'uw_ct_project':
        $content_data = $this->getProjectContent($node_flags, $view_mode);
        break;

      case 'uw_ct_service':
        $content_data = $this->getServiceContent($node_flags);
        break;

      case 'uw_ct_opportunity':
        $content_data = $this->getOpportunityContent($node_flags, $view_mode);
        break;
    }

    return $this->uwNodeData->getNodeData($node, $view_mode, $content_data);
  }

  /**
   * Get the content types that have hero images.
   *
   * @return string[]
   *   Array of content types that can have hero images.
   */
  public function getHeroImageContentTypes(): array {
    return [
      'uw_ct_blog' => 'field_uw_hero_image',
      'uw_ct_event' => 'field_uw_hero_image',
      'uw_ct_news_item' => 'field_uw_hero_image',
    ];
  }

  /**
   * Get the flags for the node.
   *
   * @param \Drupal\node\Entity\Node $node
   *   The node.
   * @param string $view_mode
   *   The view mode.
   * @param string $content
   *   The content to get (layout builder or summary).
   *
   * @return array
   *   Array of flags for the node.
   */
  public function getNodeFlags(Node $node, string $view_mode, string $content): array {

    // Flags for getting teaser content.
    $node_flags['get_header'] = FALSE;
    $node_flags['get_footer'] = FALSE;
    $node_flags['get_image'] = FALSE;
    $node_flags['get_content'] = FALSE;
    $node_flags['get_title'] = FALSE;
    $node_flags['get_media'] = FALSE;
    $node_flags['get_listing_image'] = FALSE;
    $node_flags['get_tags'] = TRUE;

    // Setup flags based on teaser content argument.
    if ($content == 'all') {
      $node_flags['get_header'] = TRUE;
      $node_flags['get_footer'] = TRUE;
      $node_flags['get_content'] = TRUE;

      if ($view_mode == 'full') {
        $node_flags['get_title'] = TRUE;
        $node_flags['get_media'] = TRUE;

        if ($node->getType() == 'uw_ct_contact') {
          $node_flags['get_image'] = TRUE;
        }
      }
      elseif ($view_mode == 'teaser') {
        if ($node->getType() !== 'uw_ct_contact') {
          $node_flags['get_footer'] = FALSE;
        }
        $node_flags['get_image'] = TRUE;
        $node_flags['get_title'] = TRUE;
      }
    }
    else {
      if ($content == 'header') {
        if ($node->getType() == 'uw_ct_contact') {
          $node_flags['get_image'] = TRUE;
        }

        $node_flags['get_header'] = TRUE;
        $node_flags['get_title'] = TRUE;
        $node_flags['get_media'] = TRUE;
        $node_flags['get_tags'] = FALSE;
      }

      if ($content == 'footer') {
        $node_flags['get_footer'] = TRUE;
        $node_flags['get_title'] = FALSE;
      }
    }

    return $node_flags;
  }

  /**
   * Common elements for content data.
   *
   * @param array $node_flags
   *   The flags for the node.
   *
   * @return array
   *   Array of common elements for content data.
   */
  public function setupContentData(array $node_flags): array {

    $content_data['url'] = [
      'type' => 'url',
    ];

    $content_data['header'] = [
      'has_children' => TRUE,
    ];

    if ($node_flags['get_title']) {
      $content_data['header']['title'] = [
        'type' => 'title',
      ];
    }

    return $content_data;
  }

  /**
   * Functiont to add to content data array.
   *
   * @param string $type
   *   The type of field.
   * @param mixed $field_name
   *   The actual field name(s).
   * @param string|null $label
   *   The label to be used with the field.
   *
   * @return string[]
   *   Array to add to the content data.
   */
  public function addToContentData(string $type, $field_name, string $label = NULL): array {
    return [
      'type' => $type,
      'field' => $field_name,
      'label' => $label,
    ];
  }

  /**
   * Get the node content for blog content type.
   *
   * @param array $node_flags
   *   The flags for the node.
   * @param string $view_mode
   *   The view mode of the node.
   *
   * @return array
   *   Array of content to get from the node.
   */
  public function getBlogContent(array $node_flags, string $view_mode): array {

    // Get the content data.
    $content_data = $this->setupContentData($node_flags);

    // Setup the header content.
    if ($node_flags['get_header']) {
      if ($view_mode == 'teaser') {
        $content_data['header']['date'] = $this->addToContentData('date', 'field_uw_blog_date');
      }
      else {
        $content_data['header']['date'] = $this->addToContentData('date', 'field_uw_blog_date');
        $content_data['header']['author'] = $this->addToContentData('author', 'field_author');
      }
    }
    // Get the media.
    if ($node_flags['get_media']) {
      $content_data['media'] = $this->addToContentData('media', NULL);
    }

    // Get the listing image.
    if ($node_flags['get_image']) {
      $content_data['image'] = $this->addToContentData('image', 'field_uw_blog_listing_page_image');
      $content_data['image']['extra_options'] = [
        'type' => 'listing_image',
        'is_responsive' => TRUE,
        'view_mode' => $view_mode,
      ];
    }

    // Setup the actual content.
    if ($node_flags['get_content']) {
      $content_data['content'] = $this->addToContentData('content', 'field_uw_blog_summary');
    }

    // Get the tags, if any.
    if ($node_flags['get_tags']) {
      $content_data['tags'] = $this->addToContentData(
        'terms',
        [
          'field_uw_blog_tags',
          'field_uw_audience',
        ]
      );
    }

    return $content_data;
  }

  /**
   * Get the node content for event content type.
   *
   * @param array $node_flags
   *   The flags for the node.
   * @param string $view_mode
   *   The view mode.
   *
   * @return array
   *   Array of content to get from the node.
   */
  public function getEventContent(array $node_flags, string $view_mode): array {

    // Setup the content data array.
    $content_data = $this->setupContentData($node_flags);

    // Setup the header content.
    if ($node_flags['get_header']) {
      $content_data['header']['date'] = $this->addToContentData('date', 'field_uw_event_date');
    }

    // Get the media.
    if ($node_flags['get_media']) {
      $content_data['media'] = $this->addToContentData('media', NULL);
    }

    // Get listing image.
    if ($node_flags['get_image']) {
      $content_data['image'] = $this->addToContentData('image', 'field_uw_event_listing_page_img');
      $content_data['image']['extra_options'] = [
        'type' => 'listing_image',
        'is_responsive' => TRUE,
        'view_mode' => $view_mode,
      ];
    }

    // Setup the actual content.
    if ($node_flags['get_content']) {
      $content_data['content'] = $this->addToContentData('content', 'field_uw_event_summary');
    }

    // Setup the footer content.
    if ($node_flags['get_footer']) {

      $content_data['footer']['additional_info'] = [
        'has_children' => TRUE,
        'host' => $this->addToContentData('link', 'field_uw_event_host', 'Host'),
        'event_website' => $this->addToContentData('link', 'field_uw_event_website', 'Event website'),
        'cost' => $this->addToContentData('plain_text', 'field_uw_event_cost', 'Cost'),
      ];

      $content_data['footer']['location_info'] = [
        'has_children' => TRUE,
        'address' => $this->addToContentData('address', 'field_uw_event_location_address', 'Location address'),
        'map' => $this->addToContentData('map', 'field_uw_event_location_coord', 'Location coordinates'),
        'map_link' => $this->addToContentData('link', 'field_uw_event_map', 'Map link'),
      ];
    }

    // Get the tags, if any.
    if ($node_flags['get_tags']) {
      $content_data['tags'] = $this->addToContentData(
        'terms',
        [
          'field_uw_event_tags',
          'field_uw_audience',
          'field_uw_event_type',
        ]
      );
    }

    return $content_data;
  }

  /**
   * Get the node content for e/c group content type.
   *
   * @param array $node_flags
   *   The flags for the node.
   *
   * @return array
   *   Array of content to get from the node.
   */
  public function getExpandCollapseGroupContent(array $node_flags): array {

    // Setup the content data array.
    $content_data = $this->setupContentData($node_flags);

    // Setup the actual content.
    if ($node_flags['get_content']) {
      $content_data['content'] = $this->addToContentData('content', '');
    }

    return $content_data;
  }

  /**
   * Get the node content for news content type.
   *
   * @param array $node_flags
   *   The flags for the node.
   * @param string $view_mode
   *   The view mode of the node.
   *
   * @return array
   *   Array of content to get from the node.
   */
  public function getNewsContent(array $node_flags, string $view_mode): array {

    // Get the content data.
    $content_data = $this->setupContentData($node_flags);

    // Setup the header content.
    if ($node_flags['get_header']) {
      $content_data['header']['date'] = $this->addToContentData('date', 'field_uw_news_date');
    }

    // Get the media.
    if ($node_flags['get_media']) {
      $content_data['media'] = $this->addToContentData('media', NULL);
    }

    // Get listing image.
    if ($node_flags['get_image']) {
      $content_data['image'] = $this->addToContentData('image', 'field_uw_news_listing_page_image');
      $content_data['image']['extra_options'] = [
        'type' => 'listing_image',
        'is_responsive' => TRUE,
        'view_mode' => $view_mode,
      ];
    }

    // Setup the actual content.
    if ($node_flags['get_content']) {
      $content_data['content'] = $this->addToContentData('content', 'field_uw_news_summary');
    }

    // Get the tags, if any.
    if ($node_flags['get_tags']) {
      $content_data['tags'] = $this->addToContentData(
        'terms',
        [
          'field_uw_news_tags',
          'field_uw_audience',
        ]
      );
    }

    return $content_data;
  }

  /**
   * Get the node content for web page content type.
   *
   * @param array $node_flags
   *   The flags for the node.
   *
   * @return array
   *   Array of content to get from the node.
   */
  public function getWebPageContent(array $node_flags): array {

    // Get the content data.
    $content_data = $this->setupContentData($node_flags);

    // Setup the actual content.
    if ($node_flags['get_content']) {
      $content_data['content'] = $this->addToContentData('content', 'layout_builder__layout');
    }

    if ($node_flags['get_media']) {
      $content_data['media'] = $this->addToContentData('media', NULL);
    }

    return $content_data;
  }

  /**
   * Get the node content for catalog item content type.
   *
   * @param array $node_flags
   *   The flags for the node.
   *
   * @return array
   *   Array of content to get from the node.
   */
  public function getCatalogItemContent(array $node_flags): array {

    // Get the content data.
    $content_data = $this->setupContentData($node_flags);

    // Setup the actual content.
    if ($node_flags['get_content']) {
      $content_data['content'] = $this->addToContentData('content', 'field_uw_catalog_summary');
    }

    if ($node_flags['get_media']) {
      $content_data['media'] = $this->addToContentData('media', NULL);
    }

    // Get the footer for catalog items.
    if ($node_flags['get_footer']) {

      // Get the catalog terms.
      $content_data['footer']['additional_info']['has_children'] = TRUE;
      $content_data['footer']['additional_info']['tags'] = $this->addToContentData(
        'catalog_terms',
        [
          'Category' => 'field_uw_catalog_category',
          'Faculty' => 'field_uw_catalog_faculty',
          'Audience' => 'field_uw_audience',
        ]
      );
    }

    return $content_data;
  }

  /**
   * Get the node content for catalog item content type.
   *
   * @param array $node_flags
   *   The flags for the node.
   * @param string $view_mode
   *   The view mode of the node.
   *
   * @return array
   *   Array of content to get from the node.
   */
  public function getContactContent(array $node_flags, string $view_mode): array {

    // Get the content data.
    $content_data = $this->setupContentData($node_flags);

    // Setup the header content.
    if ($node_flags['get_header']) {
      $content_data['header']['position'] = $this->addToContentData('plain_text', 'field_uw_ct_contact_title');
    }

    // If there is an image, we have to decide on which to get based
    // on the view mode.
    if ($node_flags['get_image']) {

      // Get the node from the parameter.
      $node = $this->routeMatch->getParameter('node');

      // If we are on a layout page, use the portrait image.
      if (
        $node &&
        $node->getType() == 'uw_ct_contact' &&
        $this->routeMatch->getRouteName() == 'layout_builder.overrides.node.view'
      ) {
        $use_portrait = TRUE;
      }
      // If it is a teaser use the listing image, if not use the portrait image.
      elseif ($view_mode == 'teaser') {
        $use_portrait = FALSE;
      }
      else {
        $use_portrait = TRUE;
      }

      // If we are to use the portrait image, set the variables and
      // extra options.  If not, use the listing image.
      if ($use_portrait) {

        // Set the portrait image.
        $content_data['image'] = $this->addToContentData('image', 'field_uw_ct_contact_image');

        // Add the extra options for portrait image style.
        $content_data['image']['extra_options'] = [
          'type' => 'portrait',
          'crop' => 'portrait',
          'is_responsive' => TRUE,
        ];
      }
      else {

        // Use the listing image.
        $content_data['image'] = $this->addToContentData('image', 'field_uw_contact_listing_image');

        $content_data['image']['extra_options'] = [
          'type' => 'listing_image',
          'is_responsive' => TRUE,
          'view_mode' => $view_mode,
        ];
      }
    }

    // Setup the actual content.
    if ($node_flags['get_content']) {
      $content_data['content'] = $this->addToContentData('content', NULL);
    }

    if ($node_flags['get_media']) {
      $content_data['media'] = $this->addToContentData('media', NULL);
    }

    // Get the footer data.
    if ($node_flags['get_footer']) {

      // Get the additional info.
      $content_data['footer']['additional_info']['has_children'] = TRUE;
      $content_data['footer']['additional_info']['info'] = $this->addToContentData('formatted_text', 'field_uw_ct_contact_info');

      // Get the contact information.
      $content_data['footer']['contact_info']['has_children'] = TRUE;
      $content_data['footer']['contact_info']['email'] = $this->addToContentData('plain_text', 'field_uw_ct_contact_email');
      $content_data['footer']['contact_info']['emails'] = $this->addToContentData('contact_emails', 'field_uw_ct_contact_emails');
      $content_data['footer']['contact_info']['phone'] = $this->addToContentData('plain_text', 'field_uw_ct_contact_phone');
      $content_data['footer']['contact_info']['location'] = $this->addToContentData('plain_text', 'field_uw_ct_contact_location', 'Location');
      $content_data['footer']['contact_info']['contact'] = $this->addToContentData('plain_text', 'field_uw_ct_contact_contact_for');
      $content_data['footer']['contact_info']['info'] = $this->addToContentData('formatted_text', 'field_uw_ct_contact_info');

      // Get the links for the profile.
      $content_data['footer']['links']['has_children'] = TRUE;
      $content_data['footer']['links']['profile'] = $this->addToContentData('link', 'field_uw_ct_contact_link_profile', 'Link to profile');
      $content_data['footer']['links']['webpage'] = $this->addToContentData('link', 'field_uw_ct_contact_link_persona', 'Link to personal webpage');

      // Get the groups for the profile.
      $content_data['footer']['groups']['has_children'] = TRUE;
      $content_data['footer']['groups']['groups'] = $this->addToContentData('terms', ['field_uw_ct_contact_group']);
    }

    return $content_data;
  }

  /**
   * Get the node content for profile content type.
   *
   * @param array $node_flags
   *   The flags for the node.
   * @param string $view_mode
   *   The view mode.
   *
   * @return array
   *   Array of content to get from the node.
   */
  public function getProfileContent(array $node_flags, string $view_mode): array {

    // Get the content data.
    $content_data = $this->setupContentData($node_flags);

    // Setup the header content.
    if ($node_flags['get_header']) {
      $content_data['header']['position'] = $this->addToContentData('plain_text', 'field_uw_ct_profile_title');
    }

    // Get listing image.
    if ($node_flags['get_image']) {
      $content_data['image'] = $this->addToContentData('image', 'field_uw_ct_profile_image');
      $content_data['image']['extra_options'] = [
        'type' => 'listing_image',
        'is_responsive' => TRUE,
        'view_mode' => $view_mode,
      ];
    }

    // Setup the actual content.
    if ($node_flags['get_content']) {
      $content_data['content'] = $this->addToContentData('content', 'field_uw_profile_summary');
    }

    if ($node_flags['get_media']) {
      $content_data['media'] = $this->addToContentData('media', NULL);
    }

    // Get the footer for the profile.
    if ($node_flags['get_footer']) {
      $content_data['footer']['links']['has_children'] = TRUE;
      $content_data['footer']['links']['additional_info'] = $this->addToContentData('link', 'field_uw_ct_profile_info_link');
      $content_data['footer']['links']['webpage'] = $this->addToContentData('link', 'field_uw_ct_profile_link_persona');
      $content_data['footer']['links']['link_to_contact'] = $this->addToContentData('link', 'field_uw_ct_profile_link_contact');
    }

    // Get the tags, if any.
    if ($node_flags['get_tags']) {
      $content_data['tags'] = $this->addToContentData(
        'terms',
        [
          'field_uw_ct_profile_type',
        ]
      );
    }

    return $content_data;
  }

  /**
   * Get the node content for project content type.
   *
   * @param array $node_flags
   *   The flags for the node.
   * @param string $view_mode
   *   The view mode.
   *
   * @return array
   *   Array of content to get from the node.
   */
  public function getProjectContent(array $node_flags, string $view_mode): array {

    // Get the content data.
    $content_data = $this->setupContentData($node_flags);

    // Setup the actual content.
    if ($node_flags['get_content']) {
      $content_data['content'] = $this->addToContentData('content', 'field_uw_project_summary');
    }

    if ($node_flags['get_media']) {
      $content_data['media'] = $this->addToContentData('media', NULL);
    }

    // Get listing image.
    if ($node_flags['get_image']) {
      $content_data['image'] = $this->addToContentData('image', 'field_uw_project_listing_image');
      $content_data['image']['extra_options'] = [
        'type' => 'listing_image',
        'is_responsive' => TRUE,
        'view_mode' => $view_mode,
      ];
    }

    // Get the footer.
    if ($node_flags['get_footer']) {
      $content_data['footer']['project_details']['has_children'] = TRUE;
      $content_data['footer']['project_details']['status'] = $this->addToContentData('terms', ['field_uw_project_status']);
      $content_data['footer']['project_details']['topics'] = $this->addToContentData('terms', ['field_uw_project_topics']);
      $content_data['footer']['project_members'] = $this->addToContentData('project_members', 'field_uw_project_members');
      $content_data['footer']['timeline'] = $this->addToContentData('timeline', 'field_uw_project_timeline');
    }

    // Get the tags, if any.
    if ($node_flags['get_tags']) {
      $content_data['tags'] = $this->addToContentData(
        'terms',
        [
          'field_uw_audience',
        ]
      );
    }

    return $content_data;
  }

  /**
   * Get the node content for service content type.
   *
   * @param array $node_flags
   *   The flags for the node.
   *
   * @return array
   *   Array of content to get from the node.
   */
  public function getServiceContent(array $node_flags): array {

    // Get the content data.
    $content_data = $this->setupContentData($node_flags);

    if ($node_flags['get_header']) {
      $content_data['header']['status'] = $this->addToContentData('select', 'field_uw_service_status');
    }

    // Setup the actual content.
    if ($node_flags['get_content']) {
      $content_data['content'] = $this->addToContentData('content', 'field_uw_service_summary');
    }

    if ($node_flags['get_media']) {
      $content_data['media'] = $this->addToContentData('media', NULL);
    }

    // Get the footer for the profile.
    if ($node_flags['get_footer']) {
      $content_data['footer']['service_information']['has_children'] = TRUE;
      $content_data['footer']['service_information']['status'] = $this->addToContentData('select', 'field_uw_service_status');
      $content_data['footer']['service_information']['categories'] = $this->addToContentData('terms', ['field_uw_service_category']);

      $content_data['footer']['service_details']['has_children'] = TRUE;
      $content_data['footer']['service_details']['popularity'] = $this->addToContentData('plain_text', 'field_uw_service_popularity');
      $content_data['footer']['service_details']['use_service'] = $this->addToContentData(
        'terms',
        [
          'field_uw_service_audience',
        ]
      );
      $content_data['footer']['service_details']['whats_available'] = $this->addToContentData('multiple_plain_text', 'field_uw_service_available');
      $content_data['footer']['service_details']['request_service'] = $this->addToContentData('formatted_text', 'field_uw_service_request');
      $content_data['footer']['service_details']['minimum_notice'] = $this->addToContentData('plain_text', 'field_uw_service_notice');
      $content_data['footer']['service_details']['average_length'] = $this->addToContentData('plain_text', 'field_uw_service_length');
      $content_data['footer']['service_details']['pricing_cost'] = $this->addToContentData('formatted_text', 'field_uw_service_cost');
      $content_data['footer']['service_details']['support'] = $this->addToContentData('formatted_text', 'field_uw_service_support');

      $content_data['footer']['service_hours']['has_children'] = TRUE;
      $content_data['footer']['service_hours']['hours'] = $this->addToContentData('hours', 'field_uw_service_hours');
      $content_data['footer']['service_hours']['hours_notes'] = $this->addToContentData('plain_text', 'field_uw_service_hours_notes');

      $content_data['footer']['location_info'] = [
        'has_children' => TRUE,
        'address' => $this->addToContentData('address', 'field_uw_service_location', 'Location address'),
        'map' => $this->addToContentData('map', 'field_uw_service_location_coord', 'Location coordinates'),
      ];
    }

    return $content_data;
  }

  /**
   * Get the node content for opportunity content type.
   *
   * @param array $node_flags
   *   The flags for the node.
   * @param string $view_mode
   *   The view mode for this node.
   *
   * @return array
   *   Array of content to get from the node.
   */
  public function getOpportunityContent(array $node_flags, string $view_mode): array {

    // Get the content data.
    $content_data = $this->setupContentData($node_flags);

    // Setup the header content.
    if ($node_flags['get_header'] && $view_mode !== 'teaser') {

      $content_data['header']['opportunity_type'] = $this->addToContentData('terms', ['field_uw_opportunity_type']);
      $content_data['header']['employment_type'] = $this->addToContentData('terms', ['field_uw_opportunity_employment']);
      $content_data['header']['rate_of_pay'] = $this->addToContentData('plain_text', 'field_uw_opportunity_pay_rate');
      $content_data['header']['rate_of_pay_type'] = $this->addToContentData('terms', ['field_uw_opportunity_pay_type']);
      $content_data['header']['job_id'] = $this->addToContentData('plain_text', 'field_uw_opportunity_job_id');
    }

    // If we are on a teaser, send some fields to the footer,
    // so that the display after the content.
    if ($view_mode == 'teaser') {
      $content_data['footer']['posted'] = $this->addToContentData('date', 'field_uw_opportunity_date');
      $content_data['footer']['deadline'] = $this->addToContentData('date', 'field_uw_opportunity_deadline');
      $content_data['footer']['opportunity_type'] = $this->addToContentData('terms', ['field_uw_opportunity_type']);
    }

    if ($node_flags['get_footer']) {
      $content_data['footer']['links']['has_children'] = TRUE;
      $content_data['footer']['links']['application'] = $this->addToContentData('link', 'field_uw_opportunity_link');
      $content_data['footer']['links']['additional_info'] = $this->addToContentData('link', 'field_uw_opportunity_additional');
      $content_data['footer']['links']['contact'] = $this->addToContentData('link', 'field_uw_opportunity_contact');

      $content_data['footer']['opportunity_details']['has_children'] = TRUE;
      $content_data['footer']['opportunity_details']['posted_by'] = $this->addToContentData('plain_text', 'field_uw_opportunity_post_by');
      $content_data['footer']['opportunity_details']['number_of_positions'] = $this->addToContentData('select', 'field_uw_opportunity_pos_number');
      $content_data['footer']['opportunity_details']['reports_to'] = $this->addToContentData('plain_text', 'field_uw_opportunity_report');

      $content_data['footer']['opportunity_dates']['has_children'] = TRUE;
      $content_data['footer']['opportunity_dates']['posted'] = $this->addToContentData('date', 'field_uw_opportunity_date');
      $content_data['footer']['opportunity_dates']['deadline'] = $this->addToContentData('date', 'field_uw_opportunity_deadline');
      $content_data['footer']['opportunity_dates']['start_date'] = $this->addToContentData('date', 'field_uw_opportunity_start_date');
      $content_data['footer']['opportunity_dates']['end_date'] = $this->addToContentData('date', 'field_uw_opportunity_end_date');
    }

    // Setup the actual content.
    if ($node_flags['get_content']) {
      $content_data['content'] = $this->addToContentData('content', 'field_uw_opportunity_position');
    }

    if ($node_flags['get_media']) {
      $content_data['media'] = $this->addToContentData('media', NULL);
    }

    return $content_data;
  }

  /**
   * Gets the most recent date.
   *
   * @param array $date
   *   The date.
   * @param string $type
   *   The type of date.
   *
   * @return array
   *   The array of dates.
   */
  public function getDate(array $date, string $type): array {
    return $this->uwNodeData->getDate($date, $type);
  }

}
