<?php

namespace Drupal\uw_cfg_common\Service;

use Drupal\node\Entity\Node;

/**
 * Class UwNodeData.
 *
 * UW node data gets all the actual data from a specific node.
 *
 * @package Drupal\uw_cfg_common\Service
 */
class UwNodeData {

  /**
   * UW field value service.
   *
   * @var \Drupal\uw_cfg_common\Service\UwNodeFieldValue
   */
  protected $uwNodeFieldValue;

  /**
   * Default constructor.
   *
   * @param \Drupal\uw_cfg_common\Service\UwNodeFieldValue $uwNodeFieldValue
   *   Entity Type Manager from core.
   */
  public function __construct(UwNodeFieldValue $uwNodeFieldValue) {
    $this->uwNodeFieldValue = $uwNodeFieldValue;
  }

  /**
   * Get out the actual data from a node.
   *
   * @param \Drupal\node\Entity\Node $node
   *   The node.
   * @param string $view_mode
   *   The view mode.
   * @param array $content_data
   *   Array of data to pull from the node.
   *
   * @return array
   *   Array of data pulled from the node.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function getNodeData(Node $node, string $view_mode, array $content_data): array {

    // Array to store the teaser data, need blank
    // array in case there is no data to return.
    $node_data = [];

    foreach ($content_data as $index => $cdata) {
      if ($index == 'footer') {
        $node_data['footer'] = $this->getFooterData($node, $view_mode, $cdata);
      }
      else {
        if (isset($cdata['has_children']) && $cdata['has_children']) {
          foreach ($cdata as $sub_index => $data) {
            if ($sub_index !== 'has_children') {
              $node_data[$index][$sub_index] = $this->uwNodeFieldValue->getFieldValue(
                $node,
                $view_mode,
                $data['type'],
                $data['field'] ?? NULL,
                $data['extra_options'] ?? NULL
              );
            }
          }
        }
        else {
          $node_data[$index] = $this->uwNodeFieldValue->getFieldValue(
            $node,
            $view_mode,
            $cdata['type'],
            $cdata['field'] ?? NULL,
            $cdata['extra_options'] ?? NULL
          );
        }
      }
    }

    // Send down the bundle to get used in modifier classes.
    $bundle = str_replace('uw_ct_', '', $node->getType());
    $node_data['bundle'] = str_replace('_', '-', $bundle);

    // Get the cleaned node.
    $cleaned_node = $this->cleanNodeData($node_data);

    // If this is a contact, and it is a teaser, we have
    // to add a variable if there are values in the teaser.
    if ($bundle == 'contact' && $view_mode == 'teaser') {

      // If there are no values in image content or footer,
      // we can simply add the flag to false.
      if (
        empty($cleaned_node['image']) &&
        empty($cleaned_node['content']) &&
        empty($cleaned_node['footer'])
      ) {
        $cleaned_node['has_values'] = FALSE;
      }
      else {

        // If there are values in the content or image, we have
        // values to display in the teaser.
        if (
          !empty($cleaned_node['content']) ||
          !empty($cleaned_node['image'])
        ) {
          $cleaned_node['has_values'] = TRUE;
        }
        else {

          // If there are values in the footer, we have to check
          // only for the contact info, as that is what is
          // displayed on the teaser.
          if (!empty($cleaned_node['footer'])) {
            if (isset($cleaned_node['footer']['contact_info'])) {
              $cleaned_node['has_values'] = TRUE;
            }
            else {
              $cleaned_node['has_values'] = FALSE;
            }
          }
        }
      }
    }

    return $cleaned_node;
  }

  /**
   * Get the footer data for the node.
   *
   * @param \Drupal\node\Entity\Node $node
   *   The node.
   * @param string $view_mode
   *   The view mode.
   * @param array $content_data
   *   Array of data to pull from the node.
   *
   * @return array
   *   Array of data pulled from the node.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function getFooterData(Node $node, string $view_mode, array $content_data): array {

    $node_data = [];
    foreach ($content_data as $index => $cdata) {
      if (isset($cdata['has_children']) && $cdata['has_children']) {
        foreach ($cdata as $sub_index => $data) {
          if ($sub_index !== 'has_children') {
            $node_data[$index][$sub_index] = $this->uwNodeFieldValue->getFieldValue(
              $node,
              $view_mode,
              $data['type'],
              $data['field'] ?? NULL,
              $data['extra_options'] ?? NULL
            );
          }
        }
      }
      else {
        $node_data[$index] = $this->uwNodeFieldValue->getFieldValue(
          $node,
          $view_mode,
          $cdata['type'],
          $cdata['field'] ?? NULL,
          $data['extra_options'] ?? NULL
        );
      }
    }
    return $node_data;
  }

  /**
   * Function to clean the node data array of nulls.
   *
   * @param array $node_data
   *   The array of node data.
   *
   * @return array
   *   The cleaned array of node data.
   */
  public function cleanNodeData(array $node_data): array {

    // Step through the node data array and check for nulls.
    foreach ($node_data as $index => $cdata) {

      // If we are on the footer, clean it by itself,
      // since it is 3D array.
      // We need to add a continue here so that elements
      // from the footer are not cleared out, if we do not
      // have the continue, footers with one element will be
      // set to null.
      if ($index == 'footer') {
        $node_data['footer'] = $this->cleanFooterNodeData($node_data['footer']);
        continue;
      }

      // If the element is an array, step through all the
      // sub elements and check if null.
      if (is_array($cdata)) {

        // Step through each of the children and check if null.
        foreach ($cdata as $sub_index => $data) {

          // If still array check all its children.
          if (is_array($data)) {

            // Step through each children and if empty,
            // unset it.
            foreach ($data as $sub_sub_index => $d) {
              if ($d == NULL && empty($d)) {
                unset($node_data[$index][$sub_index][$sub_sub_index]);
              }
            }
          }
          else {

            // If the element is empty, unset it.
            if ($data == NULL && empty($data)) {
              unset($node_data[$index][$sub_index]);
            }
          }
        }
      }
      else {

        // If the element is empty or null, unset it.
        if ($cdata == NULL || empty($cdata)) {
          unset($node_data[$index]);
        }
      }
    }

    return $node_data;
  }

  /**
   * Function to clean the footer array.
   *
   * @param array $node_data
   *   The node data for the footer.
   *
   * @return array
   *   The array of data for the footer.
   */
  public function cleanFooterNodeData(array $node_data): array {

    // Step through each of the footer elements.
    foreach ($node_data as $index => $cdata) {

      // If the element is an array, step through its elements
      // and ensure that we have data.
      if (is_array($cdata)) {

        // Step through all the sub elements and if it is
        // and empty array, unset it.
        foreach ($cdata as $sub_index => $data) {
          if (empty($data)) {
            unset($node_data[$index][$sub_index]);
          }
        }

        // Now check if the parent is empty and if so
        // unset the parent.
        if (empty($node_data[$index])) {
          unset($node_data[$index]);
        }
      }
      else {

        // If the data is null, unset it.
        if ($cdata == NULL) {
          unset($node_data[$index]);
        }
      }
    }

    return $node_data;
  }

  /**
   * Gets the most recent date.
   *
   * @param array $date
   *   The date.
   * @param string $type
   *   The type of date.
   *
   * @return array
   *   The array of date info.
   */
  public function getDate(array $date, string $type): array {
    return $this->uwNodeFieldValue->getDate($date, $type);
  }

}
