<?php

namespace Drupal\uw_cfg_common\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Google Analytics and ownership form.
 *
 * @package Drupal\uw_cfg_common\Form
 */
class UwGoogleAnalyticsForm extends ConfigFormBase implements ContainerInjectionInterface {

  public const GOOGLE_SITE_TOKEN_REGEX = '/^[a-zA-Z0-9_-]{43}$/';
  public const GOOGLE_ANALYTICS_4_ID_REGEX = '/^G-[A-Z\d]{9,10}$/';

  /**
   * The render cache.
   *
   * @var \Drupal\Core\Plugin\CachedDiscoveryClearer
   */
  protected $cachePlugin;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->cachePlugin = $container->get('plugin.cache_clearer');
    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames(): array {
    return ['uw_cfg_common.google_settings'];
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return 'uw_cfg_common_google_analytics_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config('uw_cfg_common.google_settings');

    $form['uw_cfg_common_ga_account'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Measurement ID'),
      '#default_value' => $config->get('uw_cfg_common_ga_account'),
      '#description' => $this->t('Enter your Measurement ID for Google Analytics 4 in the form G-XXXXXXXXXX. To get a Measurement ID, register your site with Google Analytics, or if you already have registered your site, sign in to your Google Analytics account to find the Measurement ID (in the search bar type “GA4 measurement ID”).'),
      '#size' => 15,
      '#maxlength' => 20,
      '#required' => FALSE,
    ];

    $form['uw_cfg_common_google_site_ownership'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Google site ownership confirmation'),
      '#default_value' => $config->get('uw_cfg_common_google_site_ownership'),
      '#description' => $this->t('Enter the provided Google key string, which will display as the html meta tag google-site-verification.'),
      '#size' => 40,
      '#maxlength' => 256,
      '#required' => FALSE,
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $ga_value = $form_state->getValue('uw_cfg_common_ga_account');
    $g_site_ownership = $form_state->getValue('uw_cfg_common_google_site_ownership');

    // Validate Google Analytics value only when the value is not empty and
    // fails a regular expression pattern.
    if (!empty($ga_value) && !preg_match(self::GOOGLE_ANALYTICS_4_ID_REGEX, $ga_value)) {
      $ga_error_message = $this->t('A valid Google Analytics 4 ID is case sensitive and formatted like G-XXXXXXXXXX.');

      // Set form error for the field.
      $form_state->setErrorByName('uw_cfg_common_ga_account', $ga_error_message);
    }

    // Validate Google site ownership value, if provided, and make sure it
    // passes a regular expression pattern.
    if (!empty($g_site_ownership) && !(preg_match(self::GOOGLE_SITE_TOKEN_REGEX, $g_site_ownership))
    ) {
      $form_state->setErrorByName('uw_cfg_common_google_site_ownership',
        $this->t('A valid Google site ownership confirmation (the key string provided by Google) is needed and formatted like qWSaxyzBjZcP0PQTKeG1tHgL_Ff9n6s2noBPAXisc01.'));
    }
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);

    $this->config('uw_cfg_common.google_settings')
      ->set('uw_cfg_common_ga_account', $form_state->getValue('uw_cfg_common_ga_account'))
      ->set('uw_cfg_common_google_site_ownership', $form_state->getValue('uw_cfg_common_google_site_ownership'))
      ->save();

    // Clear all plugin caches.
    $this->cachePlugin->clearCachedDefinitions();
  }

}
