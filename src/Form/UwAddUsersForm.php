<?php

namespace Drupal\uw_cfg_common\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\path_alias\AliasManager;
use Drupal\user\Entity\User;
use Drupal\uw_cfg_common\Service\UWLdap;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form class for the content access form.
 */
class UwAddUsersForm extends FormBase {

  /**
   * Path alias manager from core.
   *
   * @var \Drupal\path_alias\AliasManager
   */
  protected AliasManager $pathAliasManager;

  /**
   * UW Ldap service from uw_cfg_common module.
   *
   * @var \Drupal\uw_cfg_common\Service\UWLdap
   */
  protected UWLdap $uwLdap;

  /**
   * Entity type interface manager from core.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      $container->get('path_alias.manager'),
      $container->get('uw_cfg_common.uw_ldap'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Class constructor.
   */
  public function __construct(AliasManager $pathAliasManager, UWLdap $uwLdap, EntityTypeManagerInterface $entityTypeManager) {
    $this->pathAliasManager = $pathAliasManager;
    $this->uwLdap = $uwLdap;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'uw_add_users_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // The list of users from this form.
    $form['users'] = [
      '#title' => $this->t('WatIAM user ID(s): (maximum 8 characters per ID)'),
      '#description' => $this->t('Enter a single WatIAM user ID, or multiple WatIAM user IDs, one per line. All letters will be converted to lower case. Duplicates will be ignored.<br>You can use <a href="https://iamtools.uwaterloo.ca/directory/" target="_blank">authenticated campus people directory</a> if you don\'t know their user ID (will open a new window).'),
      '#type' => 'textarea',
      '#required' => TRUE,
    ];

    // The submit button.
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Create new account(s)'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    global $base_url;
    // The user ID list that was submitted in the form.
    $submitted_users = explode(PHP_EOL, $form_state->getValue('users'));

    // Be nice and remove whitespace.
    $submitted_users = array_map('trim', $submitted_users);
    // Remove empty lines.
    $submitted_users = array_filter($submitted_users);
    // Apply to lower case on all lines.
    $submitted_users = array_map('strtolower', $submitted_users);
    // Remove any duplicates.
    $submitted_users = array_unique($submitted_users);

    // Step through each of the submitted users to create if needed/possible,
    // or show a message if not.
    foreach ($submitted_users as $user) {
      // Don't process long user IDs.
      if (strlen($user) > 8) {
        $this->messenger()->addError($this->t('The user ID <em>@user</em> is too long and was skipped.', ['@user' => $user]));
        continue;
      }

      // Don't process user IDs with invalid characters.
      if (!preg_match('/^[a-z0-9]+$/', $user)) {
        $this->messenger()->addError($this->t('The user ID <em>@user</em> contains invalid characters and was skipped.', ['@user' => $user]));
        continue;
      }

      $existing_user = $this->entityTypeManager->getStorage('user')->loadByProperties([
        'name' => $user,
      ]);

      /** @var \Drupal\user\UserInterface|bool $existing_user */
      $existing_user = $existing_user ? reset($existing_user) : FALSE;

      if ($existing_user) {
        $this->messenger()->addError($this->t('The user ID <a href="@link"><em>@user</em></a> already exists and was skipped.',
          [
            '@link' => $this->pathAliasManager->getAliasByPath('/user/' . $existing_user->id()),
            '@user' => $user,
          ]
        ));
        continue;
      }

      // Seems OK - move on.
      // Create a strong random password.
      $sets[] = 'abcdefghijklmnopqrstuvwxyz';
      $sets[] = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $sets[] = '0123456789';
      $sets[] = '!@#$%^&*()-_=+[]{}?`~"';
      $password = '';
      $password_length = random_int(16, 32);
      while (strlen($password) < $password_length) {
        foreach ($sets as $set) {
          $password .= $set[array_rand(str_split($set))];
          if (strlen($password) === $password_length) {
            break;
          }
        }
      }
      $password = str_shuffle($password);

      // Do the LDAP lookup.
      $person = $this->uwLdap->lookupPerson($user);
      if (!$person) {
        $this->messenger()->addError($this->t('The user ID <em>@user</em> could not be found (or was set to private) and was skipped.', ['@user' => $user]));
        continue;
      }
      $firstname = empty($person['givenname'][0]) ? 'Firstname' : $person['givenname'][0];
      $lastname = empty($person['sn'][0]) ? 'Lastname' : $person['sn'][0];
      $email = empty($person['mail'][0]) ? $user . '@uwaterloo.ca' : $person['mail'][0];

      // Add the user.
      $new_user = User::create();
      $new_user->setPassword($password);
      $new_user->enforceIsNew();
      $new_user->setEmail($email);
      $new_user->setUsername($user);
      $new_user->set('field_uw_first_name', $firstname);
      $new_user->set('field_uw_last_name', $lastname);
      $new_user->activate();
      $new_user->save();

      $this->messenger()->addStatus($this->t('Created a new user with the user ID <a href="@link"><em>@user</em></a>.',
        [
          '@link' => $base_url . $this->pathAliasManager->getAliasByPath('/user/' . $new_user->id()),
          '@user' => $user,
        ]
      ));
    }

    // Set the message that the users have been created.
    $this->messenger()->addStatus($this->t('Finished creating users.'));
  }

}
