<?php

namespace Drupal\uw_cfg_common\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Routing\TrustedRedirectResponse;

/**
 * Form class for the search form.
 */
class UwSearchForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'uw_search_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Set the attributes and role for the form.
    $form['#attributes'] = [
      'class' => ['uw-search--form'],
      'role' => 'search',
    ];

    // Element used to open the tray for phone view.
    $form['opentray'] = [
      '#type' => 'checkbox',
      '#title' => '<span class="uw-search--checkboxlabel--labeltext">Search Checkbox</span>',
      '#label_classes' => [
        'uw-search--checkboxlabel',
      ],
      '#attributes' => [
        'class' => [
          'uw-input',
          'uw-input--checkboxform',
        ],
        'aria-hidden' => 'true',
        'tabindex' => '-1',
      ],
      '#theme_wrappers' => [],
    ];

    // This is the label that is used for the "phone" view on
    // the site search.  We need to use form element label here
    // by itself, since we use theme_wrappers as an empty array
    // above in the input, which would normally give us the form
    // label.
    $form['label'] = [
      '#theme' => 'form_element_label',
      '#title' => '<span class="uw-search--checkboxlabel__labeltext">Open Search Location </span>',
      '#title_display' => 'after',
      '#required' => FALSE,
      '#id' => 'edit-opentray',
      '#attributes' => [
        'class' => ['uw-search--checkboxlabel'],
      ],
    ];

    // The search text.
    $form['search-input'] = [
      '#type' => 'textfield',
      '#attributes' => [
        'class' => ['uw-input', 'uw-input--search'],
      ],
      '#id' => 'uw-search',
      '#placeholder' => 'Search',
      '#title' => '<span class="uw-search--labeltext">Search for </span>',
      '#title_display' => 'invisible',
      '#label' => [
        '#theme' => 'form_element_label',
        '#required' => FALSE,
        '#id' => 'uw-search',
        '#attributes' => [
          'class' => ['uw-search--hidelabel'],
        ],
      ],
    ];
    // Get the URL for this site to be used in the options.
    $url = Url::fromRoute('<front>', [], ['absolute' => TRUE])->toString();

    // Check theme setting of 'search_show_local' field
    // in admin/appearance/settings/uw_fdsu_theme_resp.
    $search_show_local = theme_get_setting('wcms_search_show_local', 'uw_fdsu_theme_resp');

    // Set defaults for dropdown. This option is always there. Also, covers
    // case where search local is unchecked.
    $options = [
      '' => 'On all sites',
    ];

    // Add option to search local site, this cover use cases where
    // search local is not found (existing sites) or setting is checked.
    if ($search_show_local === NULL || $search_show_local === 1) {
      $options['inurl:' . $url] = 'On this site';
    }

    // The type of search, either all sites or this site.
    $form['search-type'] = [
      '#type' => 'select',
      '#id' => 'uw-select-site',
      '#attributes' => [
        'class' => ['form-item__select', 'uw-select--search'],
      ],
      '#options' => $options,
      '#title_display' => 'invisible',
      '#title' => 'Search Location',
      '#label' => [
        '#theme' => 'form_element_label',
        '#required' => FALSE,
        '#id' => 'uw-select-site',
        '#attributes' => [
          'class' => ['uw-search--hidelabel'],
        ],
      ],
    ];

    // The submit button.
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#id' => 'uw-search-submit',
      '#attributes' => [
        'value' => 'Search',
        'class' => [
          'button',
          'button--submit',
          'button--submit__form',
        ],
      ],
      '#prefix' => '<div class="uw-search-button__wrapper">',
      '#suffix' => '</div>',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Variable for the parameters.
    $parameters = '';

    // Get the values from the form state.
    $values = $form_state->getValues();

    // If this is a site search, add it to parameters.
    if ($values['search-type']) {
      $parameters .= $values['search-type'] . ' ';
    }

    // If there is search text add it to the parameters.
    if ($values['search-input']) {
      $parameters .= $values['search-input'];
    }

    // The URL to the uwaterloo search.
    $url = 'https://uwaterloo.ca/search';

    // If there are parameters, encode and add to URL.
    if ($parameters !== '') {
      $url .= '/?search-input=' . urlencode($parameters);
    }

    // Redirect to the uwaterloo search.
    $form_state->setResponse(new TrustedRedirectResponse($url, 302));
  }

}
