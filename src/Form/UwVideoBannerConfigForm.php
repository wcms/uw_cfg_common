<?php

namespace Drupal\uw_cfg_common\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DefaultForm.
 */
class UwVideoBannerConfigForm extends ConfigFormBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->currentUser = $container->get('current_user');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'uw_cfg_common_video_banner.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'video_banner_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('uw_cfg_common_video_banner.settings');

    $form['settings']['video_banner_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable third-party video banners using a paid Vimeo account'),
      '#description' => $this->t('ONLY enable this if you will be using a video from a paid Vimeo account. Videos from free accounts will display the video player and other Vimeo interface elements over the video.'),
      '#default_value' => $config->get('video_banner_enabled'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->config('uw_cfg_common_video_banner.settings');
    $config->set('video_banner_enabled', $form_state->getValue('video_banner_enabled'));
    $config->save();
  }

}
