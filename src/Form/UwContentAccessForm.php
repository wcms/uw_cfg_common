<?php

namespace Drupal\uw_cfg_common\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\uw_cfg_common\UwPermissions\UwPermissions;

/**
 * Form class for the content access form.
 */
class UwContentAccessForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'uw_content_access_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // The roles to be used in this form.
    $uw_roles = UwPermissions::getRoles();

    // The permissions table to be used on this form.
    $form['permissions'] = [
      '#type' => 'table',
      '#header' => [$this->t('Content type/feature')],
      '#id' => 'permissions',
      '#attributes' => ['class' => ['permissions', 'js-permissions']],
      '#sticky' => TRUE,
    ];

    // Add to the header the functionality column.
    $form['permissions']['#header'][] = [
      'data' => 'Functionality',
    ];

    // Step through each of the uw roles and add to header as well as store role
    // object.
    foreach ($uw_roles as $uw_role) {

      // Add role to header of table for this form.
      $form['permissions']['#header'][] = [
        'data' => $uw_role['name'],
        'class' => ['checkbox'],
      ];
    }

    // Get the permissions array for this form.
    $uw_permissions = UwPermissions::getPermissionsArray();

    // Step through each permission and setup table.
    foreach ($uw_permissions as $feature => $uw_permission) {

      if ($feature !== 'help_text') {

        // Row count to tell us if we are on the same row.
        $row_count = 0;

        // Step through each of the permission types and setup table.
        foreach ($uw_permission as $perm => $uw_permission_roles) {

          // Set the row name to be use for this table (role-permission_name).
          $row_name = $feature . '-' . $perm;

          // If we are on the first of the row, setup the description.
          if ($row_count == 0) {

            // The markup for the description.
            $form['permissions'][$row_name]['description']['#markup'] = $feature;

            // If we have more than one permission setup the rowspan.
            if (count($uw_permission) >= 1) {

              // The rowspan settings.
              $form['permissions'][$row_name]['description']['#wrapper_attributes'] = [
                'rowspan' => count($uw_permission),
                'style' => 'vertical-align: top',
              ];
            }
          }

          // Increment the row count.
          $row_count++;

          if (isset($uw_permissions['help_text'][$feature])) {
            $markup = $perm . '<br/><em>' . $uw_permissions['help_text'][$feature] . '</em>';
          }
          else {
            $markup = $perm;
          }

          // Set the functionality column.
          $form['permissions'][$row_name]['functionality'] = [
            '#markup' => $markup,
          ];

          // Step through each of the uw permissions and setup checkbox if
          // permission is set.
          foreach ($uw_permission_roles as $uw_role_name => $uw_permission_role) {

            // The checkbox for the role/permission set.
            $form['permissions'][$row_name][$uw_role_name] = [
              '#title' => $perm,
              '#title_display' => 'invisible',
              '#wrapper_attributes' => [
                'class' => ['checkbox'],
              ],
              '#type' => 'checkbox',
              '#default_value' => $uw_roles[$uw_role_name]['object']->hasPermission($uw_permission_role[0]) ? 1 : 0,
            ];
          }
        }
      }
    }

    // The submit button.
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save permissions'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // The permissions array that was submitted in the form.
    $submitted_permissions = $form_state->getValue('permissions');

    // The array of uw permissions.
    $uw_permissions = UwPermissions::getPermissionsArray();

    // The array of uw roles.
    $uw_roles = UwPermissions::getRoles();

    // Step through each of the submitted permissions to grant or revoke it for
    // the role. Step through each of the permissions array to get the row name
    // (role-permission).
    foreach ($submitted_permissions as $row_name => $submitted_permission) {

      // Step through each of the role-permissions to revoke or grant.
      foreach ($submitted_permission as $uw_role_name => $permission_value) {

        // Break up the row name from role-permission into array.
        // 0th element is role.
        // 1st element is permission.
        $perm_names = explode('-', $row_name);

        // Get the list of permissions to revoke/grant from the uw_permissions
        // array.
        $uw_perms = $uw_permissions[$perm_names[0]][$perm_names[1]][$uw_role_name];

        // If the checkbox was selected on the form grant the permissions that
        // is in the uw_permissions array for that role.
        if ($permission_value) {

          // Step through each of the permissions for that role and grant the
          // permission.
          foreach ($uw_perms as $uw_perm) {

            // Grant the permission for the specified role.
            $uw_roles[$uw_role_name]['object']->grantPermission($uw_perm);
          }
        }
        // If the checkbox was not selected on the form revoke the permissions
        // that is in the uw_permissions array for that role.
        else {

          // Step through each of the permissions for that role and revoke the
          // permission.
          foreach ($uw_perms as $uw_perm) {

            // Revoke the permission for the specified role.
            $uw_roles[$uw_role_name]['object']->revokePermission($uw_perm);
          }
        }
      }
    }

    // Save the permissions.
    UwPermissions::save($uw_roles);

    // Set the message that the permissions have been saved.
    $this->messenger()->addStatus($this->t('The changes have been saved.'));
  }

}
