<?php

namespace Drupal\uw_cfg_common\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class UwNodeAccessRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    $access_route_names = [
      // Dashboard config: admin/config/dashboards/dashboardssettings.
      'dashboards.dashboards_settings_form',
      // Node pages (/node/{nid}).
      'entity.node.canonical',
      // Node delete pages.
      'entity.node.delete_form',
      // Menu link edit pages.
      'menu_ui.link_edit',
      // Menu link add page.
      // Path admin/structure/menu/manage/{menu}/add.
      'entity.menu.add_link_form',
      // Menu link edit page.
      // Path admin/structure/menu/item/{menu_link_content}/edit.
      'entity.menu_link_content.canonical',
      'entity.menu_link_content.edit_form',
      // Menu link delete page.
      // Path admin/structure/menu/item/{menu_link_content}/delete.
      'entity.menu_link_content.delete_form',
    ];
    foreach ($access_route_names as $route_name) {
      if ($route = $collection->get($route_name)) {
        $route->setRequirement('_custom_access', 'Drupal\uw_cfg_common\Access\UwNodeAccessCheck::access');
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    // Run this implementation of alterRoutes() after menu_admin_per_menu, which
    // has priority -220.
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', -300];
    return $events;
  }

}
