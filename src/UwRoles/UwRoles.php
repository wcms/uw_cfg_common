<?php

namespace Drupal\uw_cfg_common\UwRoles;

use Drupal\user\Entity\Role;
use Drupal\Component\Serialization\Yaml;

/**
 * Class UwRoles.
 *
 * UW Specific roles permissions.
 */
class UwRoles {

  /**
   * Gets an array of data about a UW role.
   *
   * @param string $rid
   *   The role id.
   *
   * @return array
   *   An array of data for a UW role.
   */
  public static function getUwRole(string $rid): array {

    return [
      'label' => UwRoles::getUwRoleLabel($rid),
      'object' => Role::load($rid),
      'permissions' => UwRoles::getUwPermissions($rid),
    ];
  }

  /**
   * Gets the list of ids for UW roles.
   *
   * @return string[]
   *   An array of ids for the UW roles
   */
  public static function getUwRoleIds(): array {
    return [
      'uw_role_website_lead',
      'uw_role_site_owner',
      'uw_role_site_manager',
      'uw_role_content_editor',
      'uw_role_content_author',
      'uw_role_form_editor',
      'uw_role_form_results_access',
      'uw_role_private_content_viewer',
      'uw_role_legacy',
    ];
  }

  /**
   * Get the role id for Drupal roles.
   *
   * @return array
   *   Array of role ids for Drupal roles.
   */
  public static function getDrupalRoleIds(): array {

    return [
      'anonymous',
      'authenticated',
    ];
  }

  /**
   * Gets all the role ids in the system, UW and Drupal.
   *
   * @return array
   *   Array of all role ids.
   */
  public static function getAllRoles(): array {

    // Get the UW roles.
    $roles = UwRoles::getUwRoleIds();

    // Append the Drupal roles.
    $roles = array_merge($roles, UwRoles::getDrupalRoleIds());

    // Return all the roles.
    return $roles;
  }

  /**
   * Gets the label for a UW role.
   *
   * @param string $rid
   *   The role id.
   *
   * @return string
   *   The label.
   */
  public static function getUwRoleLabel(string $rid): string {

    switch ($rid) {
      case 'uw_role_website_lead':
        return 'Website lead';

      case 'uw_role_site_owner':
        return 'Site owner';

      case 'uw_role_site_manager':
        return 'Site manager';

      case 'uw_role_content_editor':
        return 'Content editor';

      case 'uw_role_content_author':
        return 'Content author';

      case 'uw_role_form_editor':
        return 'Form editor';

      case 'uw_role_form_results_access':
        return 'Form results access';

      case 'uw_role_private_content_viewer':
        return 'Private content viewer';

      case 'uw_role_legacy':
        return 'Legacy';

      case 'anonymous':
        return 'Anonymous';

      case 'authenticated':
        return 'Authenticated';

      default:
        return '';
    }
  }

  /**
   * Get the permissions for a specific role.
   *
   * @param string $rid
   *   The role id.
   *
   * @return array
   *   An array of permissions for a specific role.
   */
  public static function getUwPermissions(string $rid): array {

    // Get the file to the user role permissions yml file.
    $file = __DIR__ . '/user.role.' . $rid . '.yml';

    // Parse the yml file to get the permissions.
    $permissions = Yaml::decode(file_get_contents($file));

    // Remove 'access content' permission from roles when uw_auth_site enabled.
    $roles_to_remove_from = [
      'anonymous',
      'authenticated',
      'legacy',
    ];
    if (in_array($rid, $roles_to_remove_from, TRUE)) {
      $config = \Drupal::config('uw_auth_site.auth_site')->get('auth_site');
      if ($config['enabled'] ?? FALSE) {
        $key = array_search('access content', $permissions['permissions'], TRUE);
        // If the permission exists, remove it and reindex the array.
        if ($key !== FALSE) {
          unset($permissions['permissions'][$key]);
          $permissions['permissions'] = array_values($permissions['permissions']);
        }
      }
    }

    // Return the permissions for the specific user.
    return $permissions['permissions'];
  }

  /**
   * Set the list of permissions inside the uw_role array.
   *
   * @param array $uw_role
   *   - The roles array.
   * @param array $additional
   *   - The access content array.
   *   The uw_role array from function getRole.
   */
  public static function setUwPermissions(array $uw_role, array $additional = []) {

    $current_permissions = $uw_role['object']->getPermissions();
    $desired_permissions = $uw_role['permissions'];

    $add_permissions = array_diff($desired_permissions, $current_permissions);
    foreach ($add_permissions as $permission) {
      $uw_role['object']->grantPermission($permission);
    }

    $remove_permissions = array_diff($current_permissions, $desired_permissions, $additional);
    foreach ($remove_permissions as $permission) {
      $uw_role['object']->revokePermission($permission);
    }

    $uw_role['object']->save();
  }

}
