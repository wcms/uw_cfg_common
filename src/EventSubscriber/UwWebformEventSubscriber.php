<?php

namespace Drupal\uw_cfg_common\EventSubscriber;

use Drupal\core_event_dispatcher\EntityHookEvents;
use Drupal\core_event_dispatcher\Event\Entity\EntityPresaveEvent;
use Drupal\webform\WebformInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Fixes roles permission for submission create for webforms.
 */
class UwWebformEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      EntityHookEvents::ENTITY_PRE_SAVE => 'preSave',
    ];
  }

  /**
   * Entity pre save dispatched event.
   *
   * @param \Drupal\core_event_dispatcher\Event\Entity\EntityPresaveEvent $event
   *   Event object.
   */
  public function preSave(EntityPresaveEvent $event): void {
    $entity = $event->getEntity();

    if ($entity->bundle() === 'webform') {
      /** @var \Drupal\webform\Entity\Webform $entity */
      $access_rules = $entity->getAccessRules();

      // Update webform submission access roles to be anonymous and
      // authenticated. This will be done on EVERY webform save.
      // Fixes migration issue with webforms. Once migration is updated and
      // fixed, this code is no longer needed and can be removed.
      $access_rules['create']['roles'] = [
        'anonymous',
        'authenticated',
      ];

      $entity->setAccessRules($access_rules);

      // Setting default access denied handler.
      $entity->setSetting('form_access_denied', WebformInterface::ACCESS_DENIED_PAGE);
    }
  }

}
