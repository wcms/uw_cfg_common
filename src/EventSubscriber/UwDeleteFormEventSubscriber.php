<?php

namespace Drupal\uw_cfg_common\EventSubscriber;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\core_event_dispatcher\Event\Form\FormAlterEvent;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\core_event_dispatcher\FormHookEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * UW content type delete form event subscriber.
 */
class UwDeleteFormEventSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Default constructor.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   Current user.
   */
  public function __construct(AccountProxyInterface $currentUser) {
    $this->currentUser = $currentUser;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      FormHookEvents::FORM_ALTER => 'alterForm',
    ];
  }

  /**
   * Alter form.
   *
   * @param \Drupal\core_event_dispatcher\Event\Form\FormAlterEvent $event
   *   The event.
   */
  public function alterForm(FormAlterEvent $event): void {
    $form = &$event->getForm();

    // Change the deletion warning on any of the following forms.
    // - any node, taxonomy term, or media deletion page.
    // - the node delete multiple page.
    // - the media delete multiple page.
    if (
      preg_match('/^(?:node|taxonomy_term|media)_.+_delete_form$/', $form['#form_id'])
      ||
      $form['#form_id'] == 'node_delete_multiple_confirm_form'
      ||
      $form['#form_id'] == 'media_delete_multiple_confirm_form'
    ) {
      $form['description']['#markup'] = $this->t('CAUTION. This will permanently delete this piece of content; this action cannot be undone. If anything references this content, it may cause visual or structural issues on that page. Make sure you have removed or updated all references before deleting.');
    }
  }

}
